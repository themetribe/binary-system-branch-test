(function(global, $){ $(document).ready(function(){
	$('.datepicker').datepicker();
	Video.listener($);

	buttons.bounce($);
	buttons.scroll($);
	gallery.grid($);

	Menu.listener($);
}); })(window, jQuery);


 var Menu = {
 	listener : function($){
 		$("#btn-navtoggle").on('click',function(){
 			$("#menu").toggleClass('mobile-show');
 			return false;
 		})
 	}
 }


var Video = {
	listener : function($){
		var mobileBreak = 520;
		 $(window).resize(function(){
	        var wHeight = $(window).height();
	        var wWidth = $(window).width();
	        if (wWidth < mobileBreak) {
	            $('.full-height').css({height:''});
	            $('.full-height').css({height:''});
	        }else {
	            $('.half-height').height(wHeight/2);
	            $('.full-height').height(wHeight);
	        }

	        var modW = wWidth/1200;
	        var modH = wHeight/674;
	        var vHeight =  modW<modH?wHeight:'auto';
	        var vWidth =  modW<modH?'auto':wWidth;
	        var vidWidth = modW<modH?wHeight*(1200/674):wWidth;
	        var margLeft = modW<modH?((wWidth-vidWidth)/2):0;
	        $('#video-moment-0').css({
	            height:vHeight,
	            width:vWidth,
	            marginLeft:margLeft
	        });
	    }).trigger('resize');

		Video.smartblur($);
	},
	smartblur : function($){
		$(".hero-full .container").hover(function(){
			console.log('test00');
			$(".hero-full").addClass('focus');
		}, function(){
			if(!$(".hero-full").hasClass('locked')){
				$(".hero-full").removeClass('focus');	
			}			
		})
	}
}

var buttons = {
	bounce : function($){
		$("#btn_get-started").on('click',function(){
			$(this).fadeOut('slow');
			$('#menu').show();
			$('#menu').addClass('animated bounceInDown');
			$(".hero-full").addClass('locked');
		})
	},
	scroll : function($){
		$('a[href*=#]:not([href=#])').click(function() {
		    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		      var target = $(this.hash);
		      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		      if (target.length) {
		        $('html,body').animate({
		          scrollTop: target.offset().top
		        }, 1000);
		        return false;
		      }
		    }
		})
	}
}

var gallery = {
	grid : function($){
		$( '#ri-grid' ).gridrotator( {
			rows		: 3,
			columns		: 15,
			animType	: 'fadeInOut',
			animSpeed	: 1000,
			interval	: 600,
			step		: 1,
			w320		: {
				rows	: 3,
				columns	: 4
			},
			w240		: {
				rows	: 3,
				columns	: 4
			}
		});
	}
}


