(function(global, $){ $(document).ready(function(){
	SwitchButton.listener($);
	$('.datepicker').datepicker();
	$("input[type='number']").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    
}); })(window, jQuery);

var SwitchButton = {
	listener : function($){
		$(".btn-toggle").on('click',function(){
			$(this).parent().find('.btn-toggle').removeClass('active');
			$(this).addClass('active');
		})
	}
}