<div class="main" id="library">

	<div class="block clearfix" id="menu">
		<ul class="nav navbar-nav">
			<li><a class="ebooks" href="#">Ebooks</a></li>
			<li><a class="video" href="#">Video</a></li>
			<li><a class="audio" href="#">Audio</a></li>
			<li><a class="quotes" href="<?php echo SITE_URL ?>/library?t=6">Quotes</a></li>
			<li><a class="mixed" href="<?php echo SITE_URL ?>/library">Mixed</a></li>
			<li>
				<a data-toggle="dropdown" href="#">Tutorial <span class="caret"></span></a>
				<ul class="dropdown-menu mentors" role="menu" aria-labelledby="dLabel">					
					<li><a href="<?php echo SITE_URL ?>/library?st=4">System Tour</a></li>
					<li><a href="<?php echo SITE_URL ?>/library?st=5">Inlight</a></li>
					<li><a class="systemtour" href="#">Business</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a data-toggle="dropdown" href="#">Mentor <span class="caret"></span></a>
				<ul class="dropdown-menu mentors" role="menu" aria-labelledby="dLabel">					
					<li><a href="<?php echo SITE_URL ?>/library?m=Jim%20Rohn">Jim Rohn</a></li>
					<li><a href="<?php echo SITE_URL ?>/library?m=Anthony%20Robbins">Anthony Robbins</a></li>
					<li><a href="<?php echo SITE_URL ?>/library?m=Chinky%20Tan">Chinky Tan</a></li>
					<li><a href="<?php echo SITE_URL ?>/library?m=Eric%20Worre">Eric Worre</a></li>
					<li><a href="<?php echo SITE_URL ?>/library?m=Robert%20Kiyosaki">Robert Kiyosaki</a></li>
				</ul>
			</li>
		</ul>				
	</div>

	<div class="contents">
		<?php
		if(isset($_GET['st']) && $_GET['st']=="4"){
			?>
			<h3>Inlight Marketing System Tour</h3>
			<video controls autoplay="" id="video-opener" preload="auto" canplay="true" class="is-horizontal" style="width:750px;">
		        <source src="<?php echo SITE_URL ?>/dashboard?t=site_tour&img=System Tour.mp4" type="video/mp4">
		    </video>
			<?php
		}
		elseif($library!="" && count($library)>0){
			foreach($library as $key=>$val):
				$type = $cls_library->gettype($val['type']);
				$thumb_path = $val['thumb_path'];
				$file_src = $val['file_src'];

				if($type=="quotes"){
					$thumb_path = $file_src;
				}
				?>			
				<div style="width:214px; vertical-align:top;" class="item <?php echo $type ?>" data-link="<?php echo Func::get_secured_resources($type, $file_src,0); ?>" data-title="<?php echo $val['title'] ?>" data-excerpt="<?php echo $val['excerpt'] ?>">
					<a href="#"></a>
					<?php
					/* FOR FAVORITE ITEMS ONLY - FUTURE
					<span class="favorite center"><i class="fa fa-heart"></i></span>
					*/ ?>
					<img width="214" src="<?php echo Func::get_secured_resources($type.'thumb', $thumb_path,0); ?>" />				
					<br /><strong><?php echo $val['title'] ?></strong>
				</div>
				<?php				
			endforeach;
		}
		else{
			echo "<center>We are on our way adding datas for this section. Thank you.</center>";
		}
		?>
	</div>
</div>