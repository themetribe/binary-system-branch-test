<div class="main" id="support">
	<form action="" method="POST">
		<?php if(isset($sent)) :?>
			<div class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			  <strong>Thank you!</strong> your message has been sent successfully.
			</div>
		<?php endif; ?>	
		<h4>SUPPORT TICKET</h4>
		<p>Please send us an email for inquiries and concern. Thank you!</p>

		<div class="form-group row">
			<div class="col col-sm-2 control-label"><label for="">Name:</label></div>
			<div class="col col-sm-4">
				<input type="text" class="form-control" name="name" placeholder="Enter your name" required>
			</div>
		</div>
		<div class="form-group row">
			<div class="col col-sm-2 control-label"><label for="">Email:</label></div>
			<div class="col col-sm-4">
				<input type="email" class="form-control" name="email" placeholder="Enter your email" required>
			</div>
		</div>
		<div class="form-group row">
			<div class="col col-sm-2 control-label"><label for="">Subject:</label></div>
			<div class="col col-sm-4">
				<input type="text" class="form-control" name="subject" placeholder="Enter your subject" required>
			</div>
		</div>
		<div class="form-group row">
		<div class="col col-sm-2 control-label"><label for="">Message:</label></div>
			<div class="col col-sm-5">
				<textarea type="text" class="form-control" name="message" placeholder="Enter your message" required></textarea>
			</div>
		</div>
		
		<div class="form-group row">
			<div class="col col-sm-2 control-label"></div>
			<div class="col col-sm-5"><button type="submit" name="btn-send" class="btn btn-primary pull-right">SUBMIT</button></div>
		</div>
	</form>
</div>