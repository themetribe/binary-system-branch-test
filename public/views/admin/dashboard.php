<div id="dashboard" class="main">
	<div class="block row" id="top">
		<div class="content col-md-8">
			<h1>Welcome to <strong>Inlight Mktg Backoffice</strong></h1>
			<p>This is your back office. All you need to do now is share your Inlight Marketing Affiliate link to as many people as possible. The more people you share it with, the bigger your chances you will attain your dreams and aspirations.</p>
			<hr />
			<h3>Accumulated Commissions</h3>
			<?php if($_SESSION['user_type']<=2 || $_SESSION['user_type']>5) : ?>
				<div class="row">
					<div class="col-md-3 col-sm-6"><strong>Weekly</strong><br /><span class="value">P<?php echo $_SESSION['weekly'] ?></span></div>
					<div class="col-md-3 col-sm-6"><strong>Monthly</strong><br /><span class="value">P<?php echo $_SESSION['monthly'] ?></span></div>
					<div class="col-md-3 col-sm-6"><strong>Total</strong><br /><span class="value">P<?php echo $_SESSION['total'] ?></span></div>
					<div class="col-md-3 col-sm-6"><strong>Current Balance</strong><br /><span class="value">P<?php echo $_SESSION['current'] ?></span></div>
				</div>
			<?php elseif($_SESSION['user_type']>=3 && $_SESSION['user_type']<=5): ?>
				<div class="row">
					<div class="col-md-4 col-sm-6"><strong>Weekly</strong><br /><span class="value">P<?php echo $_SESSION['weekly'] ?></span></div>
					<div class="col-md-4 col-sm-6"><strong>Monthly</strong><br /><span class="value">P<?php echo $_SESSION['monthly'] ?></span></div>
					<div class="col-md-4 col-sm-6"><strong>Profit Share(<?php echo Func::investor_share_percentage(); ?>%)</strong><br /><span class="value">P<?php echo $_SESSION['investor_share'] ?></span></div>
					<div class="col-md-6 col-sm-6"><strong>Total</strong><br /><span class="value">P<?php echo $_SESSION['total'] ?></span></div>
					<div class="col-md-6 col-sm-6"><strong>Current Balance</strong><br /><span class="value">P<?php echo $_SESSION['current'] ?></span></div>
				</div>
			<?php endif; ?>
		</div>
		<div class="side col-md-4">
			<h3>Affiliate Link</h3>
			<p>Please choose your preferred link to be shared.</p>
			<strong>Premier's Link</strong>
			<input type="text" class="form-control" value="<?php echo SITE_URL."/join?afid=".$_SESSION['user_ID']; ?>" readonly />
			<strong>Investor's Link</strong>
			<?php /*<input disabled type="text" class="form-control" value="<?php echo SITE_URL."/join?afid=".$_SESSION['user_ID']; ?>&t=3" />*/ ?>
			<input disabled type="text" class="form-control" value="Coming Soon" />
			<hr />
			<h3>Inlight Marketing Library</h3>
			<a href="<?php echo SITE_URL ?>/library?t=3" class="btn btn-primary btn-lg"><i class="fa fa-book"></i> Library v1.0</a>
		</div>
	</div>
	<div class="block row">
		<div class="content col-md-8">
			<div id="fbwall-section" class="comingsoon">
				FB Wall Section Here... coming soon...
			</div>
			<div id="slideshow-section">
				<div class="slideshow">
					<img src="<?php echo SITE_URL ?>/assets/img/welcome to inlight.png"/>
					<img src="<?php echo SITE_URL ?>/assets/img/IM TOP 10.png"/>
				</div>
			</div>
			<div id="jobs-section" class="section">
				<h3><i class="fa fa-binoculars"></i> Jobs Here...</h3>
				<p>We have incorporated latest job offers for you into our database within the Philippines.</p>
				<a href="<?php echo SITE_URL ?>/jobs" class="btn btn-primary btn-lg">Start Finding Jobs!</a>				
			</div>

			<div id="note-section" class="section" style="display:none;">
				<h5>Disclaimer</h5>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse gravida urna urna, sit amet faucibus justo volutpat sit amet. Sed at magna non eros pretium cursus quis nec eros. Vivamus euismod diam libero, sed facilisis mi dignissim in. Etiam non velit ut eros molestie tincidunt et vitae velit. Curabitur dapibus diam ipsum, eget scelerisque odio varius id. Mauris semper vehicula magna at fringilla. Sed pharetra gravida consequat. Nunc imperdiet a erat in semper. Fusce leo arcu, feugiat nec ipsum id, blandit mollis tellus. Quisque vel blandit purus.
			</div>
		</div>
		<div class="side col-md-4">
			<div id="top10-section" class="section">
				<h3>Top 10 Earners</h3>
				<p>Be one of the top 10 earners nation wide and we will be giving gifts for you! Plus your name will be published here. </p>
				<ul style="display:none;">
					<li>Doctor Jones</li>
					<li>Doctor Jones</li>
					<li>Doctor Jones</li>
					<li>Doctor Jones</li>
					<li>Doctor Jones</li>
					<li>Doctor Jones</li>
					<li>Doctor Jones</li>
					<li>Doctor Jones</li>
					<li>Doctor Jones</li>
					<li>Doctor Jones</li>
				</ul>
			</div>
			<hr />
			<div id="newsupdates-section" class="section">
				<h3>News &amp; Updates</h3>
				<div class="entry row">
					<div class="col-md-3">
						<img src="<?php echo SITE_URL ?>/assets/img/news1.jpg"/>
					</div>
					<div class="col-md-9">
						<h4>The case for a second term</h4>
						<p>When the Koalisyon ng Mamamayan para sa Reporma (KOMPRe) was launched last month, we got a glimpse into the Aquino administration’s 2016 electoral strategy... <a href="https://ph.news.yahoo.com/blogs/parallaxis/the-case-for-a-second-term-005952776.html">More</a></p>
					</div>
				</div>
				<div class="entry row">
					<div class="col-md-3">
						<img src="<?php echo SITE_URL ?>/assets/img/news2.jpg"/>
					</div>
					<div class="col-md-9">
						<h4>Run, PNoy, Run</h4>
						<p>When the Koalisyon ng Mamamayan para sa Reporma (KOMPRe) was launched last month, we got a glimpse into the Aquino administration’s 2016 electoral strategy. <a href="https://ph.news.yahoo.com/blogs/parallaxis/the-case-for-a-second-term-005952776.html">More</a></p>
					</div>
				</div>
				<div class="entry row">
					<div class="col-md-3">
						<img src="<?php echo SITE_URL ?>/assets/img/news3.jpg"/>
					</div>
					<div class="col-md-9">
						<h4>The case for a second term</h4>
						<p>When the Koalisyon ng Mamamayan para sa Reporma (KOMPRe) was launched last month, we got a glimpse into the Aquino administration’s 2016 electoral strategy. <a href="https://ph.news.yahoo.com/blogs/parallaxis/the-case-for-a-second-term-005952776.html">More</a></p>
					</div>
				</div>
			</div>
			<div id="latestebooks-section" class="section">
				<h3>Latest Ebooks</h3>
				<p>Be the first to read these ebooks</p>
				<table class="table">
					<tr>
						<td><a href="#">Backend Sales 4Pro</a></td>
						<td>in Business And Networking</td>
						<td>5 months ago</td>
					</tr>
					<tr>
						<td><a href="#">Backend Sales 4Pro</a></td>
						<td>in Business And Networking</td>
						<td>5 months ago</td>
					</tr>
					<tr>
						<td><a href="#">Backend Sales 4Pro</a></td>
						<td>in Business And Networking</td>
						<td>5 months ago</td>
					</tr>
					<tr>
						<td><a href="#">Backend Sales 4Pro</a></td>
						<td>in Business And Networking</td>
						<td>5 months ago</td>
					</tr>
					<tr>
						<td><a href="#">Backend Sales 4Pro</a></td>
						<td>in Business And Networking</td>
						<td>5 months ago</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>