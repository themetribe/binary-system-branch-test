<div id="ewallet" class="main">

	<div id="top" class="block row">
		<div class="content col-md-8">
			<p>E-wallet widthrawals will be available on every tuesday only. </p>

			<div class="row">
				<div class="col-md-4 col-sm-6">
					<a href="#" id="btn-add"  class="btn btn-default">
						<i class="fa fa-users"></i>
						Add Account
					</a>
				</div>
				<div class="col-md-4 col-sm-6">
					<a href="#" id="btn-widthraw"  class="btn btn-default">
						<i class="fa fa-money"></i>
						Widthraw Money
					</a>
				</div>
				<div class="col-md-4 col-sm-6">
					<a href="#" id="btn-upgrade" class="btn btn-default">
						<i class="fa fa-graduation-cap"></i>
						Upgrade Account
					</a>
				</div>
			</div>
		</div>
		<div class="side col-md-4">
			<h3>Accumulated Commissions</h3>
			<?php if($_SESSION['user_type']<=2) : ?>
				<div class="row">
					<div class="col-md-6"><strong>Weekly</strong><br /><span class="value">P<?php echo $_SESSION['weekly'] ?></span></div>
					<div class="col-md-6"><strong>Monthly</strong><br /><span class="value">P<?php echo $_SESSION['monthly'] ?></span></div>
					<div class="col-md-6"><strong>Total</strong><br /><span class="value">P<?php echo $_SESSION['total'] ?></span></div>
					<div class="col-md-6"><strong>Current Balance</strong><br /><span class="value">P<?php echo $_SESSION['current'] ?></span></div>
				</div>
			<?php else: ?>
				<div class="row">
					<div class="col-md-4"><strong>Weekly</strong><br /><span class="value">P<?php echo $_SESSION['weekly'] ?></span></div>
					<div class="col-md-4"><strong>Monthly</strong><br /><span class="value">P<?php echo $_SESSION['monthly'] ?></span></div>
					<div class="col-md-4"><strong>Profit Share(<?php echo Func::investor_share_percentage(); ?>%)</strong><br /><span class="value">P<?php echo $_SESSION['investor_share'] ?></span></div>
					<div class="col-md-6"><strong>Total</strong><br /><span class="value">P<?php echo $_SESSION['total'] ?></span></div>
					<div class="col-md-6"><strong>Current Balance</strong><br /><span class="value">P<?php echo $_SESSION['current'] ?></span></div>
				</div>
			<?php endif; ?>
		</div>
	</div>

	<div class="block clearfix">
		<?php
		/* FOR FUTURE
		<div class="pull-right">
			<a href="#" class="btn btn-default"><i class="fa fa-money"></i> Widthraw</a>
			<a href="#" class="btn btn-default"><i class="fa fa-shopping-cart"></i> Purchase</a>
		</div>
		<br style="clear:both;" />
		*/ ?>
		<table id="transaction-table" class="table table-striped">
			<thead>
				<tr>
					<th>Date</th>
					<th>Type of Transaction</th>
					<th>Type of Payment</th>
					<th>Trans # / Receipt</th>
					<th>Status</th>
					<th>Note</th>
					<th>Amount</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				if($transaction_details!=false && count($transaction_details)>0) :
				foreach($transaction_details as $trans) :
					$status = status($trans['status']);
					switch($trans['type']){
						case 1 : 
							$trans_type="Registration"; 
							$transno_or_receipt = '<a href="'.Func::get_secured_image($trans['receipt_picture'],0).'" target="_blank"><img width="20" src="'.Func::get_secured_image($trans['receipt_picture'],0).'" /></a>';		
							break;
						case 2 : 
							$trans_type="Upgrade"; 
							$transno_or_receipt = UPGRADE_TRANSACTION_PREFIX.$trans['ID'];
							break;
						case 3 : $trans_type="Widthraw";
							$transno_or_receipt = WIDTHRAW_TRANSACTION_PREFIX.$trans['ID'];
							break;
						case 4 : $trans_type="Add Accounts";
							$transno_or_receipt = ADD_ACCOUNT_TRANSACTION_PREFIX.$trans['ID'];
							break;
					}
				?>
				<tr>
					<td><?php echo $trans['date_transaction'] ?></td>
					<td><?php echo $trans_type ?></td>
					<td><?php echo $trans['payment_option'] ?></td>
					<td><?php echo $transno_or_receipt ?></td>
					<td><?php echo $status ?></td>
					<td><?php echo $trans['remarks'] ?></td>
					<td><?php echo Func::to_money($trans['gross']); ?></td>
				</tr>
				<?php 
				endforeach;
				else: ?>
				<tr>
					<td colspan="7">You do not have corresponding data for this view yet.</td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
		<?php
		/* FOR FUTURE
		<a class="btn btn-default pull-right" href="#">Load More</a>
		*/ ?>
	</div>
	<br />
	<!-- <div class="block">
		<div class="alert alert-warning fade in" role="alert">
			<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<strong>Important Note</strong>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sapien nisi, pellentesque ac justo viverra, egestas consequat nibh. Pellentesque eros lorem, sollicitudin eu odio in, porttitor efficitur risus. In vitae ligula non tellus congue pellentesque. Aenean mattis mauris et augue tempor bibendum. Nullam felis odio, elementum vehicula interdum et, condimentum nec sapien. Vivamus dignissim quam vulputate ex tristique, id tristique orci imperdiet.</p>
			<br />
			<ul>
				<li>For bank accounts (BDO, or UnionBank), your full name, branch, and account number are required.</li>
				<li>For BDO cash cards, your full name, card number, and cellphone number are required.</li>
				<li>For Smart Money, your 16-digit card number is required (NOT your Smart cellphone number).</li>
				<li>For Globe GCash, your Globe cellphone number is required (must be registered as a GCash phone number).</li>
				<li>For MoneyGram,	LBC, MLhuillier, and Cebuana Lhuillier, your full name, town or city, and country of residence are required.</li>
				<li>For Payza, your registered Payza email address is required.</li>
			</ul>
	    </div>
	</div> -->
</div>