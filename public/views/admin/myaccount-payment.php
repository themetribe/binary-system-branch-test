<div class="block contents" id="payment-cont">
	<?php if(!$has_submitted_payment_trans || $is_all_declined): ?>
		<video controls autoplay="" id="video-opener" preload="auto" canplay="true" class="is-horizontal" style="width:750px;">
	        <source src="<?php echo SITE_URL ?>/dashboard?t=site_tour&img=Payment Vid.mp4" type="video/mp4">
	    </video>
		<div id="steps">
			<form action="" method="post" enctype="multipart/form-data">
				<input type="hidden" name="a" value="payment" />
				<div class="step" id="step1">
					<h3>Step 1 : Choose how many accounts</h3>
					<div class="form-group">				
						
						<?php if($_SESSION['user_type']<=2) : ?>	
						<select name="howmany_accounts" class="form-control">
							<option value="">--[choose option]--</option>						
							<option value="1">Single (1)</option>
							<option value="3">Triple (3)</option>
							<option value="7">Seven (7)</option>
						</select>
						<?php else: ?>
						<select name="howmany_accounts" class="form-control">
							<option value="">--[choose option]--</option>	
							<option value="1">Silver (w/ 3 Premier acc)</option>
							<option value="2">Gold (w/ 3 Premier acc)</option>
							<option value="3">Diamond (w/ 3 Premier acc)</option>
						</select>
						<?php endif; ?>
						
					</div>
				</div>
				<div class="step" id="step2">
					<h3>Amount to be Paid : <span>-</span></h3>
				</div>
				<div class="step" id="step3">
					<h3>Step 2 : Choose your payment Option</h3>
					<div class="form-group">
						<select name="payment_option" class="form-control">
							<?php echo Func::transaction_outputs(); ?>
						</select>
					</div>
				</div>
				<div class="step" id="step4">
					<h3>Step 3 : Send your payment to</h3>

					<div id="bpi-details">
						<strong>Payment Details</strong>
						<ul>
							<li>Account Name : Inlight Marketing</li>
							<li>Account Number : 9459-2342-73</li>
						</ul>
					</div>
					<div id="bdo-details">
						<strong>Payment Details</strong>
						<ul>
							<li>Account Detail 1</li>
							<li>Account Detail 2</li>
						</ul>
					</div>
					<div id="unionbank-details">
						<strong>Payment Details</strong>
						<ul>
							<li>Account Detail 1</li>
							<li>Account Detail 2</li>
						</ul>
					</div>
					<div id="lbc-details">
						<strong>Payment Details</strong>
						<ul>
							<li><strong>Name:</strong> Kurt Roy R. Giger</li>
							<li><strong>Contact:</strong> 09107441801</li>
							<li><strong>Address:</strong> Blk 9 Lot 2 Freedom St. SGR Vilage Catalunan Grande Davao City</li>
						</ul>
					</div>
					<div id="cebuana-details">
						<strong>Payment Details</strong>
						<ul>
							<li><strong>Name:</strong> Kurt Roy R. Giger</li>
							<li><strong>Contact:</strong> 09107441801</li>
							<li><strong>Address:</strong> Blk 9 Lot 2 Freedom St. SGR Vilage Catalunan Grande Davao City</li>
						</ul>
					</div>
					<div id="palawan-exp-details">
						<strong>Payment Details</strong>
						<ul>
							<li><strong>Name:</strong> Kurt Roy R. Giger</li>
							<li><strong>Contact:</strong> 09107441801</li>
							<li><strong>Address:</strong> Blk 9 Lot 2 Freedom St. SGR Vilage Catalunan Grande Davao City</li>
						</ul>
					</div>
					<div id="mlh-details">
						<strong>Payment Details</strong>
						<ul>
							<li><strong>Name:</strong> Kurt Roy R. Giger</li>
							<li><strong>Contact:</strong> 09107441801</li>
							<li><strong>Address:</strong> Blk 9 Lot 2 Freedom St. SGR Vilage Catalunan Grande Davao City</li>
						</ul>
					</div>
					<div id="westernunion-details">
						<strong>Payment Details</strong>
						<ul>
							<li><strong>Name:</strong> Kurt Roy R. Giger</li>
							<li><strong>Contact:</strong> 09107441801</li>
							<li><strong>Address:</strong> Blk 9 Lot 2 Freedom St. SGR Vilage Catalunan Grande Davao City</li>
						</ul>
					</div>
					<div id="moneygram-details">
						<strong>Payment Details</strong>
						<ul>
							<li><strong>Name:</strong> Kurt Roy R. Giger</li>
							<li><strong>Contact:</strong> 09107441801</li>
							<li><strong>Address:</strong> Blk 9 Lot 2 Freedom St. SGR Vilage Catalunan Grande Davao City</li>
						</ul>
					</div>
					<div id="rcbc-details">
						<strong>Payment Details</strong>
						<ul>
							<li>Account Detail 1</li>
							<li>Account Detail 2</li>
						</ul>
					</div>
					<div id="rdps-details">
						<strong>Payment Details</strong>
						<ul>
							<li><strong>Name:</strong> Kurt Roy R. Giger</li>
							<li><strong>Contact:</strong> 09107441801</li>
							<li><strong>Address:</strong> Blk 9 Lot 2 Freedom St. SGR Vilage Catalunan Grande Davao City</li>
						</ul>
					</div>
				</div>
				<div class="step" id="step5">
					<h3>Step 4 : Fill up the form to verify your payment</h3>
					<table class="table" style="width: 100%;">
						<tr>
							<td width="250">
								<div class="form-group">
									<label>Date Sent:</label>
									<input type="text" class="form-control datepicker" name="date_sent" data-date-format="yyyy-mm-dd" required />																		
								</div>								
							</td>
							<td style="display: block;position: relative;">
								<div class="form-group">
									<label>Time Sent:</label><br style="clear:both;" />
									<select name="time_sent_h" class="form-control" required style="margin-right: 10px;width: 72px;padding-left:0; padding-right:0;float: left;">
										<option value="">Hour(s)</option>
										<?php for($x=1;$x<=12;$x++) { echo "<option>$x</option>"; } ?>
									</select>
									<select name="time_sent_m" class="form-control" required style="margin-right: 10px;width: 65px; float:left;padding-left:0; padding-right:0;">
										<option value="">Min(s)</option>
										<?php for($x=1;$x<=60;$x++) { echo "<option>$x</option>"; } ?>
									</select>
									<select name="time_sent_ap" class="form-control" required style="float:left; width:75px;padding-left:0; padding-right:0;">
										<option value="">AM/PM</option>
										<option>AM</option>
										<option>PM</option>
									</select>
									<span class="help-block" style="position: absolute;top: 3px;right: 20px;">HH:MM AM/PM</span>
								</div>								
							</td>
						</tr>
						<tr>
							<td>
								<div class="form-group">
									<label>Amount Sent:</label>
									<input type="number" class="form-control" name="amount_sent" required />																		
								</div>								
							</td>
							<td>
								<div class="form-group">
									<label>Reference Number:</label>
									<input type="text" class="form-control" name="reference_number" required />																		
								</div>								
							</td>
						</tr>
						<!-- wire -->
						<tr class="wire">
							<td colspan="2">
								<div class="form-group">
									<label>Sender's Name:</label>
									<input type="text" class="form-control" name="sender_name" />																		
								</div>
							</td>
						</tr>
						<tr class="wire">
							<td colspan="2">
								<div class="form-group">
									<label>Country of Origin:</label>
									<input type="text" class="form-control" name="country_origin" />																		
								</div>
							</td>
						</tr>
						<tr class="wire">
							<td colspan="2">
								<div class="form-group">
									<label>City / Province of Origin:</label>
									<input type="text" class="form-control" name="city_province_origin" />																		
								</div>
							</td>
						</tr>
						<tr class="wire">
							<td colspan="2">
								<div class="form-group">
									<label>Mobile Number:</label>
									<input type="text" class="form-control" name="mobile_number" />																		
								</div>
							</td>
						</tr>
						<!-- end wire -->
						<!-- bank -->
						<tr class="bank">
							<td colspan="2">
								<div class="form-group">
									<label>Bank Branch of Origin:</label>
									<input type="text" class="form-control" name="bank_branch" />																		
								</div>
							</td>
						</tr>
						<!-- end bank -->
						<tr>
							<td colspan="2">
								<div class="form-group">
									<label>Your Name:</label>
									<input type="text" class="form-control disabled" id="your_name" name="your_name" value="" disabled />
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<div class="form-group">
									<label>Your Sponsor:</label>
									<input type="hidden" class="form-control disabled" name="your_sponsor" id="your_sponsor_id" value="" disabled />
									<input type="text" class="form-control disabled" id="your_sponsor_name" value="" disabled/>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								Your IP Address : <span id="ipaddress">112...</span>								
							</td>
							<td>
								<input id="btn-step3" type="submit" value="Submit And Continue to Step 5" class="btn btn-primary" />								
							</td>
						</tr>
					</table>
					<div class="banktype">

					</div>
				</div>
				<div class="step" id="step6">
					<h3>Step 5 : Upload a copy of the deposit slip / remittance receipt</h3>
					<input type="file" name="payment_picture_proof" id="payment_picture_proof" />
					<img src="" id="payment_picture_proof_prev" />
					<input type="submit" id="btn-submit" name="payment-submit" class="btn btn-primary btn-lg" value="Submit" />
				</div>
				
			</form>
		</div>
	<?php else : ?>
		<video controls autoplay="" id="video-opener" preload="auto" canplay="true" class="is-horizontal" style="width:750px;">
	        <source src="<?php echo SITE_URL ?>/dashboard?t=site_tour&img=After Payment Vid.mp4" type="video/mp4">
	    </video>
	<?php endif; ?>

	<?php if($has_submitted_payment_trans) : ?>		
		<table id="tblpaymenttrans" class="table table-bordered">
			<thead>
				<tr>
					<th>Date</th>
					<th>Type of Payment</th>
					<th>Trans Receipt</th>
					<th>Status</th>
					<th>Note</th>
					<th>Amount</th>
				</tr>
			</thead>
			<tbody>		
				<?php 
				foreach($trans_data as $td) :
					if($td['status']==1){
						$status = "Completed";
						$rowstatus = "success";
					}
					elseif($td['status']==2){
						$status = "Declined";
						$rowstatus = "danger";
					}
					else{
						$status = "On Process";
						$rowstatus = "onprocess";
					}

				?>		
				<tr class="<?php echo $rowstatus; ?>">
					<td><?php echo $td['date_sent'] ?></td>
					<td><?php echo $td['payment_option'] ?></td>
					<td><a href="<?php echo Func::get_secured_image($td['receipt_picture']) ?>" target="_blank"><small><?php echo $td['trans_no'] ?></small><img width="180" src="<?php echo Func::get_secured_image($td['receipt_picture']) ?>" /></a></td>
					<td><strong><?php echo $status ?></strong></td>
					<td><?php echo $td['remarks'] ?></td>
					<td><?php echo Func::to_money($td['gross']); ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	<?php endif; ?>

</div>