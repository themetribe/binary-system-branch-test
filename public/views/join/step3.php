<div id="join-step-3" class="join-containers">
	<h2>Fill Up The Form (Step 1)</h2>
	<div class="form1">	
		<hr />
		<h3>Account Info</h3>
		<div class="form-group">
			<label for="">Plan</label>
			<?php if(isset($_GET['t']) && $_GET['t']==3) : ?>	
				<select name="account_type" id="txt_plan" class="form-control">
					<optgroup label="Investors">
						<option value="3" class="silver" selected>Silver</option>
						<option value="4">Gold</option>
						<option value="5">Diamond</option>
					</optgroup>
				</select>
			<?php else: ?>
				<select name="account_type" id="txt_plan" class="form-control">
					<optgroup label="Affiliate" class="firstop">
						<option class="basic" value="1" disabled>Basic</option>
						<option class="premium" value="2" selected>Premier</option>
					</optgroup>
					<optgroup label="Investors" disabled>
						<option class="3" value="silver">Silver</option>
						<option value="4">Gold</option>
						<option value="5">Diamond</option>
					</optgroup>
				</select>
			<?php endif; ?>			
		</div>
		<div class="form-group">
			<label for="">Create Username</label>
			<input name="username" type="text" class="form-control" id="username" placeholder="" required>
		</div>
		<div class="form-group">
			<label for="">Create Password</label>
			<input name="password" type="password" class="form-control" id="password" placeholder="" required>
		</div>
		<div class="form-group">
			<label for="">Re-Type Password</label>
			<input type="password" class="form-control" id="conf-password" required>
		</div>	
		<hr />
		<h3>Personal Info</h3>
		<div class="form-group">
			<label for="">First Name</label>
			<input name="fname" type="text" class="form-control" value="" id="" placeholder="" required>
		</div>
		<div class="form-group">
			<label for="">Middle Name</label>
			<input name="mname" type="text" class="form-control" value="" id="" placeholder="" required>
		</div>
		<div class="form-group">
			<label for="">Last Name</label>
			<input name="lname" type="text" class="form-control" value="" id="" placeholder="" required>
		</div>

		<div class="form-group">
			<label for="">Birth Date</label>
			<input name="bdate" type="text" class="form-control datepicker" id="" placeholder="" required>
		</div>
		<div class="form-group">
			<label for="">Gender</label>
			<select name="gender" class="form-control" required>
				<option value="male">Male</option>
				<option value="female">Female</option>
			</select>				
		</div>
		<div class="form-group">
			<label for="">Cell No</label>
			<input name="phone" type="text" class="form-control" value="" id="" placeholder="" required>
		</div>
		<div class="form-group">
			<label for="">Email Address</label>
			<input name="email" type="email" class="form-control" value="" id="" placeholder="" required>
		</div>
		<div class="form-group">
			<label for="">Address</label>
			<textarea name="address" class="form-control" required></textarea>
		</div>
		<div class="form-group">
			<label for="">Island Group</label>
			<select name="island_group" class="form-control" required>
				<option value="">--[Choose Island Group]--</option>
				<option value="luzon">Luzon</option>
				<option value="visayas">Visayas</option>
				<option value="mindanao">Mindanao</option>
			</select>				
		</div>		
		<hr />
		<h3>Sponsor Details</h3>
		<div class="form-group">
			<label for="">Group</label>
			<select name="sposition" class="form-control">
				<option value="1">Left</option>
				<option value="2">Right</option>
			</select>
		</div>	
		<div class="form-group">
			<label for="">Sponsor No.</label>
			<input name="sponsor_id" type="text" class="form-control" id="" value="<?php echo $sponsor_id ?>" placeholder="" disabled>
		</div>
		<div class="form-group">
			<label for="">Sponsor Name</label>
			<input name="sponsor_name" type="text" class="form-control" id="" value="<?php echo $sponsor_name ?>" placeholder="" disabled>
		</div>
		<hr />
		<div class="form1">	
			<div class="alert alert-warning fade in" role="alert">			
				<strong>Review Policies</strong> <br />
				You must read and accept the Registration Agreement and Inlight Policies &amp; Procedures when registering your name.
			</div>
			<div class="checkbox">
				<label>
					<input type="checkbox" required> I have read and accept the Registration Agreement and Inlight <a href="#">Policies &amp; Procedure</a>.
				</label>
			</div>
			
			<input type="submit" class="btn btn-primary btn-submit" value="Register Me" />
		</div>
	</div>
</div>