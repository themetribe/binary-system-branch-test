<div id="join-step-2" class="join-containers">
	<h2>Watch the video below and learn all the details.</h2>
	<p>To join in Inlight Marketing, simply become an affiliate by selecting basic Premier or Investor membership.</p>
	<?php if(!isset($_GET['t']) || $_GET['t']!=3) : ?>
	<div class="buttons">
		<a href="#" data-ytid="RL-lfcihHvU" <?php /*data-type="basic"*/ ?> id="btn-basic" class="btn btn-default center"><i class="fa fa-bicycle"></i> Basic</a>					
		<a href="#" data-ytid="NtnBQ5mDD1U" <?php /*data-type="premier"*/ ?> id='btn-premier' class="active btn btn-default center"><i class="fa fa-car"></i> Premier</a>
		<a href="#" data-ytid="W8ceZlU1dHg" <?php /*data-type="investor"*/ ?> id='btn-investor' class="btn btn-default center"><i class="fa fa-plane"></i> Investor</a>		
	</div>
	<?php endif; ?>
	<div class="monitor">
		<!-- <span class="shadow"></span> -->

		<?php if(isset($_GET['t']) && $_GET['t']==3) : ?>
			<iframe src="//www.youtube.com/embed/W8ceZlU1dHg?rel=0" frameborder="0" allowfullscreen></iframe>
		<?php else: ?>	
			<video controls id="video-premier" preload="auto" canplay="true" class="is-horizontal" style="width:802px;">
		        <source src="assets/InlightPremierVid.mp4" type="video/mp4">
		    </video>
		<?php endif; ?>

		
	</div>
	
	<a href="#" class="btn btn-primary center">Click Here to Join Now</a>
</div>