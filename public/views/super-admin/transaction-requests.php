<div id="transaction-requests" class="main">
	<h4>Transaction Request</h4>
	<table class="table">
		<tr>
			<th>Trans ID</th>
			<th>User ID</th>
			<th>Fullname</th>
			<th>Amount</th>
			<th>Request Type</th>
			<th>Account Type</th>
			<th>Request Date</th>
			<th>Proof / Pic</th>
			<th>Status</th>
			<th>Action</th>
		</tr>
		<?php 
		if(isset($requests) && count($requests)>0) :
			//print_r($requests);
		foreach($requests as $request) : ?>
			<tr>
				<td><?php echo $request['trans_ID'] ?></td>
				<td><?php echo $request['user_ID'] ?></td>
				<td><?php echo $request['user_fullname'] ?></td>
				<td><?php echo Func::to_money($request['gross']) ?></td>
				<td><?php echo $request['req_type'] ?></td>
				<td><?php echo $request['account_type']." ".$request['reg_how_many'] ?></td>
				<td><?php echo $request['req_date'] ?></td>
				<td>
					<?php 
					echo '<a href="'.Func::get_secured_image($request['proof'],0).'" target="_blank"><img style="max-width: 200px;" src="'.Func::get_secured_image($request['proof'],0).'" /></a>';
					?>
				</td>
				<td><?php echo $request['status'] ?></td>

				<?php if($request['status']!="Completed" && $request['status']!="Declined") : ?> 
					<td><a href="?tid=<?php echo $request['trans_ID'] ?>&type=<?php echo $request['req_type'] ?>" class="btn btn-primary">Activate</a></td>
				<?php else: ?>
					<td>-</td>
				<?php endif; ?>

			</tr>
		<?php endforeach; 
		else:
			echo '<tr><td colspan="8">No records</td></tr>';
		endif; ?>
	</table>
</div>