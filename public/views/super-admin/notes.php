<script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>
<script>tinymce.init({selector:'textarea'});</script>

<div id="notes" class="main">
	<form action="<?php echo SITE_URL ?>/notes" method="POST">
		<textarea name="notes" rows="20"><?php echo $notes_data ?></textarea>

		<div class="row" style="margin-top:20px;">
			<div class="col-md-4">
				<input type="submit" value="Save" class="btn btn-primary" />
			</div>
			<div class="col-md-4">
				<a href="<?php echo SITE_URL ?>/notes" class="btn btn-default">Cancel</a>
			</div>
		</div>
	</form>
</div>