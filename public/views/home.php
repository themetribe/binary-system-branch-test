<div id="homepage" class="block hero-full full-height Vcenter">
	<span class="filter"></span>
	<video autoplay="" id="video-moment-0" preload="auto" loop="loop" canplay="true" class="is-horizontal">
        <source src="assets/IntroVid.mp4" type="video/mp4">
    </video>
	<div class="container">
		<img src="<?php echo SITE_URL  ?>/assets/img/logo.png" class="img-circle">
		<h1>INLIGHT<br /> MARKETING</h1>
		<p class="sub-heading">A clear path to wealth and abundance.</p>
		<button class="btn btn-default" id="btn_get-started">Get Started</button>
		
		<ul id="menu" class="menu">
			<?php $ishome = true;  include('partials/front-menu.php'); ?>
		</ul>
	</div>      
</div>


<section id="about-us" class="block about-us">
	<div class="container">
		<h1>About Inlight Marketing</h1>
		<p>Inlight Marketing is focusing on the fundamentals of human development and behavior that most affect personal, profession and business performance.</p>
		
		<div class="row" id="mission_vision">
			<div class="col-md-4">
				<h3>Our Mission</h3>
				<p>To shine and to give light to the people. To inspire and motivate through Inlight's outstanding eBooks, services, coaching, trainings and opportunities.
				To deliver wisdom, knowledge and right perspective by duplicating oneself to another. To enlighten the people and let the world know we have a better way.</p>
			</div>
			<div class="col-md-3">
				<h3>Our Vision</h3>
				<p>To change the outlook of every member positively into their lives and careers. A transformed network and duplicated individuals that are inspired and motivated to help other people create a lifestyle of wealth and abundance. </p>
			</div>
			<div class="col-md-5">
				<h3>Our Standards</h3>
				<ul>
					<li><strong>S</strong>ee your goal.</li>
					<li><strong>U</strong>nderstand the obstacles.</li>
					<li><strong>C</strong>reate a positive solution and right perspective.</li>
					<li><strong>C</strong>lear your mind of self distraction, fear and doubt.</li>
					<li><strong>E</strong>mbrace every challenge.</li>
					<li><strong>S</strong>tay on the right path and stay focus.</li>
					<li><strong>S</strong>how the world that you can do it!</li>
				</ul>
			</div>
		</div>

		<div id="founders">

			<div class="row author">
				<div class="col-md-4 col-sm-12">
					<img src="<?php echo SITE_URL ?>/assets/img/Kurt.jpg" style="margin-top: 10px;" />
				</div>
				<div class="col-md-8 col-sm-12">
					<h3>Kurt Roy Giger</h3>
					<h4>President / Founder</h4>
					<p>He came from a very poor family, but that does hinder him to achieve his dreams in life. Instead he used it as a motivation to finish his degree and became a graduate of Bachelor of Science and Information Technology at the University of the Immaculate Conception and was able to finish his CCNA Course in Ateneo De Davao University. He took his first job as a technical Engineer of one of the biggest communication company in the Philippines. And later that job, he decided to take his expertise into new level and went to Dubai to venture out the offered opportunity of Microsoft Corporation. Given on the success he could still acquire, he decided to do something different which leads him to the business world.</p>
					<p>He was inspired by his friends from a network marketing industry that become very successful on their business. He took the challenge with confidence that he can too become successful in the industry. Given the skills and knowledge, Kurt was named the Top 1 Income Earner on his First Month and become a consistent top income earner in his first year. He became a great mentor with outstanding leadership and was able to help other member’s success. But due to unexpected turn of events and with a devastating problem occur. He found himself broke with no money left on his pocket and behind on his promises. But that did not stop him on his dreams that someday he will put up his name back on the top again. But this time, he envisions far greater opportunity and decided to put up his own company. With his passion and great idea, he transformed and created a lifetime opportunity that could help a lot people. Not just by earning but also by promoting the personal development learning system, which he believes is the foundation of human behavior that most affect the personal and business performance.</p>
					<p><strong>Inlight's President Personal Message:</strong><br /> 
					I wish for you a life of wealth, health, and happiness; a life in which you give to yourself the gift of patience, the virtue of reason, the value of knowledge, and the influence of faith in your own ability to dream about and achieve life worthy rewards. 
					</p>
				</div>				
			</div>

			<div class="row author">
				<div class="col-md-8 right col-sm-12">
					<img class="mobile-only" src="<?php echo SITE_URL ?>/assets/img/Jr.jpg" />
					<h3>Jimmy Giger Jr.</h3>
					<h4>Head Pioneer Associate / Co-Founder</h4>
					<p>Jimmy is a young businessman with great leadership performance.  He is taking up BSBA-Marketing Management and become a Student Entrepreneur Leader since 2012. <br />
					He is the middle child in their family. At the age of 16, he started looking for a job to help his family with their finances. But he failed to do so.
					<br /> He was still at his young age and they didn't accept a 16 year old worker.</p>
					<p>Therefore, he decided to look for another opportunity. Fortunately, he found Network Marketing and he earned a lot of money. He fails many times to build a network at his young age. But after a year it was his 1st break to build a network and become successful. He continue to dream big and never give up on it and now he was given a chance as one of the co-founder of this company to deliver his passion to help a lot of people and lead them to become financially free. He is very excited to let the world knows that Inlight Marketing is the future trend of business world. </p>
					<p><strong>Inlight’s Co-Founder Personal Message: </strong><br />
					Whatever you can do or Dream you can do. Begin It!</p>
					
				</div>
				<div class="col-md-4 col-sm-12 desktop-only">
					<img src="<?php echo SITE_URL ?>/assets/img/Jr.jpg" />
				</div>
			</div>			

			<div class="row author">
				<div class="col-md-4 col-sm-12">
					<img src="<?php echo SITE_URL ?>/assets/img/Francis.jpg" />
				</div>
				<div class="col-md-8 col-sm-12">
					<h3>Francis Albores</h3>
					<h4>Admin Associate / Co-Founder</h4>
					<p>A BSIT degree holder of University of Immaculate Conception has immediately worked as a Web Programmer in Offsourcing Inc., a local outsourcing company day before graduation. He's drive force was not just for the love of programming but to help his family with financial stability as well. He begun working multiple jobs and became a Teacher in ACLC in various programming subjects. <br />
					After 3 years, he quitted his first job and pursue as a business partner with one of his high school close friend on a company named WPCrunchy. With his love to become a team leader, he departed himself to the company and have started an outsourcing group of programmers called ThemeTribe. Later, he was invited and was hired back to Offsourcing Inc while he's business was still running.</p>
					<p>After a year, he founded WebArtisan. But recently, Kurt introduces the idea of Network Marketing and Personal Development. Amazed with the beauty of the idea, and was able to find the answer of what he wants to do in his life; to achieve his dreams in life and for his family. He is now prepared to take his passion to other level and very excited to help other people to achieve their dreams as well. Francis is now ready to take a new leap of his career and willing to take the risk to achieve his dreams.</p>
					<p><strong>Inlight’s Co-Founder Personal Message:</strong><br />
					SUCCESS is something you attract by the person you BECOME </p>
				</div>				
			</div>

		</div>

		
	</div>
</section>


<section id="product" class="block product">
	<div class="container">
		<h1>Products</h1>
		<div class="row">
			<div class="col-md-12">
				<p>Inlight Marketing main products are eBooks, Audios and Videos. We are offering you the ultimate collections of high value, unique and competitive products of all time and with the best learning system you could possibly get. We make sure that what you get in our company will give you something that no money can buy. An extra ordinary learning will take place within you that will awaken your maximum potential of becoming a successful person.</p>
				<img src="<?php echo SITE_URL ?>/assets/img/websites.png">
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<p>Our products are organized and categorized depending on your package level. We prepared a unique step by step learning system that you can follow. It will guide you from start to finish by selecting which career path you will take.</p>
			</div>
			<div class="col-md-6">
				<img style="width: 100%;margin: 0;" src="<?php echo SITE_URL ?>/assets/img/home_video.png">
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<img src="<?php echo SITE_URL ?>/assets/img/book.png" style="width: 300px;">
			</div>
			<div class="col-md-6">
				<p>We offer you an additional advanced modules and handbooks once you are in the premier level.</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<p>And the most exciting privilege you could get as a member is the trainings, seminars and webinar that we provide, which covers the topic on how to become successful in 5 aspects of your life; Spiritual, Financial, Intellectual, Physical and Relational. And it covers modulated topics of each aspect; like businesses, entrepreneurs, personal development, internet marketing, network marketing, concept, strategies and many more. All this will be given for free once you become an affiliate in our company. And we assure you that your stay in our company will be worthwhile. </p>
			</div>
			<div class="col-md-6">
				<img src="<?php echo SITE_URL ?>/assets/img/home_audio.png" style="width: 300px;">
			</div>
		</div>
	</div>
</section>


<section id="service" class="block service">
	<div class="container">
		<h1>Services</h1>
		<div class="row">
			<div class="col-md-3">
				<i class="fa fa-circle-o-notch"></i>
				<h4>Automated Marketing Funnel</h4>
					<p>Automated sales and marketing funnel design to help you make more money 24/7.</p>
			</div>	
			<div class="col-md-3">
				<i class="fa fa-play-circle-o"></i>
				<h4>Done For You Sales Videos</h4>
					<p>Not good in selling? No problem! Let us do the hard work for you.</p>
			</div>		
			<div class="col-md-3">
				<i class="fa fa-envelope"></i>
				<h4>Integrated Follow Up System</h4>
				<p>We help you make more sales using our integrated and automated email follow up system.</p>
			</div>	
			<div class="col-md-3">
				<i class="fa fa-coffee"></i>
				<h4>Commission Revolution</h4>
				<p>We pay the highest percentage of affiliate commission out there.</p>
			</div>	
		</div>
		<br />
		<div class="row">
			<div class="col-md-3">
				<i class="fa fa-users"></i>
				<h4>Automated Marketing Funnel</h4>
					<p>Automated sales and marketing funnel design to help you make more money 24/7.</p>
			</div>	
			<div class="col-md-3">
				<i class="fa fa-desktop"></i>
				<h4>Done For You Sales Videos</h4>
					<p>Not good in selling? No problem! Let us do the hard work for you.</p>
			</div>		
			<div class="col-md-3">
				<i class="fa fa-life-ring"></i>
				<h4>Email Chat Support</h4>
				<p>We help you make more sales using our integrated and automated email follow up system.</p>
			</div>	
			<div class="col-md-3">
				<i class="fa fa-calendar-o"></i>
				<h4>Life Changing Events</h4>
				<p>We pay the highest percentage of affiliate commission out there.</p>
			</div>	
		</div>
	</div>
</section>

<section id="affiliates" class="block investor">
	<div class="container">
		<h1>Affiliates</h1>
		<p>We are very thankful and honored to serve you here in Inlight Marketing.</p>
	
		<div id="ri-grid" class="ri-grid ri-grid-size-2">
			<ul>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/1.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/2.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/3.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/4.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/5.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/6.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/7.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/8.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/9.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/10.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/11.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/12.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/13.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/14.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/15.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/16.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/17.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/18.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/19.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/20.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/21.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/22.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/23.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/24.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/25.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/26.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/27.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/28.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/29.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/30.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/31.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/32.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/33.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/34.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/35.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/36.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/37.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/38.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/39.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/40.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/41.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/42.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/43.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/44.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/45.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/46.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/47.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/48.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/49.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/50.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/51.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/52.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/53.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/54.jpg"/></a></li>
			</ul>
		</div>
	</div>
</section>

<section id="footer">
	<a href="/legalities">Legalities</a><!--  | <a href="/tandc">Terms and Conditions</a> -->

</section>