

<div id="investors">

	<div class="block hero-full full-height Vcenter">
		<div class="container">
			<ul id="menu" class="menu animated bounceInDown has-skin" style="display: block;">
				<?php include('partials/front-menu.php'); ?>				
			</ul>

		</div>      
	</div>


	<div class="block container">
		<h1>Our Investors</h1>
		<hr />
		<div class="row">
			<div class="col-md-4 investor">
				<img src="assets/img/silver-investor.png" />
				<h2>Become an Investor</h2>
				<h3>Silver</h3>
				<p></p>
			</div>

			<div class="col-md-4 investor">
				<img src="assets/img/gold-investor.png" />
				<h2>Become an Investor</h2>
				<h3>Gold</h3>
				<p></p>
			</div>

			<div class="col-md-4 investor">
				<img src="assets/img/diamond-investor.png" />
				<h2>Become an Investor</h2>
				<h3>Diamond</h3>
				<p></p>
			</div>

			<div class="col-md-4 investor">
				<img src="assets/img/silver-investor.png" />
				<h2>Become an Investor</h2>
				<h3>Silver</h3>
				<p></p>
			</div>

			<div class="col-md-4 investor">
				<img src="assets/img/gold-investor.png" />
				<h2>Become an Investor</h2>
				<h3>Gold</h3>
				<p></p>
			</div>

			<div class="col-md-4 investor">
				<img src="assets/img/diamond-investor.png" />
				<h2>Become an Investor</h2>
				<h3>Diamond</h3>
				<p></p>
			</div>

		</div>
	</div>
</div>