<?php if(!isset($current_page)) $current_page = ""; ?>
<div id="sidebar" class="<?php echo "acctype-{$_SESSION['user_type']}"; ?>">
	<span class="extension"></span>
	<ul>
		<li><a <?php if($current_page=="dashboard") echo 'class="active"'; ?> href="/dashboard"><i class="fa fa-lightbulb-o"></i><span>Dashboard</span></a></li>
		<li class="desktop-only"><a <?php if($current_page=="geneology") echo 'class="active"'; ?> href="/geneology"><i class="fa fa-line-chart"></i><span>Geneology</span></a></li>
		<li><a <?php if($current_page=="ewallet") echo 'class="active"'; ?> href="/ewallet"><i class="fa fa-cc-mastercard"></i><span>E-Wallet</span></a></li>
		<li><a <?php if($current_page=="library") echo 'class="active"'; ?> href="/library?t=3"><i class="fa fa-book"></i><span>Library</span></a></li>
		<li><a <?php if($current_page=="jobs") echo 'class="active"'; ?> href="/jobs"><i class="fa fa-binoculars"></i><span>Jobs</span></a></li>
		<li><a <?php if($current_page=="support") echo 'class="active"'; ?> href="/support"><i class="fa fa-support"></i><span>Support Ticket</span></a></li>
		<li><a <?php if($current_page=="message") echo 'class="active"'; ?> href="/messages"><?php echo $badge ?><i class="fa fa-wechat"></i><span>Messages</span></a></li>
		<li><a <?php if($current_page=="myaccount") echo 'class="active"'; ?> href="/myaccount"><i class="fa fa-cogs"></i><span>My Account</span></a></li>
		<!-- <li><a href="/profile"><i class="fa fa-user"></i>My Profile</a></li> -->
		<?php 
		 /* FOR ADMIN ONLY */
		 $admin_ids = explode(',',ADMIN_IDS);
		 if(in_array($_SESSION['user_ID'], $admin_ids)){ ?>
		 	<li><a class="admin-page" href="/transaction_requests"><i class="fa fa-users"></i><span>Transaction Requests</span></a></li>
		 	<li><a class="admin-page" href="/notes"><i class="fa fa-codepen"></i><span>Notes</span></a></li>
		 	<?php
		 }

		?>
	</ul>
</div>