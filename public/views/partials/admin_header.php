<?php
if (session_status() == PHP_SESSION_NONE) {
	session_set_cookie_params(0);
	session_start();
}
if(!isset($_SESSION['user_ID'])){
	header('Location:'.SITE_URL);
	exit();
}

// FOR MESSAGE NOTIFICATIONS

$message = new Message();
$badge = $message->getnotification();



?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Inlight Marketing | Dashboard</title>

		<!-- Bootstrap -->
		<link href="<?php echo SITE_URL ?>/assets/css/bootstrap.min.css" rel="stylesheet">
		<link rel="image_src" type="image/jpeg" href="<?php echo SITE_URL ?>/assets/img/logo.jpg" />
		<meta property="og:title" content="Inlight Marketing" />
		<meta property="og:description" content="A clear path to wealth and abundance" />
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

		<link href="<?php echo SITE_URL ?>/assets/css/admin-style.css" rel="stylesheet">
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-55451472-1', 'auto');
		  ga('send', 'pageview');

		</script>
	</head>
	<body <?php Func::body_class("acctype-{$_SESSION['user_type']}") ?>>

	<div id="header" class="block clearfix">
		<div id="branding" class="center">
			<a href="<?php echo SITE_URL ?>"><img src="<?php echo SITE_URL ?>/assets/img/logo2.png" id="site-logo" alt="Inlight Unlimited" title="Inlight Unlimited" /></a>
			<i class="shadow"></i>
		</div>
		<div class="pull-right vcenter">			
			<a href="#" id="avatar">
				<img src="<?php echo $_SESSION['user_avatar_src'] ?>" />
			</a>
			<span id="username">Welcome <?php echo $_SESSION['user_fname'] ?>!</span>
			<a href="<?php echo SITE_URL ?>?logout=1" id="btn-logout" class="center"><i class="fa fa-power-off"></i></a>
		</div>
		<div id="headings">
			<img src="<?php echo SITE_URL ?>/assets/img/site-title.png" id="site-title" />
			<p id="sub-title">A clear path to wealth and abundance</p>
		</div>
		
	</div>