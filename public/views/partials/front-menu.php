<?php 
if(isset($ishome) && $ishome==true) : ?>

<a class="mobile-only" href="#" id="btn-navtoggle"><i class="fa fa-bars"></i></a>
<img class="logo-blue desktop-only" src="<?php echo SITE_URL ?>/assets/img/logo-blue.png">
<li><a class="btn btn-default" href="#about-us">About Us</a></li>
<li><a class="btn btn-default" href="#product">Products</a></li>
<li><a class="btn btn-default" href="#service">Services</a></li>
<li><a class="btn btn-default" href="<?php echo SITE_URL ?>/join">Opportunity</a></li>
<li><a class="btn btn-default" href="#affiliates">Affiliates</a></li>
<li><a class="btn btn-default" href="<?php echo SITE_URL ?>/investors">Investors</a></li>
<li><a class="btn btn-default" href="<?php echo SITE_URL ?>/support2">Support</a></li>
<li><a id="btnlogin" class="btn btn-default" href="#">Member Login</a></li>

<?php 
else:
?>

<a class="mobile-only" href="#" id="btn-navtoggle"><i class="fa fa-bars"></i></a>
<img class="logo-blue desktop-only" src="<?php echo SITE_URL ?>/assets/img/logo-blue.png">
<li><a class="" href="../#about-us">About Us</a></li>
<li><a class="" href="../#product">Products</a></li>
<li><a class="" href="../#service">Services</a></li>
<li><a class="" href="../#opportunity">Opportunity</a></li>
<li><a class="" href="../#affiliates">Affiliates</a></li>
<li><a class="" href="<?php echo SITE_URL ?>/investors">Investors</a></li>
<li><a class="" href="<?php echo SITE_URL ?>/support2">Support</a></li>
<li><a id="btnlogin" class="" href="#">Member Login</a></li>

<?php endif; ?>