<?php
if (session_status() == PHP_SESSION_NONE) {
	session_start();
}
if(isset($_SESSION['user_ID'])){
	header('Location:'.SITE_URL.'/dashboard');
	exit();
}

// Random OGG Image
$ogImage = array(
		SITE_URL."/assets/img/ogimg1-1.png",
		SITE_URL."/assets/img/ogimg1-2.png",
		SITE_URL."/assets/img/ogimg1-3.png",
		SITE_URL."/assets/img/ogimg2-1.png",
		SITE_URL."/assets/img/ogimg2-2.png",
		SITE_URL."/assets/img/ogimg2-3.png",
		SITE_URL."/assets/img/IM_ShareImage1.png",
		SITE_URL."/assets/img/IM_ShareImage2.png"
	);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=640">
		<title>Inlight Marketing</title>

		<!-- Bootstrap -->
		<link href="<?php echo SITE_URL ?>/assets/css/bootstrap.min.css" rel="stylesheet">

		<link rel="Facebook image" type="image/jpeg" href="<?php echo SITE_URL ?>/assets/img/logo.jpg" />
		<meta property="og:image" content="<?php echo $ogImage[rand(0, 7)] ?>" />
		<meta property="og:title" content="Inlight Marketing" />
		<meta property="og:description" content="A clear path to wealth and abundance" />

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

		<link href="<?php echo SITE_URL ?>/assets/css/jquery-grid-style.css" rel="stylesheet">
		<link href="<?php echo SITE_URL ?>/assets/css/animate.css" rel="stylesheet">
		<link href="<?php echo SITE_URL ?>/assets/css/site.css" rel="stylesheet">
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-55451472-1', 'auto');
		  ga('send', 'pageview');

		</script>
	</head>
	<body <?php Func::body_class("front") ?>>