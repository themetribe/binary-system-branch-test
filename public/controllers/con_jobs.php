 <?php
session_start();
Func::inactive_blocker();

include("views/partials/admin_header.php");
$current_page = "jobs";
include("views/partials/admin_sidebar.php");
include("views/admin/jobs.php");
function script() { ?>
	<script>
		(function(global, $){ $(document).ready(function(){
			Jobs.listener($);
		}); })(window, jQuery);
		var Jobs = {
			listener : function($){
				$("#jobs li a").on('click',function(){

					Modal._title = "Job Detail";
					Modal.hasHeader = true;
					Modal.hasButton = false;
					Modal.addId = "job_detail_modal";
					Modal.contents = $( $(this).data('content') ).html();
					Modal.show($);

					$("#job_detail_modal").on('hidden.bs.modal', function (e) {
						console.log('remove');
					  $("#job_detail_modal").remove();
					})
				});

			}
		}
	</script> <?php
} 
Func::footer_hook('script');
include("views/partials/admin_footer.php");