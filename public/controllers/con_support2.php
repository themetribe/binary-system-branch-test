<?php

include("views/partials/html_head.php");
include("views/support.php");
function script() { ?>
	<script>
		(function(global, $){ $(document).ready(function(){
			<?php if(isset($_GET['logout']) && $_GET['logout']==2) : ?>
			show_login : function(_this){
				Modal._title = "Affiliate Login"
				Modal.hasButton = false;
				Modal.addId = "member_login";
				Modal.contents =
					'<form role="form" method="POST">'+
					'	<div class="form-group">'+
					'		<label for="">Username</label>'+
					'		<input type="text" class="form-control" id="" name="username" placeholder="">'+
					'	</div>'+
					'	<div class="form-group">'+
					'		<label for="">Password</label>'+
					'		<input type="password" class="form-control" id="" name="password" placeholder="">'+
					'	</div>'+
					'	<input type="submit" class="btn btn-primary pull-right" value="Login" />'+
					'</form>';
				Modal.show($);
			},
			login_request : function(_this){
				var _data = "a=1&"+$(_this).serialize();;
				$.post("../",_data, function(data){
					if($.trim(data)==1){
						window.location.href="/dashboard?logged=1";
					}
					else if($.trim(data)==2){
						alert('Sorry your account is invalid.');
					}
					close_loader($);
					console.log(data);
				});
			}
			<?php endif; ?>

		}); })(window, jQuery);
	</script>
	<?php
}
Func::footer_hook('script');
include("views/partials/html_foot.php");

?>