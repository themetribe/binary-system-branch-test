<?php
include("models/cls_support.php");
$support = new Support();

if(isset($_POST['btn-send'])){
	$support->send($_POST['email']);
	$sent = true;
}

include("views/partials/admin_header.php");
$current_page = "support";
include("views/partials/admin_sidebar.php");
include("views/admin/support.php");	
include("views/partials/admin_footer.php");
?>