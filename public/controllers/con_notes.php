<?php
session_start();
Func::inactive_blocker();
Func::nonadmin_blocker();

include("models/adm_notes.php");
$notes = new Notes();

if(isset($_POST['notes'])){	
	$notes->set($_POST['notes']);
}

$notes_data = $notes->get();




include("views/partials/admin_header.php");
include("views/partials/admin_sidebar.php");
include("views/super-admin/notes.php");	
include("views/partials/admin_footer.php");
?>