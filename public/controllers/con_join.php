<?php
session_start();
include("models/cls_user.php");
$user = new User();

if(isset($_POST['a'])){

	include("models/cls_geneology.php");
	$geneology = new Geneology();

	$data = $_POST;
	$error = "";	
	
	$user->register($data, $geneology);
	exit();
}

if(!isset($_GET['afid'])){
	$_GET['afid'] = DEFAULT_UPLINE;
}
$sponsor_id = $_GET['afid'];
$user_details = $user->get_current_user_details($_GET['afid']);
$sponsor_name = "{$user_details['fname']} {$user_details['mname']} {$user_details['lname']}";





include("views/partials/html_head.php");



?>
<div id="join-wrapper"> 
	<form role="form" action="" method="POST">
	<?php
	include("views/join/step1.php");	
	include("views/join/step2.php");
	include("views/join/step3.php");	
	?>
	</form>
</div>

















<?php
function script() { ?>
	<script>
		(function(global, $){ $(document).ready(function(){
			Join.listener($);
			Join.step1();
			Join.step2();
			Join.step3();			
		}); })(window, jQuery);

		var Join = {
			that : null, $ : null,
			listener : function($){
				that=this; $ = $;
				$(".join-containers:not(#join-step-1) .btn-primary").on('click',function(){
					if(!$(this).hasClass('btn-submit')){
						next_step = $(this).parents('.join-containers').next('.join-containers');
						next_step.addClass('active');
						$("#join-wrapper").css({
							height:'auto'
						});
						$('html,body').animate({
							 scrollTop: $(next_step).offset().top - 150
			     		}, 500);
						return false;			
					}					
				})
				$(".join-containers .btn-back").on('click',function(){
					that.back_button(this);				
					return false;
				})
			},
			step1 : function(){
				$("#join-step-1 .btn-primary").on('click',function(){
					if(!$(this).hasClass('btn-submit')){
						that.primary_button(this);	
						return false;			
					}	
				});
				$("#btnFirstStep").on('click',function(){
					$("#video-opener").get(0).pause();
					$("#video-premier").get(0).play();
				})
			},
			step2 : function(){
				/*$("#join-step-2 .buttons a").on('click',function(){
					$(".buttons a").removeClass('active');
					$(this).addClass('active');	
					$(".monitor iframe").attr('src','//www.youtube.com/embed/'+$(this).data('ytid')+'?rel=0');

					if($(this).data('type')=="investor"){
						$("#txt_plan").find(".firstop, .firstop *").hide();
						$("#txt_plan .3").attr('selected',true);
					}
					else{
						$("#txt_plan").find(".firstop, .firstop *").show();
						$("#txt_plan ."+$(this).data('type')).attr('selected',true);
					}


				})*/
				$("#btn-premier").on('click',function(){
					$(".monitor").find('img').remove();
					$("#video-premier").fadeIn();
					return false;
				});
				$("#btn-basic").on('click',function(){
					$(".monitor").find('img').remove();
					$(".monitor").append('<img src="<?php echo SITE_URL ?>/assets/img/Basic Package Presentation.png" />');
					$("#video-premier").fadeOut();
					return false;
				});
				$("#btn-investor").on('click',function(){
					$(".monitor").find('img').remove();
					$(".monitor").append('<img src="<?php echo SITE_URL ?>/assets/img/Investors Package Presentation.png" />');
					$("#video-premier").fadeOut();
					return false;
				});
			},
			step3 : function() {
				
				$("form").on('submit',function(event){
					event.preventDefault();
					var valid = true;
					var error = "";
					var element = "";
					
					// check password confirm
					var pass = $("#password").val();
					var pass_c = $("#conf-password").val();
					if( pass != pass_c ){
						error = "Password not match!";
						element = "#password, #conf-password";
						valid=false;
					}
					if(!valid){
						$(element).parent().addClass('has-error')
							.find('label').append("<small>Error : "+error);
						$('html,body').animate({
							 scrollTop: $(element).offset().top - 100
			     		}, 500);
					}
					else{
						that.createUser(this);
					}
					return false;
				});
			},
			createUser : function(_this){
				//show_loader($);
				var _data = "a=1&"+$(_this).serialize();;
				$.post(window.location.href,_data, function(data){
					if($.trim(data)==1){
						window.location.href="/myaccount?new_user=1";
					}
					else if($.trim(data)==2){
						alert('Something is wrong with the transaction. Please contact the site administrator.')
					}
					else if($.trim(data)=="duplicate"){						
						$("#username").parent().addClass('has-error')
							.find('label').append("<small>Error : "+"This username is not available.");
						$("#username").focus();
						$('html,body').animate({
							 scrollTop: $("#username").offset().top - 100
			     		}, 500);

					}
					else{
						alert('Something is wrong with the transaction. Please contact the site administrator.')
					}
					//close_loader($,"body");
					console.log(data);
				});
			},
			primary_button : function(_this){
				$(".join-containers").removeClass('inactive');
				$(".join-containers").removeClass('active');
				$(_this).parents('.join-containers').addClass('inactive');
				setTimeout(function(){
					$('html,body').animate({
						 scrollTop: $("body").offset().top
		     		}, 500);
					next_step = $(_this).parents('.join-containers').next('.join-containers');
					next_step.addClass('active');
					height = next_step.outerHeight()+100;
					$("#join-wrapper").css('height',height);
					
				}, 900);	
			}
		}
	</script>
	<?php
}
Func::footer_hook('script');
include("views/partials/html_foot.php");
?>