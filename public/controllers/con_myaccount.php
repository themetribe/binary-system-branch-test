<?php
session_start();
include("models/cls_transaction.php");
include("models/cls_user.php");

$user = new User();
$usr_details = $user->get_current_user_details();
$user_avatar_src = ($usr_details['avatar_src']!="") ? Func::get_secured_image($usr_details['avatar_src'],false,"primary") : SITE_URL."/assets/img/upline.jpg";

$sponsor_details = $user->get_current_user_details($usr_details['sponsor_id']);
$sponsor_avatar_src = ($sponsor_details['avatar_src']!="") ? Func::get_secured_image($sponsor_details['avatar_src'],false,"primary") : SITE_URL."/assets/img/upline.jpg";


$transaction = new Transaction($_SESSION);
$trans_data = $transaction->get_payment_transaction();
if(count($trans_data)>0 && $trans_data!="") {
	$has_submitted_payment_trans=true;
	$is_all_declined = $transaction->is_all_submitted_payment_trans_declined($trans_data);
}
else{
	$has_submitted_payment_trans=false;
	$is_all_declined=0;
}
if( isset($_POST['a']) ){
	switch ($_POST['a']) {
		case "personal-info":
			$status = $user->save_personal_info($_POST);
			if($status == 1){
				header('Location:'.SITE_URL.'/myaccount?save_note=1');
				exit();
				//must says "update successfull"
			}
			elseif($status==2){
				echo "<script>alert('Image size is too large. Please upload an image file with only 3mb max size.');</script>";
			}
			break;
		case "change-password":

			echo $user->save_password($_POST);
			exit();
			break;

		case "change-pin":

			echo $user->save_pin($_POST);
			exit();
			break;		

		case "payment":			
			if( $transaction->member_payment($_POST) == 1 ){
				header('Location:'.SITE_URL.'/myaccount?tab=4');
			}
			break;
		case "calculate_price":
			switch($_SESSION['user_type']){
				case 1 : 
					$data = BASIC_FEE * $_POST['plan_option'];
					break;
				case 2 :
					$data = PREMIER_FEE * $_POST['plan_option'];
					break;
				case 3 : 
				case 4 : 
				case 5 : 
					switch($_POST['plan_option']){
						case 1 : 
							$data = SILVER_FEE + (PREMIER_FEE * 3);
							break;
						case 2 : 
							$data = GOLD_FEE + (PREMIER_FEE * 3);
							break;
						case 3 : 
							$data = DIAMOND_FEE + (PREMIER_FEE * 3);
							break;
					}
			}
			echo Func::to_money($data);
			exit();
			break;
		default:
			# code...
			break;
	}

}

// ACTIVATION CODES
include("models/cls_activation_codes.php");
$cls_activation_codes = new Activation_Codes();
$activation_codes = $cls_activation_codes->getData();

include("views/partials/admin_header.php");

$current_page = "myaccount";
include("views/partials/admin_sidebar.php");







global $activetab_script;
$activetab_script="";
if(isset($_GET['tab'])){
	switch($_GET['tab']){
		case 4:
			ob_start();
			?>
			<script>
				$(".contents").removeClass('active');
				$("#payment-cont").addClass('active');
			</script>
			<?php
			$activetab_script = ob_get_contents();
			ob_end_clean();
			break;
		case 3:
			ob_start();
			?>
			<script>
				$(".contents, #main-buttons a").removeClass('active');
				$("#activation-cont, #btn-activation").addClass('active');
			</script>
			<?php
			$activetab_script = ob_get_contents();
			ob_end_clean();
			break;
	}
}
if(isset($_GET['new_user'])) :
	
	include("views/admin/welcome.php");
	function script() { ?>
		<script>
			$("#welcome").fadeOut(9000);
			setTimeout(function(){
				window.location.href='/myaccount?tab=4';
			},5000);
		</script>
		<?php
	}
else:
	$db = new DB();
	$user = $db->select("*", array("ID"=>"0000000001"),false,"user");
	//print_r($user);

	if(isset($_POST['btn-update'])){
		$param = array(
			"email" => $_POST['email'],
			"phone" => $_POST['phone'],
			"address" => $_POST['address']
		);

		require("models/cls_user.php");
		$user = new User();
		$update = $user->updateAccount($param);
	}

	include("views/admin/myaccount.php");	
	
	function script() { 
		$user = new User();
		global $activetab_script; 		
		$current_user = $user->get_current_user_details();
		$sponsor_details = $user->get_current_user_details($current_user['sponsor_id']);
		$current_user_fullname = $current_user['fname']." ".$current_user['mname']." ".$current_user['lname'];
		?>
		<script>

			(function(global, $){ $(document).ready(function(){
				MyAccounts.listener($);	
				Security.listener($);
				PersonalInfo.listener($);
				Payment.listener($);
				<?php if(isset($_GET['save_note'])) : ?>
				Message.editSuccess($);
				<?php endif; ?>
				<?php if(isset($_GET['msg'])) {
					echo "Message.inactiveaccount();";
				} ?>
			}); })(window, jQuery);

			var Message = {
				editSuccess : function(){				
					Modal.hasButton = false;
					Modal._title = "Configuration";
					Modal.addId = "messaging-tips";
					Modal.contents = 
						'<h1>Update Successfull</h1>'+
						'<p>Your changes to the database is successfull.</p>';						
					Modal.show($);
				},
				inactiveaccount : function(){
					Modal.hasButton = false;
					Modal._title = "Account Status";
					Modal.addId = "account-status";
					Modal.contents = 
						'<h1>Your account is still inactive.</h1>'+
						'<p>Your account is still not activated. Kindly confirm your transaction. Thank you!</p>'+
						'<h1><i class="fa fa-thumbs-o-down"></i></h1>';
					Modal.show($);					
				}
			}
			var Security = {
				listener : function($){
					$("#frm_changepassword").on('submit',function(e){
						e.preventDefault();
						if($("#new_password").val() == $("#retype_new_password").val()){
							show_loader($);
							var _data = $(this).serialize();
							$.post(window.location.href,_data, function(data){
								data = $.trim(data);
								if(data==1){								
									window.location.href="<?php echo SITE_URL ?>/myaccount?save_note=1";
								}
								else{
									alert('Old password incorrect.');
								}
								close_loader($);
								console.log(data);
							});	
						}
						else{
							alert('New Password mismatched.')
						}
						
					});

					$("#frm_changepin").on('submit',function(e){
						e.preventDefault();
						if($("#new_pin").val() == $("#retype_new_pin").val()){
							show_loader($);
							var _data = $(this).serialize();
							$.post(window.location.href,_data, function(data){
								data = $.trim(data);
								if(data==1){								
									window.location.href="<?php echo SITE_URL ?>/myaccount?save_note=1";
								}
								else{
									alert('Old PIN incorrect.');
								}
								close_loader($);
								console.log(data);
							});
						}
						else{
							alert('New PIN mismatched.')
						}
					})
				}
			}
			var PersonalInfo = {
				listener : function($){
					$("#change-avatar").on('change',function(){
						var oFReader = new FileReader();
						oFReader.readAsDataURL(document.getElementById("change-avatar").files[0]);

						oFReader.onload = function (oFREvent) {
						    document.getElementById("avatar-prev").src = oFREvent.target.result;
						};
					})
				}
			}
			var Payment = {
				listener : function($){
					$("#step1 select").on('change',function(){
						show_loader($);
						var _data = "a=calculate_price&plan_option="+$(this).val();
						$.post(window.location.href,_data, function(data){
							data = $.trim(data);
							if(data!=-1){								
								$("#step2 span").html(data);
							}
							close_loader($);
							console.log(data);
						});
						$("#step2, #step3").slideDown();
						$("#myaccount .wrapper").height("+=200");
						$('html,body').animate({
							 scrollTop: $("#step1").offset().top - 150
			     		}, 500);
					})
					$("#step3 select").on('change',function(){
						$("#step4 div").slideUp();
						$("#"+$(this).find('option:selected').data('details')).slideDown();
						if($(this).find('option:selected').hasClass('wire')){
							$("#step5 .bank").fadeOut("fast",function(){
								$("#step4, #step5, #step5 .wire").slideDown();								
								$("#myaccount .wrapper").css('height',"2500px");	
							});							
						}
						else if($(this).find('option:selected').hasClass('bank')){
							$("#step5 .wire").fadeOut("fast",function(){
								$("#step4, #step5, #step5 .bank").slideDown();
								$("#myaccount .wrapper").css('height',"2500px");
							});							
						}
					})
					if($("#payment-cont").hasClass('active')){
						$("#myaccount .wrapper").css('height',$("#payment-cont").outerHeight())
					}
					Payment.fill_up($);
					Payment._submit($);
					Payment.step4($);
				},
				fill_up : function($){
					$('#your_name').val( "<?php echo $current_user_fullname ?>" );
					$('#your_sponsor_id').val( "<?php echo $current_user['sponsor_id'] ?>" );
					$('#your_sponsor_name').val( "<?php echo $sponsor_details['fname'].' '.$sponsor_details['mname'].' '.$sponsor_details['lname'] ?>" ); 
					$('#ipaddress').html( "<?php echo $_SERVER['REMOTE_ADDR'] ?>" )
				},
				_submit : function($){
					$("#payment-cont form").on('submit',function(event){
						if($("#payment_picture_proof").val()=="" ){
							event.preventDefault();
							$("#step6").slideDown();
							$('html,body').animate({
								scrollTop: $("#step6").offset().top - 150
				     		}, 500);
							$("#myaccount .wrapper").css('height',"2500px");
							return false;
						}
						else{
							show_loader($);
							/*
							we can't use ajax since we can't upload files using jquery ajax... for now
							var _data = "a=payment&"+$(this).serialize();
							$.post(window.location.href,_data, function(data){
								if($.trim(data)==1){
									window.location.href="/dashboard?logged=1";
								}
								else if($.trim(data)==2){
									alert('Sorry your account is invalid.');
								}
								close_loader($);
								console.log(data);
							});*/
						}
					})
				},
				step4 : function($){
					$("#payment_picture_proof").on('change',function(){
						var oFReader = new FileReader();
						oFReader.readAsDataURL(document.getElementById("payment_picture_proof").files[0]);

						oFReader.onload = function (oFREvent) {
						    document.getElementById("payment_picture_proof_prev").src = oFREvent.target.result;
						};
					})
				}
			}
			var MyAccounts = {
				that : null, $ : null,
				listener : function($){
					that=this; $ = $;
					$("#btn-security").on('click',function(){
						that.show_security(this);
					});
					$("#btn-personal").on('click',function(){
						that.show_personal(this);
					});
					$("#btn-activation").on('click',function(){
						that.show_activation(this);
					});
					$("#btn-payment").on('click',function(){
						that.show_payment(this);
					});

					
				},
				show_security : function(that){
					$(".contents").removeClass('active');
					$("#security-cont").addClass('active');
					$("#myaccount .wrapper").css('height',$("#security-cont").outerHeight())
				},
				show_personal : function(that){
					$(".contents").removeClass('active');
					$("#personal-info-cont").addClass('active');
					$("#myaccount .wrapper").css('height',$("#personal-info-cont").outerHeight())
				},
				show_activation : function(that){
					$(".contents").removeClass('active');
					$("#activation-cont").addClass('active');
					$("#myaccount .wrapper").css('height',$("#activation-cont").outerHeight())
				},
				show_payment : function(that){
					$(".contents").removeClass('active');
					$("#payment-cont").addClass('active');
					$("#myaccount .wrapper").css('height',$("#payment-cont").outerHeight())
				}
			}		
		</script>
		<?php 
		echo $activetab_script; 
	}
endif;
Func::footer_hook('script');

include("views/partials/admin_footer.php");