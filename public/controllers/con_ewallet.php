<?php
session_start();
Func::inactive_blocker();

include("models/cls_transaction.php");	
$transaction = new Transaction($_SESSION);

if(isset($_POST['a'])){
	switch($_POST['a']){
		case "widthraw_request":
			$transaction->widthraw_request($_POST);
			break;
		case "upgade_request":
			$transaction->upgade_request($_POST);
			break;
		case "addaccount_request":
			$transaction->addaccount_request($_POST);
			break;
	}	
}


include("views/partials/admin_header.php");
$current_page = "ewallet";
include("views/partials/admin_sidebar.php");


$transaction_details = $transaction->getalldata();

$membership = $transaction->getmembership();

function status($status){
	switch($status){
		case 0 : $data = "On Process"; break;
		case 1 : $data = "Completed"; break;
		case 2 : $data = "Declined"; break;
	}
	return $data;
}

include("views/admin/ewallet.php");	



global $focus_script;
$focus_script="";
if(isset($_GET['focus'])){
	switch($_GET['focus']){
		case "table":
			ob_start();
			?>
			<script>
				$('html,body').animate({
					 scrollTop: $("#transaction-table").offset().top - 50
	     		}, 500);
			</script>
			<?php
			$focus_script = ob_get_contents();
			ob_end_clean();
			break;
	}
}




function script() { 
	global $focus_script; 	
	?>
	<script>
		(function(global, $){ $(document).ready(function(){
			EWallet.listener($);
			Transaction.widthraw($);
			Transaction.upgrade($);
			Transaction.addaccount($);
			<?php 
			if(isset($_GET['status'])) {
				echo "Transaction.successmessage($,".$_GET['status'].");";
			}
			?>
		}); })(window, jQuery);

		var Transaction = {
			widthraw : function($){
				Transaction.request($,"#ewallet_widthraw", "widthraw_request", 1 );
			},
			upgrade : function($){				
				Transaction.request($,"#ewallet_upgrade", "upgade_request", 2 );
			},	
			addaccount : function($){
				Transaction.request($,"#ewallet_add_account", "addaccount_request", 3 );
			},
			request : function($, modal_id, function_name, getvalue){
				$(document).on('submit',modal_id+" form",function(e){
					e.preventDefault();
					show_loader($);
					var _data = "a="+function_name+"&"+$(this).serialize();
					$.post(window.location.href,_data, function(data){
						data = $.trim(data);
						if(data==1){
							window.location.href="<?php echo SITE_URL ?>/ewallet?status="+getvalue;
						}
						else if(data==2 && getvalue==2){
							window.location.href="<?php echo SITE_URL ?>?logout=2";
						}
						else if(data==2 && getvalue==3){
							window.location.href="<?php echo SITE_URL ?>/myaccount?tab=3";
						}
						else if(data==-1){
							alert('Sorry! Your PIN code is incorrect.');
						}
						close_loader($);
						console.log(data);
					});
				})
			},
			successmessage : function($,type){
				Modal.hasButton = false;
				Modal._title = "Transaction Status";
				Modal.addId = "transaction-result";
				switch(type){
					case 1 :
						Modal.contents = 
						'<h1>Great Job!</h1>'+
						'<p>You have sent a request for widthrawal. <br />We will be processing your request in about 24-48 hours. Thank you!</p>';
						break;
					case 2 :
						Modal.contents = 
						'<h1>Great Job!</h1>'+
						'<p>You have sent a request for upgrade. <br />We will be processing your request in about 24-48 hours. Thank you!</p>';
						break;
					case 3 :
						Modal.contents = 
						'<h1>Great Job!</h1>'+
						'<p>You have sent a request for adding new accounts. <br />Expect to have new activation code(s) after the transaction has completed. We will be processing your request in about 24-48 hours. Thank you!</p>';
						break;
				}
				
				Modal.show($);
			}
		}
		var EWallet = {
			that : null, $ : null,
			listener : function($){
				that=this; $ = $;
				$("#btn-add").on('click',function(){
					that.show_add(this);
				});
				$("#btn-widthraw").on('click',function(){
					<?php if(date('N')=="2") : ?>
					that.show_widthraw(this);
					<?php else: ?>
					alert('E-wallet request is only available on Tuesday.');
					<?php endif; ?>
				});				
				$("#btn-upgrade").on('click',function(){
					<?php if($_SESSION['user_type']==1) : ?>
					that.show_upgrade(this);
					<?php else: ?>
					alert('Coming soon...');
					<?php endif; ?>
				});
				$(".btn-back").on('click',function(){
					that.show_step1(this);
				});
				$(document).on('change','#qty',function(){
					var cost = <?php echo ($_SESSION['user_type']==1) ? BASIC_FEE : PREMIER_FEE ?>;
					$("#cost span").html( $(this).val() * cost );
				});
				$(document).on('change','#payout_type',function(){
					var pay_type = $(this).find('option:selected').attr('class');
					console.log(pay_type);
					if($.trim(pay_type)!="wire"){
						$(".bank-details input").removeAttr('disabled');
						$(".wire-details input").attr('disabled',true);
					}
					else{
						$(".wire-details input").removeAttr('disabled');
						$(".bank-details input").attr('disabled',true);
					}
				})
			},
			show_add : function(that){
				Modal._title = "E-Wallet Add Accounts"
				Modal.hasButton = false;
				Modal.addId = "ewallet_add_account";
				Modal.contents = 
					'<form role="form">' +
						'<div class="form-group">'+
							'<label for="">How many accounts to add? <strong id="cost">Php <span>0</span></strong></label>'+
							'<select class="form-control" id="qty" name="qty">'+
								'<option value="">--[choose]--</option>'+
								'<option>1</option>'+
								'<option>3</option>'+
								'<option>7</option>'+
							'</select>'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">Enter PIN:</label>'+
							'<input class="form-control" type="text" id="pin" name="pin" value="" />'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">Payout Type:</label>'+
							'<select class="form-control" id="payout_type" name="payout_type">'+								
								<?php echo Func::transaction_outputs('js'); ?>
								'<option class="ewallet">E-Wallet</option>'+
							'</select>'+
						'</div>'+
						'<hr />'+
						'<div class="row">'+
							'<div class="col-md-6 bank-details">'+
						'<strong>For Banks</strong>'+
						'<div class="form-group">'+
							'<label for="">Enter Branch Address:</label>'+
							'<input class="form-control" type="text" id="bank_branch_addr" name="bank_branch_addr" value="" />'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">Enter Account No:</label>'+
							'<input class="form-control" type="text" id="acc_no" name="acc_no" value="" />'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">Enter Account Name:</label>'+
							'<input class="form-control" type="text" id="acc_name" name="acc_name" value="" />'+
						'</div>'+
							'</div>'+
							'<div class="col-md-6 wire-details">'+
						'<strong>For Wire Transfers</strong>'+
						'<div class="form-group">'+
							'<label for="">Enter Name:</label>'+
							'<input class="form-control" type="text" id="wire_name" name="wire_name" value="" />'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">Enter Contact:</label>'+
							'<input class="form-control" type="text" id="wire_contact" name="wire_contact" value="" />'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">Enter Complete Address:</label>'+
							'<input class="form-control" type="text" id="wire_address" name="wire_address" value="" />'+
						'</div>'+
							'</div>'+
						'</div>'+
						'<input class="btn btn-primary pull-right btn-lg" type="submit" value="Add Account(s)">'+
					'</form>'
				Modal.show($);
			},
			show_widthraw : function(that){
				Modal._title = "E-Wallet Money Widthrawal Request";
				Modal.hasButton = false;
				Modal.addId = "ewallet_widthraw";
				Modal.contents = 
					'<form role="form">' +
						'<div class="form-group">'+
							'<label for="">Enter Amount:</label>'+
							'<input class="form-control" type="text" id="amount" name="amount" value="" />'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">Enter PIN:</label>'+
							'<input class="form-control" type="text" id="pin" name="pin" value="" />'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">Payout Type:</label>'+
							'<select class="form-control" id="payout_type" name="payout_type">'+								
								<?php echo Func::transaction_outputs('js'); ?>
							'</select>'+
						'</div>'+
						'<hr />'+
						'<div class="row">'+
							'<div class="col-md-6 bank-details">'+
						'<strong>For Banks</strong>'+
						'<div class="form-group">'+
							'<label for="">Enter Branch Address:</label>'+
							'<input class="form-control" type="text" id="bank_branch_addr" name="bank_branch_addr" value="" />'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">Enter Account No:</label>'+
							'<input class="form-control" type="text" id="acc_no" name="acc_no" value="" />'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">Enter Account Name:</label>'+
							'<input class="form-control" type="text" id="acc_name" name="acc_name" value="" />'+
						'</div>'+
							'</div>'+
							'<div class="col-md-6 wire-details">'+
						'<strong>For Wire Transfers</strong>'+
						'<div class="form-group">'+
							'<label for="">Enter Name:</label>'+
							'<input class="form-control" type="text" id="wire_name" name="wire_name" value="" />'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">Enter Contact:</label>'+
							'<input class="form-control" type="text" id="wire_contact" name="wire_contact" value="" />'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">Enter Complete Address:</label>'+
							'<input class="form-control" type="text" id="wire_address" name="wire_address" value="" />'+
						'</div>'+
							'</div>'+
						'</div>'+
						
						'<input class="btn btn-primary pull-right btn-lg" type="submit" value="Widthraw Money">'+
					'</form>';
				Modal.show($);
			},
			show_upgrade : function(that){
				Modal._title = "E-Wallet Upgrade Request";
				Modal.hasButton = false;
				Modal.addId = "ewallet_upgrade";
				Modal.contents = 
					'<form role="form">' +
						'<div class="form-group">'+
							'<label for="">Upgrade Account To:</label>'+
							'<select class="form-control" id="upgrade_to" name="upgrade_to">'+
								'<option value="2">Premier</option>'+
							'</select>'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">Enter PIN:</label>'+
							'<input class="form-control" type="text" id="pin" name="pin" value="" />'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">Payout Type:</label>'+
							'<select class="form-control" id="payout_type" name="payout_type">'+								
								<?php echo Func::transaction_outputs('js'); ?>
								'<option class="ewallet">E-Wallet</option>'+
							'</select>'+
						'</div>'+
						'<hr />'+
						'<div class="row">'+
							'<div class="col-md-6">'+
						'<strong>For Banks</span>'+
						'<div class="form-group">'+
							'<label for="">Enter Branch Address:</label>'+
							'<input class="form-control" type="text" id="bank_branch_addr" name="bank_branch_addr" value="" />'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">Enter Account No:</label>'+
							'<input class="form-control" type="text" id="acc_no" name="acc_no" value="" />'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">Enter Account Name:</label>'+
							'<input class="form-control" type="text" id="acc_name" name="acc_name" value="" />'+
						'</div>'+
							'</div>'+
							'<div class="col-md-6">'+
						'<strong>For Wire Transfers</span>'+
						'<div class="form-group">'+
							'<label for="">Enter Name:</label>'+
							'<input class="form-control" type="text" id="wire_name" name="wire_name" value="" />'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">Enter Contact:</label>'+
							'<input class="form-control" type="text" id="wire_contact" name="wire_contact" value="" />'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">Enter Complete Address:</label>'+
							'<input class="form-control" type="text" id="wire_address" name="wire_address" value="" />'+
						'</div>'+
							'</div>'+
						'</div>'+
						
						'<input class="btn btn-primary pull-right btn-lg" type="submit" value="Upgrade Account">'+
					'</form>';
				Modal.show($);
			},
			show_step1 : function(that){
				$(that).parent().toggleClass('active');
				$("#step1").toggleClass('active');
			}
		}		
	</script>
	<?php
	echo $focus_script; 
}
Func::footer_hook('script');

include("views/partials/admin_footer.php");