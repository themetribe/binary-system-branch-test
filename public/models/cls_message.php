<?php
class Message extends DB{
	
	public function __construct(){
		parent::__construct(); 
		$this->table = "messages";		
	}

	public function fetchInbox(){
		$q = "	SELECT a.*, CONCAT(b.fname,' ',b.mname,' ',b.lname) as _from
				FROM messages as a, user as b
				WHERE a.user_ID= :user_ID AND a.type = :type AND a.from_ID = b.ID
				ORDER BY ID DESC
			";
		$arr = array("user_ID"=>$_SESSION['user_ID'], "type"=>1);
		$con = $this->query($q,$arr);
		$data = $con->fetchAll(PDO::FETCH_ASSOC);
		return $data;
	}

	public function fetchSent() {
		$q = "	SELECT CONCAT(b.fname,' ',b.mname,' ',b.lname) _to, 
					a.message, a.date_time, a.from_ID
				FROM messages as a, user as b
				WHERE a.from_ID = :user_ID AND a.type = :type AND a.user_ID = b.ID
			";
		$arr = array("user_ID"=>$_SESSION['user_ID'], "type"=>1);
		$con = $this->query($q,$arr);
		$data = $con->fetchAll(PDO::FETCH_ASSOC);
		return $data;
	}

	public function fetchTrash(){
		$data = $this->select("*",array("type"=>0,"user_ID"=>$_SESSION['user_ID']));
		return $data;
	}

	public function toInbox($temp){
		// kind of tricky. we need to interchange userID and fromID since we're using the type 'inbox' :)
		$data['user_ID'] = $temp['toid'];
		$data['message'] = $temp['message'];
		$data['date_time'] = date("Y-m-d H:i:s");
		$data['from_ID'] = $_SESSION['user_ID'];
		$data['type'] = 1;
		echo $this->save($data,array(),"rowCount");
	}

	public function toTrash($id){
		$data = $this->save(array("type"=>0),array("ID"=>$id));
		return $data;
	}

	public function getnotification(){
		$arr = array("isRead"=>0,"type"=>1,"user_ID"=>$_SESSION['user_ID']);
		$data = $this->select("*",$arr);
		if(isset($data) && count($data)>0 && $data!=""){
			return '<span class="badge">'.count($data).'</span>';
		}
		else{
			return "";
		}
	}

	public function setToRead($mid){
		$data = $this->save(array("isRead"=>1),array("ID"=>$mid));
	}

}