<?php
class Library extends DB{
	
	public function __construct(){
		parent::__construct(); 
		$this->table = "library";		
	}

	public function getall(){
		$data = $this->select("*");
		return $data;
	}

	public function getebooks_lists($cat=""){
		if($cat==""){
			$arr_where = array('type'=>1);
		}
		else{
			$arr_where = array('type'=>1,"ebook_category"=>$cat);
		}
		$data = $this->select("*",$arr_where);
		
		return $data;
	}

	public function getaudio_lists($cat=""){
		if($cat==""){
			$arr_where = array('type'=>2);
		}
		else{
			$arr_where = array('type'=>2,"category"=>$cat);
		}
		$data = $this->select("*",$arr_where);
		
		return $data;
	}

	public function getvideo_lists($cat=""){
		if($cat==""){
			$arr_where = array('type'=>3);
		}
		else{
			$arr_where = array('type'=>3,"category"=>$cat);
		}
		$data = $this->select("*",$arr_where);
		
		return $data;
	}

	public function gettype($data){
		switch($data){
			case 1 : $return = "ebook"; break;
			case 2 : $return = "audio"; break;
			case 3 : $return = "video"; break;
			case 4 :
			case 5 : $return = "site_tour"; break;
			case 6 : $return = "quotes"; break;
			default: $return = "";
		}
		return $return;
	}

	public function getmentor($data){
		$arr_where = array();
		$arr_where = array("mentor"=>$data);
		$data = $this->select("*",$arr_where);
		return $data; break;
	}

	public function getsitetutorial($data){
		$arr_where = array();
		$arr_where = array("type"=>$data);
		$data = $this->select("*",$arr_where);
		return $data; break;
	}

	public function getquote_lists(){
		$arr_where = array();
		$arr_where = array("type"=>6);
		$data = $this->select("*",$arr_where);
		return $data; break;
	}
}