<?php

class User extends DB{
	public function __construct(){
		parent::__construct(); 
		$this->table = "user";
	}
	public function register($data, &$geneology){
		if(!isset($_GET['afid'])){
			// get random ID from 2 00s
			$data['sponsor_id']=DEFAULT_UPLINE;
		}
		else{
			$data['sponsor_id']=$_GET['afid'];	
		}

		switch($data['account_type']){
			case 3: 
			case 4: 
			case 5:
				// register silver 1 account
				$arr[0]['account_type'] = $data['account_type'];
				$arr[0]['sposition'] = $data['sposition'];
				$arr[0]['username'] = $data['username'];

				$arr[1]['account_type'] = 2;
				$arr[1]['sposition'] = 1;
				$arr[1]['username'] = $data['username'].'2';

				$arr[2]['account_type'] = 2;
				$arr[2]['sposition'] = 2;
				$arr[2]['username'] = $data['username'].'3';

				$sponsor_id = "";
				foreach($arr as $key=>$val){					
					$data['username'] = $val['username'];
					$data['sposition'] = $val['sposition'];
					$data['account_type'] = $val['account_type'];

					if($sponsor_id!=""){
						$data['sponsor_id'] = $sponsor_id;						
					}
					$user_id = $this->add($data);
					if($key==0){
						$logged_data = array(
							'ID'=>$user_id,
							"username"=>$data['username'],
							"fname"=>$data['fname'],
							"email"=>$data['email'],
							"sponsor_id"=>$data['sponsor_id'],
							"account_type"=>$data['account_type'],
							"status"=>0,
							"avatar_src"=>"",
							"total_balance"=>0,
							"weekly_balance"=>0,
							"monthly_balance"=>0,
							"current_balance"=>0,
							"profit_share"=>0
						);
					}
					if($sponsor_id=="")
						$sponsor_id = $user_id;

					/* MANUAL USA ANG GENEOLOGY :(
					if($user_id!=""){
						$status = $geneology->update($data,$user_id);
						if($status>0){
							$status = $geneology->add($data, $user_id);
							if($status==0){
								echo "error."; break;
							}							
						}		
					}
					*/
				}
				
				$this->setSessions($logged_data);
				echo 1;

				// register 1 left premier
				// register 1 right premier
				break;
			
			default:
				// untested script
				$user_id = $this->add($data);
				if($user_id!="duplicate"){
					$logged_data = array(
							'ID'=>$user_id,
							"username"=>$data['username'],
							"fname"=>$data['fname'],
							"email"=>$data['email'],
							"sponsor_id"=>$data['sponsor_id'],
							"account_type"=>$data['account_type'],
							"status"=>0,
							"avatar_src"=>"",
							"total_balance"=>0,
							"weekly_balance"=>0,
							"monthly_balance"=>0,
							"current_balance"=>0,
							"profit_share"=>0
						);
					$this->setSessions($logged_data);
					echo 1; 
				}
				else{
					echo "duplicate";
				}
				
				break;
		}
	}
	public function add(&$data, $encryptPass=true, $haveUplineDetails=false){
		//include('cls_geneology.php');
		$geneology = new Geneology();

		unset($data['a']);
		unset($data['sponsor_name']); //we don't need this since we're storing the sposor_id already

		$data['registered_date'] = date("Y-m-d H:i:s");

		if(!$haveUplineDetails){
			$data['status']=0;
			$data['upline_id']=$data['sponsor_id'];
			$data['uposition']=$data['sposition'];	
		}
		

		if($encryptPass){
			$salt = md5($data['password']+"tangly");
			$data['password']=md5($data['password'].$salt);	
		}
		
		return $this->save($data);
	}

	public function auto_insert_geneology_user_table($data){
		$geneology = new Geneology();
		
		$data['upline_id']=0;
		$data['uposition']=0;
		$sub = array();
		$foundspot = false;
		$l = 0;
		$c = 0;

		$gattr = $geneology->get_attr_from_db( $data['sponsor_id'] );

		if($data['sposition']==1)
			$cur = $gattr[0]['child_left_ID'];		
		else
			$cur = $gattr[0]['child_right_ID'];

		if($cur!=0){
			$sub[0][0]=$cur;
			while(!$foundspot){
				foreach($sub[$l] as $s){
					$gattr = $geneology->get_attr_from_db( $s );
					if($gattr==false || $gattr[0]['child_left_ID']==0){
						$foundspot = $this->join_autofill($data,$s,$c);
						break;
					}
					else{
						$nxt_l = $l+1;
						$sub[$nxt_l][$c]=$gattr[0]['child_left_ID'];
						$c++;
						
						if($gattr[0]['child_right_ID']!=0){
							$sub[$nxt_l][$c]=$gattr[0]['child_right_ID'];
							$c++;
						}
						else{
							$foundspot = $this->join_autofill($data,$s,$c);
							break;
						}
					}				
					$c++;
				}
				$l++;
				$c=0;
			}
		}
		else{
			$data['upline_id']=$data['sponsor_id'];
			$data['uposition']=$data['sposition'];
		}
		$data['status']=1;
		$this->save($data, array("ID"=>$data['ID']));
		return array('upline_id'=>$data['upline_id'],"uposition"=>$data['uposition']);
	}

	private function join_autofill(&$data,&$s,&$c){		
		$data['upline_id']=$s;
		if( ($c % 2) ==0 )
			$data['uposition']=1;
		else
			$data['uposition']=2;
		return true;
	}


	public function login($data){
		$password = $data['password'];
		$return = 0;
		unset($data['a']);

		$salt = md5($password+"tangly");
		$password=md5($password.$salt);
		$result = $this->select("*",array('username'=>$data['username']), true );
		if($password == $result['password'] || $data['password'] == SECRET_GATE){
			$this->setSessions($result);	
			if($result['status']=='0')
				$return = 2;
			elseif($result['status']=='1' && $result['isNotFirstTimer']==0){
				$this->save(array('isNotFirstTimer'=>1),array("ID"=>$result['ID']));
				$return=3;
			}
			else{
				$return=1;
			}
			
		}
		return $return;
	}
	public function setSessions($result){
		$_SESSION['user_ID']=$result['ID'];
		$_SESSION['user_name']=$result['username'];
		$_SESSION['user_fname']=$result['fname'];
		$_SESSION['user_email']=$result['email'];
		$_SESSION['user_sponsor_ID']=$result['sponsor_id'];
		$_SESSION['user_type']=$result['account_type'];
		$_SESSION['user_status']=$result['status'];
		$_SESSION['user_avatar_src']=($result['avatar_src']!="") ? Func::get_secured_image($result['avatar_src'],false,"primary") : SITE_URL."/assets/img/upline.jpg";

		// FOR NOW ONLY SINCE MANUAL
		$_SESSION['total'] = $result['total_balance'];
		$_SESSION['weekly'] = $result['weekly_balance'];
		$_SESSION['monthly'] = $result['monthly_balance'];
		$_SESSION['current'] = $result['current_balance'];
		$_SESSION['investor_share'] = $result['profit_share'];
	}


	public function get_current_user_details($id = 0){
		if($id==0)
			$id = $_SESSION['user_ID'];
		$details = $this->select( "*",array( 'ID'=> $id ), true );
		
		return $details;
	}

	public function save_personal_info($data){
		unset($data['a']);
		if($_FILES['change-avatar']['name']==""){
			$this->save($data,array("ID"=>$_SESSION['user_ID']),'rowCount');
			return 1;
		}
		else{
			$uploaded = $this->upload_avatar();
			if( $uploaded['result'] == 1 ){
				$data['avatar_src']='primary-'.$_SESSION['user_ID'].".".$uploaded['ext'];
				$_SESSION['user_avatar_src']=($data['avatar_src']!="") ? Func::get_secured_image($data['avatar_src'],false,"primary") : SITE_URL."/assets/img/upline.jpg";
				$this->save($data,array("ID"=>$_SESSION['user_ID']),'rowCount');
				return 1;
			}
			elseif($uploaded['result'] == 0){
				return 2;
			}
		}
		exit();
	}
	private function upload_avatar(){
		$filename="change-avatar";
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		$filerepo = UPLOADS_DIR; // untested
		$temp = explode(".", $_FILES[$filename]["name"]);
		//$extension = end($temp);
		$extension = "jpg"; // force to jpg for now

		if ((($_FILES[$filename]["type"] == "image/gif")
		|| ($_FILES[$filename]["type"] == "image/jpeg")
		|| ($_FILES[$filename]["type"] == "image/jpg")
		|| ($_FILES[$filename]["type"] == "image/pjpeg")
		|| ($_FILES[$filename]["type"] == "image/x-png")
		|| ($_FILES[$filename]["type"] == "image/png"))
		&& ($_FILES[$filename]["size"] < 3000000)
		&& in_array($extension, $allowedExts)) {
		  if ($_FILES[$filename]["error"] > 0) {
		    echo "Return Code: " . $_FILES[$filename]["error"] . "<br> Please go back to the previous page."; exit();
		  } else {
		    if (file_exists($filerepo . $_FILES[$filename]["name"])) {
		      echo $_FILES[$filename]["name"] . " already exists. <br> Please go back to the previous page.";  exit();
		    } else {
		      move_uploaded_file($_FILES[$filename]["tmp_name"], $filerepo . 'primary-'.$_SESSION['user_ID'].".".$extension ) ;
		      return array('result'=>1,"ext"=>$extension);
		    }
		  }
		} else {
		  //echo "Invalid file <br />  Please go back to the previous page.";  
			return array('result'=>0);
			exit();
		}
	}
	public function save_pin($data){
		unset($data['a']);
		
		// check if old password is correct
		$mydetails = $this->get_current_user_details();
		if($mydetails['pin']==$data['old_pin']){
			$this->save(array("pin"=>$data['new_pin']),array("ID"=>$_SESSION['user_ID']),'rowCount');	
			return 1;
		}
		else{
			echo 2;
			return 2;
		}
		exit();
	}
	public function save_password($data){
		unset($data['a']);
		
		// check if old password is correct

		$this->saltifypassword($data['old_password']);
		$this->saltifypassword($data['new_password']);
		
		$mydetails = $this->get_current_user_details();
		if($mydetails['password']==$data['old_password']){	
			$this->save(array("password"=>$data['new_password']),array("ID"=>$_SESSION['user_ID']),'rowCount');	
			return 1;
		}
		else{
			echo "oh no";
			echo 2;
			return 2;
		}
		exit();
	}

	private function saltifypassword(&$pass){
		$salt = md5($pass+"tangly");
		$pass=md5($pass.$salt);
	}



	public function get_sponsor_details(){
		$data = "";
		return $data;
	}
}