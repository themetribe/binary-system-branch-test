<?php
class Geneology extends DB{
	
	public function __construct(){
		parent::__construct(); 
		$this->table = "geneology";		
	}
	public function add_for_multiple($data){
		return $this->save($data);
	}
	public function update_specific($data,$id){
		return $this->save($data,array("user_ID"=>$id),'rowCount');
	}
	public function update_for_multiple($id, $left, $right){
		return $this->save(array("child_left_ID"=>$left,"child_right_ID"=>$right),array("user_ID"=>$id),'rowCount');
	}
	public function add($temp,$user_id){
		$data['user_ID']=$user_id;
		/*
		These are blank because new registered user pa xa 
		so wala pay child left and right.
		*/
		$data['child_left_ID']="";
		$data['child_right_ID']="";
		return $this->save($data,array(),'rowCount');
	}
	public function update($temp,$user_id){
		$data['user_ID']=$temp['upline_id'];
		if($temp['uposition']==1){
			$data['child_left_ID']=$user_id;
		}		
		else{
			$data['child_right_ID']=$user_id;
		}
		return $this->save($data,array("user_ID"=>$temp['upline_id']),'rowCount');
	}

	public function generate_graph_data_until_end(){
		$subj = array();
		$attr = array();
		$isEnd = false;
		$data_per_level = 0;

		$lvl = 0;
		$c = 0;
		
		if(isset($_GET['s00'])){
			$subj[0][0]['ID'] = $_GET['s00'];	
		}
		else{
			$subj[0][0]['ID'] = $_SESSION['user_ID'];
		}
		
		$subj[0][0]['account_type'] = ($_SESSION['user_type']==2) ? "premier" : "basic";

		while(!$isEnd){

			foreach($subj[$lvl] as $key=>$cur){
				if($cur!=-1) { // walay kasunod
					$attr = $this->get_attr_from_db($cur['ID']);					
					$attr = $attr[0];
					$subj[$lvl][$key]['fullname'] = (trim($attr['fullname'])!="") ? $attr['fullname'] : $attr['username'];
					$subj[$lvl][$key]['sponsor_ID'] = $attr['sponsor_ID'];
					$subj[$lvl][$key]['upline_id'] = $attr['upline_id'];
					$subj[$lvl][$key]['side'] = ($attr['uposition']==1) ? "Left" : "Right";
					$subj[$lvl][$key]['registered_date'] = $attr['registered_date'];
					$subj[$lvl][$key]['upgrade_date'] = $attr['upgrade_date'];

					$l=$lvl;
					$c=$key;
					$x = pow(2,$l)/2;

					if($c<$x) {
						$subj[$lvl][$key]['group']="Left";
					}
					else{
						$subj[$lvl][$key]['group']="Right";	
					}

					$subj[$lvl][$key]['upgrade_date'] = $attr['upgrade_date'];
					if($attr['sponsor_ID']==$_SESSION['user_ID'])
						$subj[$lvl][$key]['isDirect']=1; //true direct referral
					else
						$subj[$lvl][$key]['isDirect']=0; //false indirect referral
					switch($attr['account_type']){
						case 1: $subj[$lvl][$key]['account_type']= "basic"; break;
						case 2: $subj[$lvl][$key]['account_type']= "premier"; break;
						case 3: $subj[$lvl][$key]['account_type']= "silver"; break;
						case 4: $subj[$lvl][$key]['account_type']= "gold"; break;
						case 5: $subj[$lvl][$key]['account_type']= "diamond"; break;
						case 6: $subj[$lvl][$key]['account_type']= "_01"; break;
						case 7: $subj[$lvl][$key]['account_type']= "admin"; break;
					}
				}
				else{
					$attr = array(
							'account_type'=>-1,
							'child_left_ID'=>0,
							'child_right_ID'=>0
						);
				}

				$next_lvl = $lvl+1;
				$subj[$next_lvl] = array();
				if( $attr['child_left_ID']!=0 ){
					$subj[$next_lvl][$c]['ID'] = $attr['child_left_ID'];
					$data_per_level++;
				}
				else{
					$subj[$next_lvl][$c] = -1;
				}
				$c++;
				if( $attr['child_right_ID']!=0 ){
					$subj[$next_lvl][$c]['ID'] = $attr['child_right_ID'];
					$data_per_level++;
				}
				else{
					$subj[$next_lvl][$c] = -1;
				}
				unset($attr);
				$c++;		
			}
			if($data_per_level==0){
				$isEnd=true;
			}
			else{
				$data_per_level=0;
				$lvl++;
				$c=0;
			}
		}
		// since this function is build for the use of Geneology Textual Only, we'll remove the user from the list.
		unset($subj[0]);
		unset($subj[$lvl+1]);
		return $subj;
	}
	public function generate_graph_data(){		
		$lvl_limit = 3;
		$lvl = 0;
		$c = 0;
		
		if(isset($_GET['s00'])){
			$subj[0][0]['ID'] = $_GET['s00'];	
		}
		else{
			$subj[0][0]['ID'] = $_SESSION['user_ID'];
		}
		
		$subj[0][0]['account_type'] = ($_SESSION['user_type']==2) ? "premier" : "basic";

		while($lvl<=$lvl_limit){
			foreach($subj[$lvl] as $key=>$cur){
				if($cur!=-1) { // walay kasunod
					$attr = $this->get_attr_from_db($cur['ID']);					
					$attr = $attr[0];
					$subj[$lvl][$key]['fullname'] = $attr['fullname'];
					$subj[$lvl][$key]['registered_date']=$attr['registered_date'];
					if($attr['sponsor_ID']==$_SESSION['user_ID'])
						$subj[$lvl][$key]['isDirect']=1; //true direct referral
					else
						$subj[$lvl][$key]['isDirect']=0; //false indirect referral
					switch($attr['account_type']){
						case 1: $subj[$lvl][$key]['account_type']= "basic"; break;
						case 2: $subj[$lvl][$key]['account_type']= "premier"; break;
						case 3: $subj[$lvl][$key]['account_type']= "silver"; break;
						case 4: $subj[$lvl][$key]['account_type']= "gold"; break;
						case 5: $subj[$lvl][$key]['account_type']= "diamond"; break;
						case 6: $subj[$lvl][$key]['account_type']= "_01"; break;
						case 7: $subj[$lvl][$key]['account_type']= "admin"; break;
					}
				}
				else{
					$attr = array(
							'account_type'=>-1,
							'child_left_ID'=>0,
							'child_right_ID'=>0
						);
				}

				$next_lvl = $lvl+1;
				if( $attr['child_left_ID']!=0 ){
					$subj[$next_lvl][$c]['ID'] = $attr['child_left_ID'];
				}
				else{
					$subj[$next_lvl][$c] = -1;
				}
				$c++;
				if( $attr['child_right_ID']!=0 ){
					$subj[$next_lvl][$c]['ID'] = $attr['child_right_ID'];
				}
				else{
					$subj[$next_lvl][$c] = -1;
				}
				unset($attr);
				$c++;		
			}
			$lvl++;
			$c=0;
		}
		return $subj;
	}
	public function get_attr_from_db($id){
		$arr = array("user_ID"=>$id);
		$q = "	SELECT 	a.ID, 
						a.account_type, 
						a.sponsor_ID,
						b.child_left_ID, 
						b.child_right_ID, 
						a.upline_id,
						a.uposition,
						DATE_FORMAT(a.registered_date, '%Y-%m-%d') as registered_date,
						a.upgrade_date,
						a.username,
						CONCAT(a.fname,' ',a.lname) as fullname
				FROM 	user as a, 
						geneology as b 
				WHERE a.ID = :user_ID 
				AND a.ID = b.user_ID";	
		$stmt = $this->query($q,$arr);
		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);		
		return $data;
	}
	private function user_type($graph_data, $x, $y){
		if( $graph_data[$x][$y] != -1 ){
			echo "user-".$graph_data[$x][$y]['account_type'];
		}
	}
	public function generate_graph_html($graph_data, $x, $y){ 
		$afid = ($graph_data[$x][$y]!=-1) ? $graph_data[$x][$y]['ID'] : '';
		$class = ($graph_data[$x][$y]['isDirect']==1) ? "user referral-direct" : "user referral-indirect";
		if($afid!="") : ?>
			<div class="<?php echo $class ?>" data-afid="<?php echo $afid ?>" data-fullname="<?php echo $graph_data[$x][$y]['fullname'] ?>" data-acctype="<?php echo strtoupper($graph_data[$x][$y]['account_type']) ?>" data-regdate="<?php echo $graph_data[$x][$y]['registered_date'] ?>">
				<a class="link" href="<?php echo SITE_URL ?>/geneology?s00=<?php echo $afid ?>"></a>
				<?php if(file_exists('file://'.UPLOADS_DIR."primary-".$afid.".jpg")) : ?>
					<i class="<?php $this->user_type($graph_data,$x,$y) ?>" >
						<span class="<?php echo $graph_data[$x][$y]['account_type'] ?>-filter"></span>
					</i>	
					<img src="<?php echo Func::get_secured_image('primary-'.$afid.'.jpg',false,"primary") ?>" />
				<?php else : ?>
					<i class="sprite <?php $this->user_type($graph_data,$x,$y) ?> avatar-default<?php if($x>=2) echo "-sm"; ?>"><span class="<?php echo $graph_data[$x][$y]['account_type'] ?>-filter"></span></i>	
					<img src="<?php echo SITE_URL ?>/assets/img/avatar-default.png" />
				<?php endif; ?>				
			</div>
		<?php else: ?>
			<div class="user blank">
				<i class="sprite <?php $this->user_type($graph_data,$x,$y) ?> avatar-default<?php if($x>=2) echo "-sm"; ?>"><span class="<?php echo $graph_data[$x][$y]['account_type'] ?>-filter"></span></i>
			</div>
		<?php endif; ?>
		<?php if($x<3) : ?>
		<div class="connector">
			<div class="left">
				<span class="l1"></span>
				<span class="l2"></span>
				<span class="l3"></span>
			</div>
			<div class="right">
				<span class="l1"></span>
				<span class="l2"></span>
				<span class="l3"></span>
			</div>
		</div>
		<?php endif; ?>
		<?php
	}
	public function get_all_affiliates(){
		/*
		for future functionalities
		$l=0;
		$c=0;
		$stop=false;
		$subj[$l][$c]=$_SESSION['user_ID'];

		while(!$stop){
			foreach($subj){
				// diri nako dapit
			}
			$l++;
			$c=0;
		}		

		return $data;
		*/
	}

	public function getAccountTypeIcon($acc){
		return '<span class="acc-type '.$acc.'" title="'.$acc.'"></span>';
	}

	public function getGeneologyTextualSummary($alldata){
		
		$arr['total_downlines'] = 0;
		$arr['total_direct'] = 0;
		$arr['total_indirect'] = 0;
		$arr['total_left'] = 0;
		$arr['total_right'] = 0;

		$l = 0;
		$c = 0;

		foreach($alldata as $key=>$val){

			foreach($val as $k=>$v){
				if($v!=-1){
					$arr['total_downlines']++;

					$l=$key;
					$c=$k;
					$x = pow(2,$l)/2;

					if($c<$x) {
						$arr['total_left']++;
					}
					else {
						$arr['total_right']++;
					}
				}
				if($v['isDirect']==1){
					$arr['total_direct']++;
				}
				
			}
		}
		$arr['total_indirect'] = abs( $arr['total_downlines'] - $arr['total_direct'] );
		return $arr;
	}
}