<?php 
class Support{
	function __construct(){}

	public function send($param){
		$from = $_POST["email"]; // sender
	    $subject = $_POST["subject"];
	    $message = $_POST["message"];
	    // message lines should not exceed 70 characters (PHP rule), so wrap it
	    $message = wordwrap($message, 70);
	    // send mail
	    mail(ADMIN_EMAIL,$subject,$message,"From: $from\n");
	}
}
