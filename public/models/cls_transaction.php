<?php 
class Transaction extends DB{
	private $session;
	public function __construct($session){
		parent::__construct(); 
		$this->table = "transaction";
		$this->session = $session;
	}
	public function member_payment($data){		
		// store to db
		unset($data['a']);

		
		// get clean data array
		foreach($data as $k=>$v){
			if($v=="")
				unset($data[$k]);
		}
		
		$data['ipaddress']=$_SERVER['REMOTE_ADDR'];
		$uploaded = $this->member_payment_upload_receipt();
		if( $uploaded['result'] == 1 ){
			$data = $this->member_payment_storetodb($data, $uploaded['ext']);
			$this->member_payment_sendmail($data);	
		}			
		return 1;		
	}
	public function getmembership(){
		$data = $this->select("*",array("type"=>1,"user_ID"=>$this->session['user_ID']));

		return $data;
	}

	public function get_payment_transaction(){
		$data = $this->select(
					'*,  CONCAT(  "'.REGISTRATION_TRANSACTION_PREFIX.'", ID ) AS trans_no',
					array("user_ID"=>$_SESSION['user_ID'], "type"=>1),
					false, "", false, "ID", "DESC");
		return $data;
	}

	public function is_all_submitted_payment_trans_declined($trans_data){
		
		$is_all_declined = true;
		foreach($trans_data as $tr){
			if($tr['status']!=2){
				$is_all_declined=false; break;
			}
		}

		return $is_all_declined;
	}

	public function getalldata(){
		$data = $this->select("*", array("user_ID"=>$_SESSION['user_ID']),false,"",false,"date_transaction","DESC");
		return $data;
	}

	public function addaccount_request($_data){
		if($this->have_correct_pin($_data['pin'])){

			$cost = ($_SESSION['user_type']==1) ? BASIC_FEE : PREMIER_FEE;
			$amount = $cost * $_data['qty'];

			if(!isset($_data['wire_name'])){
				$_data['wire_name']="";
				$_data['wire_contact']="";
				$_data['wire_address']="";
			}
			else{
				$_data['bank_branch_addr']="";
				$_data['acc_no']="";
				$_data['acc_name']="";
			}

			$arr = array(
					"user_ID"=>$_SESSION['user_ID'],
					"date_transaction"=>date("Y-m-d H:i:s"),
					"type"=>4,
					"gross"=>$amount,
					"payment_option"=>$_data['payout_type'],
					"bank_branch"=>$_data['bank_branch_addr'],
					"acc_no"=>$_data['acc_no'],
					"acc_name"=>$_data['acc_name'],
					"wire_name"=>$_data['wire_name'],
					"wire_contact"=>$_data['wire_contact'],
					"wire_address"=>$_data['wire_address']
				);
			$trans_id = $this->save($arr);

			// send email
			$subject = "W{$trans_id} - Customer Request Widthrawal";
			$message = "Here are the details : ";
			
			unset($_data['pin']);
			foreach($_data as $key=>$val){
				$message .= "$key : $val \n";
			}

			$to = ADMIN_EMAIL;
			$from = "From: system@inlight.com";

			mail($to,$subject,$message);

			if($_data['payout_type']!="E-Wallet"){
				// generate activation code
				$this->generate_activation_code();
				echo 2;
			}
			else{
				echo 1;
			}			
		}
		else{
			echo "-1"; // wrong pin
		}
		exit();
	}
	public function widthraw_request($_data){
		
		if($this->have_correct_pin($_data['pin'])){

			if(!isset($_data['wire_name'])){
				$_data['wire_name']="";
				$_data['wire_contact']="";
				$_data['wire_address']="";
			}
			else{
				$_data['bank_branch_addr']="";
				$_data['acc_no']="";
				$_data['acc_name']="";
			}

			// store to database
			$arr = array(
					"user_ID"=>$_SESSION['user_ID'],
					"date_transaction"=>date("Y-m-d H:i:s"),
					"type"=>3,
					"gross"=>$_data['amount'],
					"payment_option"=>$_data['payout_type'],
					"bank_branch"=>$_data['bank_branch_addr'],
					"acc_no"=>$_data['acc_no'],
					"acc_name"=>$_data['acc_name'],
					"wire_name"=>$_data['wire_name'],
					"wire_contact"=>$_data['wire_contact'],
					"wire_address"=>$_data['wire_address']
				);
			$trans_id = $this->save($arr);

			// send email
			$subject = "W{$trans_id} - Customer Request Widthrawal";
			$message = "Here are the details : ";
			
			unset($_data['pin']);
			foreach($_data as $key=>$val){
				$message .= "$key : $val \n";
			}

			$to = ADMIN_EMAIL;
			$from = "From: system@inlight.com";

			mail($to,$subject,$message);

			echo 1;
		}
		else{
			echo "-1"; // wrong pin
		}
		exit();

	}

	public function upgade_request($_data){

		if($this->have_correct_pin($_data['pin'])){

			if($_data['payout_type']=="E-Wallet"){
				$this->save( array("account_type"=>$_data['upgrade_to']), 
							 array("ID"=>$_SESSION['user_ID']),
							 "rowCount", "user" );
			}

			switch($_data['upgrade_to']){
				case 2:
					$amount = PREMIER_FEE;
					break;
				case 3:
					$amount = SILVER_FEE + (2*PREMIER_FEE);
					break;
				case 4:
					$amount = GOLD_FEE + (2*PREMIER_FEE);
					break;
				case 5:
					$amount = DIAMOND_FEE + (2*PREMIER_FEE);
					break;
			}

			// store to database
			$arr = array(
					"user_ID"=>$_SESSION['user_ID'],
					"date_transaction"=>date("Y-m-d H:i:s"),
					"type"=>2,
					"gross"=>$amount,
					"payment_option"=>$_data['payout_type'],
					"bank_branch"=>$_data['bank_branch_addr'],
					"acc_no"=>$_data['acc_no'],
					"acc_name"=>$_data['acc_name'],
					"wire_name"=>$_data['wire_name'],
					"wire_contact"=>$_data['wire_contact'],
					"wire_address"=>$_data['wire_address']
				);
			$trans_id = $this->save($arr);

			// send email
			$subject = "W{$trans_id} - Customer Request Upgrade";
			$message = "Here are the details : ";
			
			unset($_data['pin']);
			foreach($_data as $key=>$val){
				$message .= "$key : $val \n";
			}

			$to = ADMIN_EMAIL;
			$from = "From: system@inlight.com";

			mail($to,$subject,$message);

			if($_data['payout_type']=="E-Wallet")
				echo 2;			
			else
				echo 1;
		}
		else{
			echo "-1"; // wrong pin
		}
		exit();
	}

	private function have_correct_pin($pin){
		$q = "SELECT pin FROM user WHERE ID = :ID";
		$arr = array("ID"=>$_SESSION['user_ID']);
		$con = $this->query($q, $arr);
		$data = $con->fetchAll(PDO::FETCH_ASSOC);

		if($pin==$data[0]['pin'])
			return true;
		else
			return false;
	}

	private function generate_activation_code(){
		$stop_loop = false;
		while(!$stop_loop){
			$rand = Func::random_string(6);
			// check to table			
			$data = $this->select("*",array("code"=>$rand),true,"activation_codes");

			if(count($data)!=1 || $data==""){
				$iarr = array(
					"user_ID"=>$_SESSION['user_ID'],
					"date_created"=>date("m/d/y g:i A"),
					"code"=>$rand,
					"status"=>1
					);
				$this->save($iarr, array(), "rowCount", "activation_codes");
				$stop_loop = true;				
			}
		}
		return $rand;
	}

	public function getTransactionDetails($id){
		$data = $this->select("*",array("ID"=>$id));
		return $data;
	}

















	private function member_payment_upload_receipt(){
		$filename="payment_picture_proof";
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		//$filerepo = dirname($_SERVER['DOCUMENT_ROOT']).'/web/uploads/';
		$filerepo = UPLOADS_DIR; // untested
		$temp = explode(".", $_FILES[$filename]["name"]);
		$extension = end($temp);

		if ((($_FILES[$filename]["type"] == "image/gif")
		|| ($_FILES[$filename]["type"] == "image/jpeg")
		|| ($_FILES[$filename]["type"] == "image/jpg")
		|| ($_FILES[$filename]["type"] == "image/pjpeg")
		|| ($_FILES[$filename]["type"] == "image/x-png")
		|| ($_FILES[$filename]["type"] == "image/png"))
		&& ($_FILES[$filename]["size"] < 3000000)
		&& in_array($extension, $allowedExts)) {
		  if ($_FILES[$filename]["error"] > 0) {
		    echo "Return Code: " . $_FILES[$filename]["error"] . "<br> Please go back to the previous page."; exit();
		  } else {
		    if (file_exists($filerepo . $_FILES[$filename]["name"])) {
		      echo $_FILES[$filename]["name"] . " already exists. <br> Please go back to the previous page.";  exit();
		    } else {
		      move_uploaded_file($_FILES[$filename]["tmp_name"], $filerepo . "reg-".$this->session['user_ID'].date("YmdHis").'.'.$extension ) ;
		      return array('result'=>1,"ext"=>$extension);
		    }
		  }
		} else {
		  echo "Invalid file <br />  Please go back to the previous page.";  exit();
		}
	}
	private function member_payment_storetodb($_data, $imgext){
		$data["type"]=1; // 1 = "membership" in options table. Meaning.. membership fee.
		$data["date_transaction"]=date("Y-m-d H:i:s");
		$data["status"]=0;
		if($_data["time_sent_ap"]=="PM"){
			$_data["time_sent_h"]=$_data["time_sent_h"]+12;
		}

		$data["date_sent"] = $_data['date_sent']." ".$_data["time_sent_h"].":".$_data["time_sent_m"].":00";
		$data['gross']=$_data['amount_sent'];
		$data['reference']=$_data['reference_number'];
		$data['payment_option']=$_data['payment_option'];
		$data['bank_branch']=$_data['bank_branch'];
		$data['user_ID']=$this->session['user_ID'];
		$data['sponsor_ID']=$this->session['user_sponsor_ID'];
		$data['receipt_picture']="reg-".$this->session['user_ID'].date("YmdHis").'.'.$imgext;
		$data['reg_how_many']=$_data['howmany_accounts'];

		// for wire
		$data['wire_name']=$_data['sender_name'];
		$data['wire_address']=$_data['city_province_origin'].", ".$_data['country_origin'];

		$this->save($data);
		return $data;
	}
	private function member_payment_sendmail($data){
		// send mail
		
		$to  = ADMIN_EMAIL;
		$subject = '# New Member Registered';
		$message = '
		<html>
			<head>
				<title># New Member Registered</title>
			</head>
			<body>
				<p>Here are the info : </p>';

				foreach($data as $key=>$val){
					if($key=="type"){
						if($val==1) $val="Membership";
						if($val==2) $val="Upgrade";
						if($val==3) $val="Widthrawal";
						if($val==4) $val="Purchase Account";
					}
					if($key=="gross") {
						$key="Amount";
						$val=Func::to_money($val);
					}
					if($key=="receipt_picture"){
						$val="<img style='width:300px;' src='".Func::get_secured_image($val,0)."' />";
					}
					$message .= "<strong>$key : </strong> $val <br />";
				}

		$message .= '
			</body>
		</html>
		';

		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		// Additional headers
		$headers .= 'From: Inlight Ultd Systems <admin@inlight.com>' . "\r\n";
		// Mail it
		echo mail($to, $subject, $message, $headers);
	}
}