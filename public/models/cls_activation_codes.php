<?php 
class Activation_Codes extends DB{
	public function __construct(){
		parent::__construct(); 
		$this->table = "activation_codes";
	}

	public function getData(){
		$data = $this->select("*",array("user_ID"=>$_SESSION['user_ID']));
		return $data;
	}
}