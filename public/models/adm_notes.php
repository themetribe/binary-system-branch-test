<?php
class Notes extends DB{
	public function __construct(){
		parent::__construct();
		$this->table = "note";
	}

	public function get(){
		$val = $this->select("note");
		return $val[0]['note'];
	}

	public function set($data){
		$status = $this->save(array("note"=>$data),array("ID"=>"1"));
		if($status==1)
			header('location:'.SITE_URL.'/notes');
	}
}