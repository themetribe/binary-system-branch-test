<?php
class TransactionRequests extends DB{
	public function __construct(){
		parent::__construct();
	}

	public function fetch_requests($id=''){	

		if($id==''){
			$q = "	SELECT 
						a.ID as trans_ID,
						a.user_ID,
						a.gross,
						a.reg_how_many,
						CASE b.account_type
							WHEN 1 THEN 'Basic'
							WHEN 2 THEN 'Premier'
							WHEN 3 THEN 'Silver (w/ 2 Premium)'
							WHEN 4 THEN 'Gold (w/ 2 Premium)'
							WHEN 5 THEN 'Diamond (w/ 2 Premium)'
						END as account_type,
						CONCAT(b.lname,', ',b.fname,' ',b.mname) as user_fullname,
						CASE a.type 
							WHEN 1 THEN 'Membership'
							WHEN 2 THEN 'Upgrade'
							WHEN 3 THEN 'Widthraw'
							WHEN 4 THEN 'Add Account'
						END as req_type,
						a.date_transaction as req_date,
						a.receipt_picture as proof,
						CASE a.status
							WHEN 0 THEN 'Pending'
							WHEN 1 THEN 'Completed'
							WHEN 2 THEN 'Declined'
						END as status
					FROM transaction as a, user as b
					WHERE a.user_ID = b.ID
					ORDER BY a.ID DESC";
			$arr = array();	
		}
		else{
			$q = "	SELECT 
					a.ID as trans_ID,
					a.user_ID,
					a.gross,
					a.reg_how_many,
					CASE b.account_type
						WHEN 1 THEN 'Basic'
						WHEN 2 THEN 'Premier'
						WHEN 3 THEN 'Silver (w/ 2 Premium)'
						WHEN 4 THEN 'Gold (w/ 2 Premium)'
						WHEN 5 THEN 'Diamond (w/ 2 Premium)'
					END as account_type,
					CONCAT(b.lname,', ',b.fname,' ',b.mname) as user_fullname,
					CASE a.type 
						WHEN 1 THEN 'Membership'
						WHEN 2 THEN 'Upgrade'
						WHEN 3 THEN 'Widthraw'
						WHEN 4 THEN 'Add Account'
					END as req_type,
					a.date_transaction as req_date,
					a.receipt_picture as proof,
					CASE a.status
						WHEN 0 THEN 'Pending'
						WHEN 1 THEN 'Completed'
						WHEN 2 THEN 'Declined'
					END as status
				FROM transaction as a, user as b
				WHERE a.user_ID = b.ID
					AND a.ID = :ID
				ORDER BY a.ID DESC";
			$arr = array("ID"=>$id);
		}
		

		$con = $this->query($q,$arr);
		$data = $con->fetchAll(PDO::FETCH_ASSOC);

		return $data;
	}

	public function getUserAndDetails($trans_ID){
		$q = " 	SELECT a.*			
				FROM user as a, transaction as b
				WHERE a.ID = b.user_ID AND
				b.ID = :bID ";
		$con = $this->query($q,array("bID"=>$trans_ID));
		$data = $con->fetchAll(PDO::FETCH_ASSOC);

		return $data[0];
	}
	public function activateAccount($trans_ID){
		include('cls_geneology.php');
		include('cls_user.php');
		
		$geneology = new Geneology();
		$user = new User();

		$trans_details = $this->fetch_requests($trans_ID);
		$user_and_details = $this->getUserAndDetails($trans_ID);	

		$trans_details = $trans_details[0];

		// check if premium/basic and how many accounts
		if($user_and_details['account_type']<=2 && $trans_details['reg_how_many']>=3){

			$position = 1;
			$arr = array();

			// i'm not sure if this block is working or not
				// update user.upline and position 	// activate user.status			
			$upline_and_uposition = $user->auto_insert_geneology_user_table($user_and_details); // will activate status automatically
			// add geneology
			$status = $geneology->update($upline_and_uposition,$user_and_details['ID']);
			// <- until here

			if($trans_details['reg_how_many']>=3){

				$arr[0] = $user_and_details;
				$arr[0]['account_type'] = $user_and_details['account_type'];
				$arr[0]['uposition'] = 1;
				$arr[0]['sposition'] = 1;
				$arr[0]['username'] = $user_and_details['username']."2";
				$arr[0]['upline_id'] = $arr[0]["ID"];
				$arr[0]['status']=1;
				$arr[0]['sponsor_id']=$user_and_details['ID'];
				unset($arr[0]["ID"]);
				$arr_2_ID = $user->add($arr[0],false,true);
				// geneology
				$gen[0]['user_ID'] = $arr_2_ID;
				$gen[0]['child_left_ID'] = "0";
				$gen[0]['child_right_ID'] = "0";



				$arr[1] = $user_and_details;				
				$arr[1]['account_type'] = $user_and_details['account_type'];
				$arr[1]['uposition'] = 2;
				$arr[1]['sposition'] = 2;
				$arr[1]['username'] = $user_and_details['username']."3";		
				$arr[1]['upline_id'] = $arr[0]['upline_id'];
				$arr[1]['status']=1;
				$arr[1]['sponsor_id']=$user_and_details['ID'];
				unset($arr[1]["ID"]);
				$arr_4_ID = $user->add($arr[1],false,true);
				// geneology
				$gen[1]['user_ID'] = $arr_4_ID;
				$gen[1]['child_left_ID'] = "0";
				$gen[1]['child_right_ID'] = "0";

			}
			if($trans_details['reg_how_many']==7){

				$arr[2] = $user_and_details;
				$arr[2]['status']=1;
				unset($arr[2]["ID"]);
				$arr[2]['upline_id'] = $arr_2_ID;
				$arr[2]['account_type'] = $user_and_details['account_type'];
				$arr[2]['uposition'] = 1;
				$arr[2]['sposition'] = 1;
				$arr[2]['username'] = $user_and_details['username']."4";
				$arr[2]['sponsor_id']=$arr_2_ID;
				$user_temp_id = $user->add($arr[2],false,true);
				// geneology
				$gen[2]['user_ID'] = $user_temp_id;
				$gen[2]['child_left_ID'] = "0";
				$gen[2]['child_right_ID'] = "0";
				$gen[0]['child_left_ID'] = $user_temp_id;


				$arr[3] = $user_and_details;
				$arr[3]['status']=1;
				unset($arr[3]["ID"]);
				$arr[3]['upline_id'] = $arr_2_ID;
				$arr[3]['sponsor_id']=$arr_2_ID;
				$arr[3]['account_type'] = $user_and_details['account_type'];
				$arr[3]['uposition'] = 2;
				$arr[3]['sposition'] = 2;
				$arr[3]['username'] = $user_and_details['username']."5";
				$user_temp_id = $user->add($arr[3],false,true);
				// geneology
				$gen[3]['user_ID'] = $user_temp_id;
				$gen[3]['child_left_ID'] = "0";
				$gen[3]['child_right_ID'] = "0";
				$gen[0]['child_right_ID'] = $user_temp_id;

				$arr[4] = $user_and_details;
				$arr[4]['status']=1;
				unset($arr[4]["ID"]);
				$arr[4]['upline_id'] = $arr_4_ID;
				$arr[4]['sponsor_id']=$arr_4_ID;
				$arr[4]['account_type'] = $user_and_details['account_type'];
				$arr[4]['uposition'] = 1;
				$arr[4]['sposition'] = 1;
				$arr[4]['username'] = $user_and_details['username']."6";
				$user_temp_id = $user->add($arr[4],false,true);
				// geneology
				$gen[4]['user_ID'] = $user_temp_id;
				$gen[4]['child_left_ID'] = "0";
				$gen[4]['child_right_ID'] = "0";
				$gen[1]['child_left_ID'] = $user_temp_id;


				$arr[5] = $user_and_details;
				$arr[5]['status']=1;
				unset($arr[5]["ID"]);
				$arr[5]['upline_id'] = $arr_4_ID;
				$arr[5]['sponsor_id']=$arr_4_ID;
				$arr[5]['account_type'] = $user_and_details['account_type'];
				$arr[5]['uposition'] = 2;
				$arr[5]['sposition'] = 2;
				$arr[5]['username'] = $user_and_details['username']."7";
				$user_temp_id = $user->add($arr[5],false,true);
				// geneology
				$gen[5]['user_ID'] = $user_temp_id;
				$gen[5]['child_left_ID'] = "0";
				$gen[5]['child_right_ID'] = "0";
				$gen[1]['child_right_ID'] = $user_temp_id;
			}

			// generate the first item's geneologies.
			$first_item = array(
					'user_ID'=>$user_and_details['ID'],
					"child_left_ID"=>$arr_2_ID,
					"child_right_ID"=> $arr_4_ID
				);
			$status = $geneology->add_for_multiple($first_item);
			if($status==0){
				echo "error";
			}
			// generate the multiple geneologies.
			foreach($gen as $key=>$val){
				$status = $geneology->add_for_multiple($val);
				if($status==0){
					echo "error";
				}	
			}	
			// // update the parent of the first item
			// if($user_and_details["uposition"]==1)
			// 	$temp_arr = array("child_left_ID"=>$user_and_details['ID']);
			// else
			// 	$temp_arr = array("child_right_ID"=>$user_and_details['ID']);
			// $status = $geneology->update_specific($temp_arr,$user_and_details['upline_id']);
			// if($status==0){
			// 	echo "error";
			// }

			
		}
		else{
			// update user.upline and position 	// activate user.status
			$upline_and_uposition = $user->auto_insert_geneology_user_table($user_and_details); // will activate status automatically
			// add geneology
			$status = $geneology->update($upline_and_uposition,$user_and_details['ID']);
			if($status>0){
				$status = $geneology->add($user_and_details, $user_and_details['ID']);
				if($status==0){
					echo "error ";
				}							
			}
		}

		// set transaction as complete
		$status = $this->save(array('status'=>1), array('ID'=>$trans_ID),"rowCount","transaction");

		if($status==1)
			header('location:'.SITE_URL.'/transaction_requests');
	}
}
?>