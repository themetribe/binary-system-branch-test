-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 09, 2014 at 07:38 PM
-- Server version: 5.5.37-35.1-log
-- PHP Version: 5.4.23

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `franciu5_inlight`
--

-- --------------------------------------------------------

--
-- Table structure for table `activation_codes`
--

DROP TABLE IF EXISTS `activation_codes`;
CREATE TABLE IF NOT EXISTS `activation_codes` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `user_ID` int(10) unsigned zerofill NOT NULL,
  `used_by_ID` int(10) unsigned zerofill NOT NULL,
  `date_created` datetime NOT NULL,
  `date_used` datetime NOT NULL,
  `code` varchar(6) NOT NULL,
  `status` int(1) NOT NULL COMMENT '1 : available | 0 : used',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `geneology`
--

DROP TABLE IF EXISTS `geneology`;
CREATE TABLE IF NOT EXISTS `geneology` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `user_ID` int(10) unsigned zerofill NOT NULL,
  `child_left_ID` int(10) unsigned zerofill NOT NULL COMMENT 'from "user" table',
  `child_right_ID` int(10) unsigned zerofill NOT NULL COMMENT 'from "user" table',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='this must be inserted on "Sign Up" phase together with "users" table.' AUTO_INCREMENT=316 ;

--
-- Dumping data for table `geneology`
--

INSERT INTO `geneology` (`ID`, `user_ID`, `child_left_ID`, `child_right_ID`) VALUES
(1, 0000000001, 0000000002, 0000000003),
(2, 0000000002, 0000000287, 0000000000),
(3, 0000000003, 0000000000, 0000000000),
(225, 0000000287, 0000000288, 0000000289),
(226, 0000000288, 0000000293, 0000000290),
(227, 0000000289, 0000000321, 0000000300),
(228, 0000000290, 0000000291, 0000000292),
(229, 0000000291, 0000000294, 0000000000),
(230, 0000000292, 0000000000, 0000000000),
(231, 0000000293, 0000000295, 0000000296),
(232, 0000000295, 0000000297, 0000000000),
(233, 0000000296, 0000000000, 0000000000),
(234, 0000000297, 0000000298, 0000000299),
(235, 0000000298, 0000000301, 0000000000),
(236, 0000000299, 0000000000, 0000000000),
(237, 0000000294, 0000000000, 0000000000),
(238, 0000000301, 0000000302, 0000000303),
(239, 0000000302, 0000000306, 0000000000),
(240, 0000000303, 0000000314, 0000000344),
(241, 0000000300, 0000000304, 0000000305),
(242, 0000000304, 0000000312, 0000000338),
(243, 0000000305, 0000000315, 0000000000),
(244, 0000000306, 0000000307, 0000000308),
(245, 0000000307, 0000000309, 0000000000),
(246, 0000000308, 0000000343, 0000000000),
(247, 0000000309, 0000000310, 0000000311),
(248, 0000000310, 0000000313, 0000000000),
(249, 0000000311, 0000000000, 0000000000),
(250, 0000000313, 0000000000, 0000000000),
(251, 0000000314, 0000000316, 0000000000),
(252, 0000000312, 0000000317, 0000000318),
(253, 0000000317, 0000000000, 0000000000),
(254, 0000000318, 0000000000, 0000000000),
(255, 0000000316, 0000000319, 0000000320),
(256, 0000000319, 0000000324, 0000000000),
(257, 0000000320, 0000000000, 0000000000),
(258, 0000000315, 0000000000, 0000000000),
(259, 0000000321, 0000000322, 0000000323),
(260, 0000000322, 0000000352, 0000000353),
(261, 0000000323, 0000000354, 0000000355),
(262, 0000000324, 0000000325, 0000000326),
(263, 0000000325, 0000000328, 0000000000),
(264, 0000000326, 0000000000, 0000000000),
(266, 0000000328, 0000000329, 0000000330),
(267, 0000000329, 0000000331, 0000000000),
(268, 0000000330, 0000000000, 0000000000),
(269, 0000000331, 0000000332, 0000000333),
(270, 0000000332, 0000000334, 0000000000),
(271, 0000000333, 0000000000, 0000000000),
(272, 0000000334, 0000000335, 0000000336),
(273, 0000000335, 0000000371, 0000000000),
(274, 0000000336, 0000000000, 0000000000),
(276, 0000000338, 0000000000, 0000000000),
(277, 0000000343, 0000000348, 0000000349),
(278, 0000000348, 0000000000, 0000000000),
(279, 0000000349, 0000000000, 0000000000),
(280, 0000000344, 0000000000, 0000000000),
(284, 0000000352, 0000000000, 0000000000),
(285, 0000000353, 0000000000, 0000000000),
(286, 0000000354, 0000000000, 0000000000),
(287, 0000000355, 0000000356, 0000000357),
(288, 0000000356, 0000000000, 0000000358),
(289, 0000000357, 0000000361, 0000000000),
(290, 0000000358, 0000000359, 0000000360),
(291, 0000000359, 0000000000, 0000000000),
(292, 0000000360, 0000000000, 0000000364),
(293, 0000000361, 0000000362, 0000000363),
(294, 0000000362, 0000000370, 0000000000),
(295, 0000000363, 0000000000, 0000000000),
(296, 0000000364, 0000000365, 0000000366),
(297, 0000000365, 0000000000, 0000000000),
(298, 0000000366, 0000000000, 0000000367),
(299, 0000000367, 0000000368, 0000000369),
(300, 0000000368, 0000000000, 0000000000),
(301, 0000000369, 0000000000, 0000000381),
(302, 0000000370, 0000000000, 0000000000),
(303, 0000000371, 0000000372, 0000000373),
(304, 0000000372, 0000000376, 0000000000),
(305, 0000000373, 0000000000, 0000000000),
(309, 0000000376, 0000000377, 0000000378),
(310, 0000000377, 0000000379, 0000000000),
(311, 0000000378, 0000000000, 0000000000),
(312, 0000000379, 0000000382, 0000000000),
(313, 0000000381, 0000000000, 0000000000),
(314, 0000000382, 0000000383, 0000000000),
(315, 0000000383, 0000000000, 0000000000);

-- --------------------------------------------------------

--
-- Table structure for table `library`
--

DROP TABLE IF EXISTS `library`;
CREATE TABLE IF NOT EXISTS `library` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `date_uploaded` datetime NOT NULL,
  `excerpt` varchar(500) NOT NULL,
  `file_src` varchar(300) NOT NULL COMMENT 'path of file, this should be protected',
  `thumb_path` varchar(50) NOT NULL DEFAULT 'thumbnail-default.jpg',
  `category` int(10) NOT NULL COMMENT '1: financial | 2:physical | 3:mental | 4:spiritual | 5:relationship | 6:Educational | 7:Inspirational | 8:Motivational  | 9:Health | 10:Tutorial',
  `ebook_category` varchar(100) NOT NULL,
  `type` int(1) NOT NULL COMMENT '1:ebook | 2:audio | 3:video | 4:system tour | 5:inlight | 6:quotes',
  `mentor` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=230 ;

--
-- Dumping data for table `library`
--

INSERT INTO `library` (`ID`, `title`, `date_uploaded`, `excerpt`, `file_src`, `thumb_path`, `category`, `ebook_category`, `type`, `mentor`) VALUES
(6, 'All About Perfumes', '2014-09-25 00:00:00', '', 'All_About_Perfumes.zip', 'All About Perfumes.png', 0, 'Beauty and Fashion', 1, ''),
(7, 'Anti-Ageing and Skin Care Made Easy', '2014-09-25 00:00:00', '', 'Anti-Ageing_and_Skin_Care_Made_Easy.zip', 'Anti-Ageing_and_Skin_Care_Made_Easy.png', 0, 'Beauty and Fashion', 1, ''),
(8, 'Body Detox Made Easy', '2014-09-25 00:00:00', '', 'Body_Detox_Made_Easy.zip', 'Body_Detox_Made_Easy.png', 0, 'Beauty and Fashion', 1, ''),
(9, 'Boost Your Memory Power', '2014-09-25 00:00:00', '', 'Boost_Your_Memory_Power.zip', 'Boost_Your_Memory_Power.png', 0, 'Body and Mind', 1, ''),
(10, 'Boost Your Self Esteem', '2014-09-25 00:00:00', '', 'Boost_Your_Self_Esteem.zip', 'Boost_Your_Self_Esteem.png', 0, 'Body and Mind', 1, ''),
(11, 'Massage Therapy', '2014-09-25 00:00:00', '', 'Massage_Therapy.zip', 'Massage_Therapy.png', 0, 'Body and Mind', 1, ''),
(12, 'Achieve a Better Credit_Score', '2014-09-25 00:00:00', '', 'Achieve_a_Better_Credit_Score.zip', 'Achieve_a_Better_Credit_Score.png', 0, 'Business and Financial', 1, ''),
(13, 'Achieve Prosperous Living', '2014-09-25 00:00:00', '', 'Achieve_Prosperous_Living.zip', 'Achieve_Prosperous_Living.png', 0, 'Business and Financial', 1, ''),
(14, 'Attract Anything You Want in Life', '2014-09-25 00:00:00', '', 'Attract_Anything_You_Want_in_Life.zip', 'Attract_Anything_You_Want_in_Life.png', 0, 'Business and Financial', 1, ''),
(15, 'Business Building Basics', '2014-09-25 00:00:00', '', 'Business_Building_Basics.zip', 'Business_Building_Basics.png', 0, 'Business and Financial', 1, ''),
(16, 'Buying and Maintaining a Car', '2014-09-25 00:00:00', '', 'Buying_and_Maintaining_a_Car.zip', 'Buying_and_Maintaining_a_Car.png', 0, 'Business and Financial', 1, ''),
(17, 'Direct Mail Secrets Exposed', '2014-09-25 00:00:00', '', 'Direct_Mail_Secrets_Exposed.zip', 'Direct_Mail_Secrets_Exposed.png', 0, 'Business and Financial', 1, ''),
(18, 'Family Budget Demystified', '2014-09-25 00:00:00', '', 'Family_Budget_Demystified.zip', 'Family_Budget_Demystified.png', 0, 'Business and Financial', 1, ''),
(19, 'Get the Most Out of Your Timeshares', '2014-09-25 00:00:00', '', 'Get_the_Most_Out_of_Your_Timeshares.zip', 'Get_the_Most_Out_of_Your_Timeshares.png', 0, 'Business and Financial', 1, ''),
(20, 'Guerrilla Marketing Explained', '2014-09-25 00:00:00', '', 'Guerrilla_Marketing_Explained.zip', 'Guerrilla_Marketing_Explained.png', 0, 'Business and Financial', 1, ''),
(21, 'Health and Wealth Magnetism', '2014-09-25 00:00:00', '', 'Health_and_Wealth_Magnetism.zip', 'Health_and_Wealth_Magnetism.png', 0, 'Business and Financial', 1, ''),
(22, 'Health Insurance and Savings', '2014-09-25 00:00:00', '', 'Health_Insurance_and_Savings.zip', 'Health_Insurance_and_Savings.png', 0, 'Business and Financial', 1, ''),
(23, 'Home Automation Inside Out', '2014-09-25 00:00:00', '', 'Home_Automation_Inside_Out.zip', 'Home_Automation_Inside_Out.png', 0, 'Business and Financial', 1, ''),
(24, 'Lean Manufacturing Principles', '2014-09-25 00:00:00', '', 'Lean_Manufacturing_Principles.zip', 'Lean_Manufacturing_Principles.png', 0, 'Business and Financial', 1, ''),
(25, 'Marketing On A Budget', '2014-09-25 00:00:00', '', 'Marketing_On_A_Budget.zip', 'Marketing_On_A_Budget.png', 0, 'Business and Financial', 1, ''),
(26, 'Retirement Planning', '2014-09-25 00:00:00', '', 'Retirement_Planning.zip', 'Retirement_Planning.png', 0, 'Business and Financial', 1, ''),
(27, 'Sell Real Eastate for Profits', '2014-09-25 00:00:00', '', 'Sell_Real_Eastate_for_Profits.zip', 'Sell_Real_Eastate_for_Profits.png', 0, 'Business and Financial', 1, ''),
(28, 'Successfully Plan and Organize an Event', '2014-09-25 00:00:00', '', 'Successfully_Plan_and_Organize_an_Event.zip', 'Successfully_Plan_and_Organize_an_Event.png', 0, 'Business and Financial', 1, ''),
(29, 'Taxes Made Easy', '2014-09-25 00:00:00', '', 'Taxes_Made_Easy.zip', 'Taxes_Made_Easy.png', 0, 'Business and Financial', 1, ''),
(30, 'The Art of Coin Collecting', '2014-09-25 00:00:00', '', 'The_Art_of_Coin_Collecting.zip', 'The_Art_of_Coin_Collecting.png', 0, 'Business and Financial', 1, ''),
(31, 'Unleash the Financial Giant Within!', '2014-09-25 00:00:00', '', 'Unleash_the_Financial_Giant_Within!.zip', 'Unleash_the_Financial_Giant_Within!.png', 0, 'Business and Financial', 1, ''),
(32, 'Ace Any Job Interview', '2014-09-26 00:00:00', '', 'Ace_Any_Job_Interview.zip', 'Ace_Any_Job_Interview.png', 0, 'Career', 1, ''),
(33, 'Career Planning Made Easy', '2014-09-26 00:00:00', '', 'Career_Planning_Made_Easy.zip', 'Career_Planning_Made_Easy.png', 0, 'Career', 1, ''),
(34, 'Become a Successful Public Speaker', '2014-09-26 00:00:00', '', 'Become_a_Successful_Public_Speaker.zip', 'Become_a_Successful_Public_Speaker.png', 0, 'Communications', 1, ''),
(35, '40 Premium WordPress Themes', '2014-09-26 00:00:00', '', '40_Premium_WordPress_Themes.zip', '', 0, 'Computer and Internet', 1, ''),
(36, '100 Icons', '2014-09-26 00:00:00', '', '100_Icons.zip', '', 0, 'Computer and Internet', 1, ''),
(37, '250 WordPress Plugins', '2014-09-26 00:00:00', '', '250_WordPress_Plugins.zip', '250_WordPress_Plugins.png', 0, 'Computer and Internet', 1, ''),
(38, 'Keep Your PC Safe from Virus and Data Loss', '2014-09-26 00:00:00', '', 'Keep_Your_PC_Safe_from_Virus_and_Data_Loss.zip', 'Keep_Your_PC_Safe_from_Virus_and_Data_Loss.png', 0, 'Computer and Internet', 1, ''),
(39, 'Manage Life Before and After Divorce', '2014-09-26 00:00:00', '', 'Manage_Life_Before_and_After_Divorce.zip', 'Manage_Life_Before_and_After_Divorce.png', 0, 'Drama', 1, ''),
(40, 'Newbies University', '2014-09-26 00:00:00', '', 'Newbies_University.zip', 'Newbies_University.png', 0, 'Educational', 1, ''),
(41, 'Online Education Made Easy', '2014-09-26 00:00:00', '', 'Online_Education_Made_Easy.png', 'Online_Education_Made_Easy.png', 0, 'Educational', 1, ''),
(42, 'Successfully Homeschool Your Child', '2014-09-26 00:00:00', '', 'Successfully_Homeschool_Your_Child.zip', 'Successfully_Homeschool_Your_Child.png', 0, 'Educational', 1, ''),
(43, 'Understanding Scholarships', '2014-09-26 00:00:00', '', 'Understanding_Scholarships.zip', 'Understanding_Scholarships.png', 0, 'Educational', 1, ''),
(44, 'Unforgettable Camping Vacation', '2014-09-26 00:00:00', '', 'Unforgettable_Camping_Vacation.zip', 'Unforgettable_Camping_Vacation.png', 0, 'Educational', 1, ''),
(45, 'Hunting Mastery', '2014-09-26 00:00:00', '', 'Hunting_Mastery.zip', 'Hunting_Mastery.png', 0, 'Environment', 1, ''),
(46, '6 Pack Abs', '2014-09-26 00:00:00', '', '6_Pack_Abs.zip', '6_Pack_Abs.png', 0, 'Fitness', 1, ''),
(47, 'Advantages of Being Vegetarian', '2014-09-26 00:00:00', '', 'Advantages_of_Being_Vegetarian.zip', 'Advantages_of_Being_Vegetarian.png', 0, 'Fitness', 1, ''),
(48, 'Burn Calories and Stay Fit Forever', '2014-09-26 00:00:00', '', 'Burn_Calories_and_Stay_Fit_Forever.zip', 'Burn_Calories_and_Stay_Fit_Forever.png', 0, 'Fitness', 1, ''),
(49, 'Complete Body Fitness', '2014-09-26 00:00:00', '', 'Complete_Body_Fitness.zip', 'Complete_Body_Fitness.png', 0, 'Fitness', 1, ''),
(50, 'Diet and Weight Loss', '2014-09-26 00:00:00', '', 'Diet_and_Weight_Loss.zip', 'Diet_and_Weight_Loss.png', 0, 'Fitness', 1, ''),
(51, 'Lose Weight wth the Right_Food', '2014-09-26 00:00:00', '', 'Lose_Weight_wth_the_Right_Food.zip', 'Lose_Weight_wth_the_Right_Food.png', 0, 'Fitness', 1, ''),
(52, 'Maximize the Power of Yoga', '2014-09-26 00:00:00', '', 'Maximize_the_Power_of_Yoga.zip', 'Maximize_the_Power_of_Yoga.png', 0, 'Fitness', 1, ''),
(53, 'Pilates and Complete Body Fitness', '2014-09-26 00:00:00', '', 'Pilates_and_Complete_Body_Fitness.zip', 'Pilates_and_Complete_Body_Fitness.png', 0, 'Fitness', 1, ''),
(54, 'Ultimate Body Building and Fitness', '2014-09-26 00:00:00', '', 'Ultimate_Body_Building_and_Fitness.zip', 'Ultimate_Body_Building_and_Fitness.png', 0, 'Fitness', 1, ''),
(55, 'Cooking Mastery', '2014-09-26 00:00:00', '', 'Cooking_Mastery.zip', 'Cooking_Mastery.png', 0, 'Food and Recipes', 1, ''),
(56, 'Backyard Ideas for Fun and Frolic', '2014-09-26 00:00:00', '', 'Backyard_Ideas_for_Fun_and_Frolic.zip', 'Backyard_Ideas_for_Fun_and_Frolic.png', 0, 'Games', 1, ''),
(57, 'Golf for Seniors', '2014-09-26 00:00:00', '', 'Golf_for_Seniors.zip', 'Golf_for_Seniors.png', 0, 'Games', 1, ''),
(58, 'Golf Gift Ideas', '2014-09-26 00:00:00', '', 'Golf_Gift_Ideas.zip', 'Golf_Gift_Ideas.png', 0, 'Games', 1, ''),
(59, 'Hunting Video Games', '2014-09-26 00:00:00', '', 'Hunting_Video_Games.zip', 'Hunting_Video_Games.png', 0, 'Games', 1, ''),
(60, 'Snowboarding Fun', '2014-09-26 00:00:00', '', 'Snowboarding_Fun.zip', 'Snowboarding_Fun.png', 0, 'Games', 1, ''),
(61, 'Understanding and Enjoying Golf', '2014-09-26 00:00:00', '', 'Understanding_and_Enjoying_Golf.zip', 'Understanding_and_Enjoying_Golf.png', 0, 'Games', 1, ''),
(62, 'Challenges to Pursue', '2014-10-03 00:00:00', '', 'Challenges to Pursue.mp4', 'challenges_to_pursue.png', 1, '', 3, 'Jim Rohn'),
(63, 'Goal Setting Workshop', '2014-10-03 00:00:00', '', 'Goal%20Setting%20Workshop%20%20%20by%20Jim%20Rohn,%20hosted%20by%20Kendal%20Robinson,%20Executive%20Presidents%20Team%20Member.mp4', 'GoalSettingWorkshop.png', 1, '', 3, 'Jim Rohn'),
(64, 'How to Avoid Being Broke and Stupid', '0000-00-00 00:00:00', '', 'Jim Rohn- How to Avoid Being Broke and Stupid.mp4', 'jrHowtoavoid.png', 1, '', 3, 'Jim Rohn'),
(65, 'How to have Your Best Year Ever 2 of 3', '0000-00-00 00:00:00', '', 'bestyear2.mp4', 'jrHowtohavebestyear.png', 1, '', 3, 'Jim  Rohn'),
(66, 'How to have Your Best Year Ever (1 of 3)', '2014-10-03 00:00:00', '', 'bestyear1.mp4', 'jrHowtohavebestyear1.png', 1, '', 3, 'Jim  Rohn'),
(67, 'How to have Your Best Year Ever (3 of 3)', '0000-00-00 00:00:00', '', 'bestyear3.mp4', 'jrHowtohavebestyear3.png', 1, '', 3, 'Jim  Rohn'),
(68, 'The Mentor Of Millionaires', '0000-00-00 00:00:00', '', 'Jim Rohn Quotes - The Mentor Of Millionaires.mp4', 'jrTheMentorOfMillionaires.png', 1, '', 3, 'Jim Rohn'),
(69, 'Setting Goals Part 1', '0000-00-00 00:00:00', '', 'Jim Rohn Setting Goals Part 1.mp4', 'jrSettingGoals1.png', 1, '', 3, 'Jim Rohn'),
(70, 'Setting Goals Part 2', '0000-00-00 00:00:00', '', 'Jim Rohn Setting Goals Part 2.mp4', 'jrSettingGoals2.png', 1, '', 3, 'Jim Rohn'),
(71, 'Six Days of Labor, One Day of Rest', '0000-00-00 00:00:00', '', 'Jim Rohn- Six Days of Labor, One Day of Rest.mp4', 'jrsixdaysoflabor.png', 1, '', 3, 'Jim Rohn'),
(72, 'SUCCESS is something you ATTRACT by the person you BECOME', '0000-00-00 00:00:00', '', 'successyouattract.mp4', 'jrsuccessyouattracts.png', 1, '', 3, 'Jim Rohn'),
(73, 'Three Keys To Greatness', '0000-00-00 00:00:00', '', 'threeKeysToGreatness.mp4', 'jr3keystogreatness.jpg', 1, '', 3, 'Jim Rohn'),
(74, 'Tribute Video', '2014-10-03 00:00:00', '', 'Jim Rohn Tribute Video.mp4', 'jrtribute.png', 1, '', 3, 'Jim Rohn'),
(75, 'You Must DREAM!', '2014-10-03 00:00:00', '', 'Jim Rohn - You Must DREAM!.mp4', 'jsyoumustdream.jpg', 1, '', 3, 'Jim Rohn'),
(76, 'Motivational Success Quotes - In a Minute', '2014-10-03 00:00:00', '', 'Motivational Success Quotes - In a Minute.mp4', 'jsmotivation.jpg', 1, '', 3, 'Jim Rohn'),
(77, 'Must Watch - Most Awesome Advice EVER', '2014-10-03 00:00:00', '', 'Must Watch - Most Awesome Advice EVER.mp4', 'jrmostawesomeadvice.jpg', 1, '', 3, 'Jim Rohn'),
(78, 'The Day That Turns Your Life Around', '2014-10-03 00:00:00', '', 'The Day That Turns Your Life Around (Jim Rohn).mp4', 'jrtheday.jpg', 1, '', 3, 'Jim Rohn'),
(79, 'The Psychology of Wealth', '2014-10-03 00:00:00', '', 'The Psychology of Wealth Jim Rohn.mp4', 'jsthepsychologyofwealth.png', 1, '', 3, 'Jim Rohn'),
(80, 'Being a Powerful Leader = Being a...', '0000-00-00 00:00:00', '', 'Anthony Robbins - Being a Powerful Leader = Being a... _ Facebook.mp4', 'arbeingapowerfulleader.png', 1, '', 3, 'Anthony Robbins'),
(81, '5 Keys to Break Through Stress', '0000-00-00 00:00:00', '', 'Anthony Robbins - Tony Robbins_ 5 Keys to Break Through Stress _ Facebook.mp4', 'ar5keysstress.png', 1, '', 3, 'Anthony Robbins'),
(82, 'Tony Robbins and T. Boone Pickens', '0000-00-00 00:00:00', '', 'Anthony Robbins - Tony Robbins and T. Boone Pickens on... _ Facebook.mp4', 'artony.png', 1, '', 3, 'Anthony Robbins'),
(83, 'An Extraordinary Life', '0000-00-00 00:00:00', '', 'An Extraordinary Life.mp4', 'arextraordinarylife.png', 1, '', 3, 'Anthony Robbins'),
(84, 'Find Your True Passion', '0000-00-00 00:00:00', '', 'Anthony Robbins - Find Your True Passion _ Facebook.mp4', 'arfindpassion.png', 1, '', 3, 'Anthony Robbins'),
(85, 'Tony''s Rapid Planning Method', '0000-00-00 00:00:00', '', 'Anthony Robbins - Tony''s Rapid Planning Method _ Facebook.mp4', 'arrapidplanning.png', 1, '', 3, 'Anthony Robbins'),
(86, 'Unleash The Power Within', '0000-00-00 00:00:00', '', 'Anthony Robbins - Unleash The Power Within (Tony Robbins... _ Facebook.mp4', 'arunleashthepower.png', 1, '', 3, 'Anthony Robbins'),
(87, 'How can you overcome fear part 1', '2014-10-03 00:00:00', '', 'How can you overcome fear part 1.mp4', 'cthow1.png', 1, '', 3, 'Chinky Tan'),
(88, 'How can you overcome fear part 2', '2014-10-03 00:00:00', '', 'How can you overcome fear part 2.mp4', 'cthow2.png', 1, '', 3, 'Chinky Tan'),
(89, 'Change Your Life In 90 Days Part 1 of 5', '2014-10-03 00:00:00', '', 'Change Your Life In 90 Days Part 1 of 5.mp4', 'ewchang1.png', 1, '', 3, 'Eric Worre'),
(90, 'Change Your Life In 90 Days Part 2 of 5', '2014-10-03 00:00:00', '', 'Change Your Life In 90 Days Part 2 of 5.mp4', 'ewchang2.png', 1, '', 3, 'Eric Worre'),
(91, 'Change Your Life In 90 Days Part 3 of 5', '2014-10-03 00:00:00', '', 'Change Your Life In 90 Days Part 3 of 5.mp4', 'ewchang3.png', 1, '', 3, 'Eric Worre'),
(92, 'Change Your Life In 90 Days Part 4 of 5', '2014-10-03 00:00:00', '', 'Change Your Life In 90 Days Part 4 of 5.mp4', 'ewchang4.png', 1, '', 3, 'Eric Worre'),
(93, 'Change Your Life In 90 Days Part 5 of 5', '2014-10-03 00:00:00', '', 'Change Your Life In 90 Days Part 5 of 5.mp4', 'ewchang5.png', 1, '', 3, 'Eric Worre'),
(94, 'The Perfect Business - Network Marketing', '2014-10-06 00:00:00', '', 'Robert Kiyosaki The Perfect Business _ Network Marketing _ Royale Business Club.mp4', 'rkperfectbusiness.jpg', 1, '', 3, 'Robert Kiyosaki'),
(95, 'Inlight Opener', '2014-10-06 00:00:00', '', 'InlightOpener.mp4', 'opener.jpg', 10, '', 5, ''),
(96, 'Inlight Premier', '2014-10-06 00:00:00', '', 'InlightPremierVid.mp4', 'premier.jpg', 10, '', 5, ''),
(97, 'Homepage Intro Video', '2014-10-06 00:00:00', '', 'IntroVid.mp4', 'intro.jpg', 10, '', 5, ''),
(98, 'Amazing Facts to Blow your Mind', '2014-10-07 00:00:00', '', 'facts1.mp4', 'thumbnail-default.jpg', 2, '', 3, ''),
(99, 'Amazing Facts to Blow Your Mind Pt. 2', '2014-10-07 00:00:00', '', 'facts2.mp4', 'thumbnail-default.jpg', 2, '', 3, ''),
(100, 'Amazing Facts To Blow Your Mind Pt. 3', '2014-10-07 00:00:00', '', 'fact3.mp4', 'thumbnail-default.jpg', 2, '', 3, ''),
(101, 'The Scientific Power of Thought', '0000-00-00 00:00:00', '', 'powerofthought.mp4', 'thumbnail-default.jpg', 3, '', 3, ''),
(102, 'The Science of Aging', '2014-10-07 00:00:00', '', 'scienceofaging.mp4', 'thumbnail-default.jpg', 9, '', 3, ''),
(103, '2 Important Habits of Successful people', '2014-10-07 00:00:00', '', '2importanthabitsofsuccess.mp4', '2habit.png', 8, '', 3, ''),
(104, 'Living The Dream', '2014-10-07 00:00:00', '', 'livingthedream.mp4', 'livingdream.jpg', 8, '', 3, ''),
(105, 'Starting Today', '0000-00-00 00:00:00', '', '0.png', '0.png', 8, '', 6, ''),
(106, '19 Things to Be Successful', '0000-00-00 00:00:00', '', '01.png', '01.jpg', 6, '', 6, ''),
(107, 'I''m Telling You', '0000-00-00 00:00:00', '', '1.jpg', 'thumbnail-default.jpg', 6, '', 6, ''),
(108, 'You can do anything', '0000-00-00 00:00:00', '', '2.jpg', 'thumbnail-default.jpg', 6, '', 6, ''),
(109, 'Life', '0000-00-00 00:00:00', '', '3.jpg', 'thumbnail-default.jpg', 0, '', 6, ''),
(110, 'If you don''t win today', '0000-00-00 00:00:00', '', '167207.jpg', 'thumbnail-default.jpg', 0, '', 6, ''),
(111, 'Don''t Cry', '0000-00-00 00:00:00', '', '168907.jpg', 'thumbnail-default.jpg', 0, '', 6, ''),
(112, 'Whatever You Can Do', '0000-00-00 00:00:00', '', '169942.jpg', 'thumbnail-default.jpg', 0, '', 6, ''),
(113, 'Don''t Procrastinate', '0000-00-00 00:00:00', '', '221109.jpg', 'thumbnail-default.jpg', 0, '', 6, ''),
(114, 'Just Do It', '0000-00-00 00:00:00', '', '230046.jpg', 'thumbnail-default.jpg', 0, '', 6, ''),
(115, 'I Am', '0000-00-00 00:00:00', '', '230080.jpg', 'thumbnail-default.jpg', 0, '', 6, ''),
(116, 'The Will To Win', '0000-00-00 00:00:00', '', '244589.jpg', 'thumbnail-default.jpg', 0, '', 6, ''),
(117, 'Remember The Time', '0000-00-00 00:00:00', '', '254272.jpg', 'thumbnail-default.jpg', 0, '', 6, ''),
(118, 'Where there''s Life', '0000-00-00 00:00:00', '', '255996.jpg', 'thumbnail-default.jpg', 0, '', 6, ''),
(119, 'Could Not Fail?', '0000-00-00 00:00:00', '', '263806.png', 'thumbnail-default.jpg', 0, '', 6, ''),
(120, 'Diabetes and Glycermic Index', '2014-10-09 21:28:38', '', 'Diabetes_and_Glycemic_Index.zip', 'thumbnail-default.jpg', 0, 'Health', 1, ''),
(121, 'Hearing Aids Inside Out', '2014-10-09 21:30:12', '', 'Hearing_Aids_Inside_Out.zip', 'thumbnail-default.jpg', 0, 'Health', 1, ''),
(122, 'Lower Your Cholesterol', '2014-10-09 21:30:44', '', 'Lower_Your_Cholesterol.zip', 'thumbnail-default.jpg', 0, 'Health', 1, ''),
(123, 'Quit Smoking Today', '2014-10-09 21:31:17', '', 'Quit_Smoking_Today.zip', 'thumbnail-default.jpg', 0, 'Health', 1, ''),
(124, 'The ADHD Success Formula', '2014-10-09 21:31:55', '', 'The_ADHD_Success_Formula.zip', 'thumbnail-default.jpg', 0, 'Health', 1, ''),
(125, 'Understanding and Treating ADHD', '2014-10-09 21:32:27', '', 'Understanding_and_Treating_ADHD.zip', 'thumbnail-default.jpg', 0, 'Health', 1, ''),
(126, 'Understanding and Treating Autism', '2014-10-09 21:32:56', '', 'Understanding_and_Treating_Autism.zip', 'thumbnail-default.jpg', 0, 'Health', 1, ''),
(127, 'Understanding and Treating Baby Colic', '2014-10-09 21:33:30', '', 'Understanding_and_Treating_Baby_Colic.zip', 'thumbnail-default.jpg', 0, 'Health', 1, ''),
(128, 'Understanding and Treating Bipolar Disorders', '2014-10-09 21:34:03', '', 'Understanding_and_Treating_Bipolar_Disorders.zip', 'thumbnail-default.jpg', 0, 'Health', 1, ''),
(129, 'Win Against Allergies', '2014-10-09 21:34:44', '', 'Win_Against_Allergies.zip', 'thumbnail-default.jpg', 0, 'Health', 1, ''),
(130, 'Win Against Back Pain', '2014-10-09 21:35:15', '', 'Win_Against_Back_Pain.zip', 'thumbnail-default.jpg', 0, 'Health', 1, ''),
(131, 'Win Against Bad Breath', '2014-10-09 21:35:43', '', 'Win_Against_Bad_Breath.zip', 'thumbnail-default.jpg', 0, 'Health', 1, ''),
(132, 'Win Against Bed Bugs', '2014-10-09 21:36:14', '', 'Win_Against_Bed_Bugs.zip', 'thumbnail-default.jpg', 0, 'Health', 1, ''),
(133, 'Win Against Bronchitis', '2014-10-09 21:36:40', '', 'Win_Against_Bronchitis.zip', 'thumbnail-default.jpg', 0, 'Health', 1, ''),
(134, 'Win Against Depression', '2014-10-09 21:37:12', '', 'Win_Against_Depression.zip', 'thumbnail-default.jpg', 0, 'Health', 1, ''),
(135, 'Win Against Insomnia and Sleep Disorders', '2014-10-09 21:37:46', '', 'Win_Against_Insomnia_and_Sleep_Disorders.zip', 'thumbnail-default.jpg', 0, 'Health', 1, ''),
(136, 'Win Against Irritable Bowel Syndrome', '2014-10-09 21:38:18', '', 'Win_Against_Irritable_Bowel_Syndrome.zip', 'thumbnail-default.jpg', 0, 'Health', 1, ''),
(137, 'Win Against Snoring and Sleep Apnea', '2014-10-09 21:38:52', '', 'Win_Against_Snoring_and_Sleep_Apnea.zip', 'thumbnail-default.jpg', 0, 'Health', 1, ''),
(138, 'Win Against Termites', '2014-10-09 21:39:18', '', 'Win_Against_Termites.zip', 'thumbnail-default.jpg', 0, 'Health', 1, ''),
(139, 'Your Body The Missing Manual The Book You Should Have Been Born With Ebook', '2014-10-09 21:39:47', '', 'Your Body The Missing Manual The Book You Should Have Been Born With Ebook.rar', 'thumbnail-default.jpg', 0, 'Health', 1, ''),
(140, 'Avoid Identity Theft', '2014-10-09 21:46:09', '', 'Avoid_Identity_Theft.zip', 'thumbnail-default.jpg', 0, 'Humanities', 1, ''),
(141, '8 Internet Marketing Scripts', '2014-10-09 21:46:59', '', '8_Internet_Marketing_Scripts.zip', 'thumbnail-default.jpg', 0, 'Internet Marketing', 1, ''),
(142, '47 Money Making Clickbank Websites', '2014-10-09 21:47:34', '', '47_Money_Making_Clickbank_Websites.zip', 'thumbnail-default.jpg', 0, 'Internet Marketing', 1, ''),
(143, '168 Internet Business Scripts', '2014-10-09 21:49:17', '', '168_Internet_Business_Scripts.zip', 'thumbnail-default.jpg', 0, 'Internet Marketing', 1, ''),
(144, '350 Sales and Marketing Tactics', '2014-10-09 21:51:49', '', '350_Sales_and_Marketing_Tactics.zip', 'thumbnail-default.jpg', 0, 'Internet Marketing', 1, ''),
(145, 'Awesome Link Effects', '2014-10-09 21:52:18', '', 'Awesome_Link_Effects.zip', 'thumbnail-default.jpg', 0, 'Internet Marketing', 1, ''),
(146, 'Classified Marketing Tactics', '2014-10-09 21:52:45', '', 'Classified_Marketing_Tactics.zip', 'thumbnail-default.jpg', 0, 'Internet Marketing', 1, ''),
(147, 'Google AdWords Exposed', '2014-10-09 21:53:13', '', 'Google_AdWords_Exposed.zip', 'thumbnail-default.jpg', 0, 'Internet Marketing', 1, ''),
(148, 'High End Affiliate Marketing Secrets', '2014-10-09 21:53:45', '', 'High_End_Affiliate_Marketing_Secrets.zip', 'thumbnail-default.jpg', 0, 'Internet Marketing', 1, ''),
(149, 'Home Business Models Exposed', '2014-10-09 21:54:13', '', 'Home_Business_Models_Exposed.zip', 'thumbnail-default.jpg', 0, 'Internet Marketing', 1, ''),
(150, 'How to Outsource Anything to Anyone', '2014-10-09 21:54:45', '', 'How_to_Outsource_Anything_to_Anyone.zip', 'thumbnail-default.jpg', 0, 'Internet Marketing', 1, ''),
(151, 'Instant Cash Strategies', '2014-10-09 21:55:37', '', 'Instant_Cash_Strategies.zip', 'thumbnail-default.jpg', 0, 'Internet Marketing', 1, ''),
(152, 'Kult Kingdom Tactics', '2014-10-09 21:56:08', '', 'Kult_Kingdom_Tactics.zip', 'thumbnail-default.jpg', 0, 'Internet Marketing', 1, ''),
(153, 'Online Stock Trading Made Easy', '2014-10-09 21:56:38', '', 'Online_Stock_Trading_Made_Easy.zip', 'thumbnail-default.jpg', 0, 'Internet Marketing', 1, ''),
(154, 'Six Minute Marketing', '2014-10-09 21:57:13', '', 'Six_Minute_Marketing.zip', 'thumbnail-default.jpg', 0, 'Internet Marketing', 1, ''),
(155, 'The Unstoppable Internet Entrepreneur Mindset', '2014-10-09 21:57:44', '', 'The_Unstoppable_Internet_Entrepreneur_Mindset.zip', 'thumbnail-default.jpg', 0, 'Internet Marketing', 1, ''),
(156, 'Traffic Jam', '2014-10-09 21:58:10', '', 'Traffic_Jam.zip', 'thumbnail-default.jpg', 0, 'Internet Marketing', 1, ''),
(157, 'Traffic Tidal Wave', '2014-10-09 21:58:38', '', 'Traffic_Tidal_Wave.zip', 'thumbnail-default.jpg', 0, 'Internet Marketing', 1, ''),
(158, 'Work at Home Mastermind', '2014-10-09 21:59:06', '', 'Work_at_Home_Mastermind.zip', 'thumbnail-default.jpg', 0, 'Internet Marketing', 1, ''),
(159, 'Become a Highly Effective Time Manager', '2014-10-09 21:59:43', '', 'Become_a_Highly_Effective_Time_Manager.zip', 'thumbnail-default.jpg', 0, 'Magazines', 1, ''),
(160, 'Date the Hottest Women', '2014-10-09 22:00:09', '', 'Date_the_Hottest_Women.zip', 'thumbnail-default.jpg', 0, 'Magazines', 1, ''),
(161, 'Wines and Spirits Inside Out', '2014-10-09 22:00:39', '', 'Wines_and_Spirits_Inside_Out.zip', 'thumbnail-default.jpg', 0, 'Magazines', 1, ''),
(162, 'Your Own Home Theater', '2014-10-09 22:01:08', '', 'Your_Own_Home_Theater.zip', 'thumbnail-default.jpg', 0, 'Magazines', 1, ''),
(163, 'Network Marketing Survival 3', '2014-10-09 22:01:36', '', 'Network_Marketing_Survival_3.zip', 'thumbnail-default.jpg', 0, 'Magazines', 1, ''),
(164, 'Adoption Made Easy', '2014-10-09 22:02:25', '', 'Adoption_Made_Easy.zip', 'thumbnail-default.jpg', 0, 'Parenting and Children', 1, ''),
(165, 'All About Baby Boomers', '2014-10-09 22:02:56', '', 'All_About_Baby_Boomers.zip', 'thumbnail-default.jpg', 0, 'Parenting and Children', 1, ''),
(166, 'Have a Successful Baby Shower', '2014-10-09 22:03:30', '', 'Have_a_Successful_Baby_Shower.zip', 'thumbnail-default.jpg', 0, 'Parenting and Children', 1, ''),
(167, 'How to Take Great Care of Elders', '2014-10-09 22:03:56', '', 'How_to_Take_Great_Care_of_Elders.zip', 'thumbnail-default.jpg', 0, 'Parenting and Children', 1, ''),
(168, 'How to Uncover your Genealogy', '2014-10-09 22:04:19', '', 'How_to_Uncover_your_Genealogy.zip', 'thumbnail-default.jpg', 0, 'Parenting and Children', 1, ''),
(169, 'Parental Control', '2014-10-09 22:04:47', '', 'Parental_Control.zip', 'thumbnail-default.jpg', 0, 'Parenting and Children', 1, ''),
(170, 'Single Parenting', '2014-10-09 22:05:03', '', 'Single_Parenting.zip', 'thumbnail-default.jpg', 0, 'Parenting and Children', 1, ''),
(171, 'Understanding Daycare', '2014-10-09 22:05:17', '', 'Understanding_Daycare.zip', 'thumbnail-default.jpg', 0, 'Parenting and Children', 1, ''),
(172, 'Understanding Pregnancy', '2014-10-09 22:05:31', '', 'Understanding_Pregnancy.zip', 'thumbnail-default.jpg', 0, 'Parenting and Children', 1, ''),
(173, '101 Power Affirmations', '2014-10-09 22:06:27', '', '101 Power Affirmations.pdf', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(174, '101 Power Qualities', '2014-10-09 22:06:27', '', '101 Power Qualities.pdf', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(175, '101 Power Questions', '2014-10-09 22:06:27', '', '101 Power Questions.pdf', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(176, '101 Power Values', '2014-10-09 22:06:27', '', '101 Power Values.pdf', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(177, '101 Power Words', '2014-10-09 22:06:27', '', '101 Power Words.pdf', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(178, 'Be the Creator of Your Own Life', '2014-10-09 22:06:27', '', 'Be_the_Creator_of_Your_Own_Life.zip', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(179, 'Concrete Confidence', '2014-10-09 22:06:27', '', 'Concrete_Confidence.zip', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(180, 'Develop Your Power of Concentration', '2014-10-09 22:06:27', '', 'Develop_Your_Power_of_Concentration.zip', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(181, 'Effectively Control Your Anger', '2014-10-09 22:06:27', '', 'Effectively_Control_Your_Anger.zip', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(182, 'Kick Bad Habits Out', '2014-10-09 22:06:27', '', 'Kick_Bad_Habits_Out.zip', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(183, 'Life Power - Main', '2014-10-09 22:06:27', '', 'Life Power - Main.pdf', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(184, 'Life Power Series 01 - You', '2014-10-09 22:06:27', '', 'Life Power Series 01 - You.pdf', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(185, 'Life Power Series 02 - Passion', '2014-10-09 22:06:27', '', 'Life Power Series 02 - Passion.pdf', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(186, 'Life Power Series 03 - Thought', '2014-10-09 22:06:27', '', 'Life Power Series 03 - Thought.pdf', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(187, 'Life Power Series 04 - Being', '2014-10-09 22:06:27', '', 'Life Power Series 04 - Being.pdf', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(188, 'Life Power Series 05 - Decision', '2014-10-09 22:06:27', '', 'Life Power Series 05 - Decision.pdf', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(189, 'Life Power Series 06 - Vision', '2014-10-09 22:06:27', '', 'Life Power Series 06 - Vision.pdf', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(190, 'Life Power Series 07 - Aim', '2014-10-09 22:06:27', '', 'Life Power Series 07 - Aim.pdf', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(191, 'Life Power Series 08 - Focus', '2014-10-09 22:06:27', '', 'Life Power Series 08 - Focus.pdf', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(192, 'Life Power Series 09 - Faith', '2014-10-09 22:06:27', '', 'Life Power Series 09 - Faith.pdf', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(193, 'Life Power Series 10 - Riches', '2014-10-09 22:06:27', '', 'Life Power Series 10 - Riches.pdf', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(194, 'Overcome Procastination', '2014-10-09 22:06:27', '', 'Overcome_Procastination.zip', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(195, 'Passion Driven Prosperity', '2014-10-09 22:06:27', '', 'Passion_Driven_Prosperity.zip', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(196, 'Positive Attitude for Unlimited Success', '2014-10-09 22:06:27', '', 'Positive_Attitude_for_Unlimited_Success.zip', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(197, 'Self Improvement Made Easy', '2014-10-09 22:06:27', '', 'Self_Improvement_Made_Easy.zip', 'thumbnail-default.jpg', 0, 'Personal Development', 1, ''),
(198, 'Energy Conservation and Alternative Fuel', '2014-10-09 22:06:27', '', 'Energy_Conservation_and_Alternative_Fuel.zip', 'thumbnail-default.jpg', 0, 'Science', 1, ''),
(199, 'The Truth Behind The Lies', '2014-10-09 22:06:27', '', 'The_Truth_Behind_The_Lies.zip', 'thumbnail-default.jpg', 0, 'Short Stories', 1, ''),
(200, 'Unforgettable Cruise Vacation', '2014-10-09 22:06:27', '', 'Unforgettable_Cruise_Vacation.zip', 'thumbnail-default.jpg', 0, 'Short Stories', 1, ''),
(201, 'Basics of Film-Making', '2014-10-09 22:06:27', '', 'Basics_of_Film-Making.zip', 'thumbnail-default.jpg', 0, 'Technology', 1, ''),
(202, 'GPS Made Easy', '2014-10-09 22:06:27', '', 'GPS_Made_Easy.zip', 'thumbnail-default.jpg', 0, 'Technology', 1, ''),
(203, 'Home Security Made Easy', '2014-10-09 22:06:27', '', 'Home_Security_Made_Easy.zip', 'thumbnail-default.jpg', 0, 'Technology', 1, ''),
(204, 'Relocation Made Easy', '2014-10-09 22:06:27', '', 'Relocation_Made_Easy.zip', 'thumbnail-default.jpg', 0, 'Technology', 1, ''),
(205, 'Remote Control Cars', '2014-10-09 22:06:27', '', 'Remote_Control_Cars.zip', 'thumbnail-default.jpg', 0, 'Technology', 1, ''),
(206, 'Have a Perfect Boating Experience', '2014-10-09 22:06:27', '', 'Have_a_Perfect_Boating_Experience.zip', 'thumbnail-default.jpg', 0, 'Travel', 1, ''),
(207, 'Aquarium Care Made Easy', '2014-10-09 22:06:27', '', 'Aquarium_Care_Made_Easy.zip', 'thumbnail-default.jpg', 0, 'Tutorials', 1, ''),
(208, 'ATV Made Easy', '2014-10-09 22:06:27', '', 'ATV_Made_Easy.zip', 'thumbnail-default.jpg', 0, 'Tutorials', 1, ''),
(209, 'Auto Sound Systems Made Easy', '2014-10-09 22:06:27', '', 'Auto_Sound_Systems_Made_Easy.zip', 'thumbnail-default.jpg', 0, 'Tutorials', 1, ''),
(210, 'Basics of Quilting', '2014-10-09 22:06:27', '', 'Basics_of_Quilting.zip', 'thumbnail-default.jpg', 0, 'Tutorials', 1, ''),
(211, 'Become a Bee Keeping Pro', '2014-10-09 22:06:27', '', 'Become_a_Bee_Keeping_Pro.zip', 'thumbnail-default.jpg', 0, 'Tutorials', 1, ''),
(212, 'Become a Fly Fishing Pro', '2014-10-09 22:06:27', '', 'Become_a_Fly_Fishing_Pro.zip', 'thumbnail-default.jpg', 0, 'Tutorials', 1, ''),
(213, 'Become a Gambling Pro', '2014-10-09 22:06:27', '', 'Become_a_Gambling_Pro.zip', 'thumbnail-default.jpg', 0, 'Tutorials', 1, ''),
(214, 'Become an Expert at Sudoku', '2014-10-09 22:06:27', '', 'Become_an_Expert_at_Sudoku.zip', 'thumbnail-default.jpg', 0, 'Tutorials', 1, ''),
(215, 'Candle Making Made Easy', '2014-10-09 22:06:27', '', 'Candle_Making_Made_Easy.zip', 'thumbnail-default.jpg', 0, 'Tutorials', 1, ''),
(216, 'Create Great Gift Baskets', '2014-10-09 22:06:27', '', 'Create_Great_Gift_Baskets.zip', 'thumbnail-default.jpg', 0, 'Tutorials', 1, ''),
(217, 'Decorate Your House Like a Pro', '2014-10-09 22:06:27', '', 'Decorate_Your_House_Like_a_Pro.zip', 'thumbnail-default.jpg', 0, 'Tutorials', 1, ''),
(218, 'Fishing Mastery', '2014-10-09 22:06:27', '', 'Fishing_Mastery.zip', 'thumbnail-default.jpg', 0, 'Tutorials', 1, ''),
(219, 'Fun with Telescopes', '2014-10-09 22:06:27', '', 'Fun_with_Telescopes.zip', 'thumbnail-default.jpg', 0, 'Tutorials', 1, ''),
(220, 'Gardening Made Easy', '2014-10-09 22:06:27', '', 'Gardening_Made_Easy.zip', 'thumbnail-default.jpg', 0, 'Tutorials', 1, ''),
(221, 'Gardening Secrets for a Lush Garden', '2014-10-09 22:06:27', '', 'Gardening_Secrets_for_a_Lush_Garden.zip', 'thumbnail-default.jpg', 0, 'Tutorials', 1, ''),
(222, 'Greenhouse Maintenance', '2014-10-09 22:06:27', '', 'Greenhouse_Maintenance.zip', 'thumbnail-default.jpg', 0, 'Tutorials', 1, ''),
(223, 'How to Become a Bass Fishing Pro', '2014-10-09 22:06:27', '', 'How_to_Become_a_Bass_Fishing_Pro.zip', 'thumbnail-default.jpg', 0, 'Tutorials', 1, ''),
(224, 'Make Your Own Spooky Halloween Crafts', '2014-10-09 22:06:27', '', 'Make_Your_Own_Spooky_Halloween_Crafts.zip', 'thumbnail-default.jpg', 0, 'Tutorials', 1, ''),
(225, 'Scrapbooking Made Easy', '2014-10-09 22:06:27', '', 'Scrapbooking_Made_Easy.zip', 'thumbnail-default.jpg', 0, 'Tutorials', 1, ''),
(226, 'The Right Food for Your Dog', '2014-10-09 22:06:27', '', 'The_Right_Food_for_Your_Dog.zip', 'thumbnail-default.jpg', 0, 'Tutorials', 1, ''),
(227, 'Train Yourself for Soccer', '2014-10-09 22:06:27', '', 'Train_Yourself_for_Soccer.zip', 'thumbnail-default.jpg', 0, 'Tutorials', 1, ''),
(228, 'Your Garden', '2014-10-09 22:06:27', '', 'Your_Garden.zip', 'thumbnail-default.jpg', 0, 'Tutorials', 1, ''),
(229, 'Meaning of Life', '2014-10-10 00:40:11', '', 'Meaning Of Life Inlight.mp4', 'thumbnail-default.jpg', 8, '', 5, '');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `user_ID` int(10) unsigned zerofill NOT NULL,
  `message` text NOT NULL,
  `date_time` datetime NOT NULL,
  `from_ID` int(10) unsigned zerofill NOT NULL COMMENT 'fetched from "user" table',
  `type` int(1) NOT NULL COMMENT '0 : trash | 1 : inbox',
  `isRead` int(1) DEFAULT '0' COMMENT '1: true | 2 : false',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`ID`, `user_ID`, `message`, `date_time`, `from_ID`, `type`, `isRead`) VALUES
(1, 0000000002, 'Test', '2014-09-25 00:00:00', 0000000002, 1, 0),
(2, 0000000002, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type a', '0000-00-00 00:00:00', 0000000002, 1, 0),
(3, 0000000002, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distrib', '2014-09-25 05:25:53', 0000000002, 1, 0),
(4, 0000000001, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vel diam consequat, laoreet dolor non, vestibulum tortor. Duis neque purus, posuere et luctus id, imperdiet non tortor. Praesent vitae', '2014-09-27 04:13:21', 0000000001, 1, 1),
(5, 0000000002, 'HOY JIMMY :AG LUNGAG', '2014-09-27 04:14:46', 0000000001, 1, 1),
(6, 0000000287, 'CONGRATS!', '2014-10-02 15:20:22', 0000000001, 1, 1),
(7, 0000000001, 'Check', '2014-10-08 01:41:46', 0000000001, 1, 1),
(8, 0000000287, 'Testing\r\n', '2014-10-08 01:45:32', 0000000001, 1, 1),
(9, 0000000287, 'Welcome Inlight Pioneers! This is a new beginning and a new opportunity! I hope you do your best in here. Let go of the pas and start again here in Inlight. You are the foundation of this company. The', '2014-10-08 02:19:44', 0000000001, 1, 1),
(10, 0000000287, 'Important: Welcome Inlight Pioneers! ', '2014-10-08 02:22:56', 0000000001, 1, 1),
(11, 0000000287, 'Important: Welcome Inlight Pioneers! Please be reminded that your account is Free Slot (FS) that means all affiliates that exist on your geneology as of this moment are all FS and its has 0 points.  Y', '2014-10-08 02:35:06', 0000000001, 1, 1),
(12, 0000000287, 'Important: Welcome Pioneers! Please be reminded that all accounts under you, including yours are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank You!\r\n', '2014-10-08 02:47:27', 0000000001, 1, 0),
(13, 0000000293, 'Important: Welcome Pioneers! Please be reminded that all accounts under you, including yours are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank You!\r\n', '2014-10-08 02:48:05', 0000000001, 1, 0),
(14, 0000000355, 'Important: Welcome Pioneers! Please be reminded that all accounts under you, including yours are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank You!\r\n', '2014-10-08 02:48:33', 0000000001, 1, 0),
(15, 0000000361, 'Important: Welcome Pioneers! Please be reminded that all accounts under you, including yours are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank You!\r\n', '2014-10-08 02:49:08', 0000000001, 1, 0),
(16, 0000000370, 'Important: Welcome Pioneers! Please be reminded that all accounts under you, including yours are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank You!\r\n', '2014-10-08 02:49:22', 0000000001, 1, 0),
(17, 0000000358, 'Important: Welcome Pioneers! Please be reminded that all accounts under you, including yours are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank You!\r\n', '2014-10-08 02:49:45', 0000000001, 1, 1),
(18, 0000000364, 'Important: Welcome Pioneers! Please be reminded that all accounts under you, including yours are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank You!\r\n', '2014-10-08 02:49:57', 0000000001, 1, 1),
(19, 0000000367, 'Important: Welcome Pioneers! Please be reminded that all accounts under you, including yours are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank You!\r\n', '2014-10-08 02:50:14', 0000000001, 1, 0),
(20, 0000000297, 'Important: Welcome Pioneers! Please be reminded that all accounts under you, including yours are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank You!\r\n', '2014-10-08 02:50:49', 0000000001, 1, 0),
(21, 0000000301, 'Important: Welcome Pioneers! Please be reminded that all accounts under you, including yours are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank You!\r\n', '2014-10-08 02:50:58', 0000000001, 1, 1),
(22, 0000000314, 'Important: Welcome Pioneers! Please be reminded that all accounts under you, including yours are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank You!\r\n', '2014-10-08 02:51:12', 0000000001, 1, 0),
(23, 0000000344, 'Important: Welcome Pioneers! Please be reminded that all accounts under you, including yours are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank You!\r\n', '2014-10-08 02:51:26', 0000000001, 1, 1),
(24, 0000000316, 'Important: Welcome Pioneers! Please be reminded that all accounts under you, including yours are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank You!\r\n', '2014-10-08 02:51:40', 0000000001, 1, 0),
(25, 0000000324, 'Important: Welcome Pioneers! Please be reminded that all accounts under you, including yours are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank You!\r\n', '2014-10-08 02:51:54', 0000000001, 1, 1),
(26, 0000000328, 'Important: Welcome Pioneers! Please be reminded that all accounts under you, including yours are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank You!\r\n', '2014-10-08 02:52:07', 0000000001, 1, 1),
(27, 0000000331, 'Important: Welcome Pioneers! Please be reminded that all accounts under you, including yours are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank You!\r\n', '2014-10-08 02:52:20', 0000000001, 1, 1),
(28, 0000000334, 'Important: Welcome Pioneers! Please be reminded that all accounts under you, including yours are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank You!\r\n', '2014-10-08 02:52:36', 0000000001, 1, 1),
(29, 0000000371, 'Important: Welcome Pioneers! Please be reminded that all accounts under you, including yours are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank You!\r\n', '2014-10-08 02:52:52', 0000000001, 1, 0),
(30, 0000000376, 'Important: Welcome Pioneers! Please be reminded that all accounts under you, including yours are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank You!\r\n', '2014-10-08 02:53:02', 0000000001, 1, 0),
(31, 0000000379, 'Important: Welcome Pioneers! Please be reminded that all accounts under you, including yours are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank You!\r\n', '2014-10-08 02:53:13', 0000000001, 1, 0),
(32, 0000000306, 'Important: Welcome Pioneers! Please be reminded that all accounts under you, including yours are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank You!\r\n', '2014-10-08 02:54:01', 0000000001, 1, 0),
(33, 0000000343, 'Important: Welcome Pioneers! Please be reminded that all accounts under you, including yours are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank You!\r\n', '2014-10-08 02:54:15', 0000000001, 1, 1),
(34, 0000000309, 'Important: Welcome Pioneers! Please be reminded that all accounts under you, including yours are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank You!\r\n', '2014-10-08 02:54:22', 0000000001, 1, 0),
(35, 0000000313, 'Important: Welcome Pioneers! Please be reminded that all accounts under you, including yours are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank You!\r\n', '2014-10-08 02:54:35', 0000000001, 1, 0),
(36, 0000000381, 'Important: Welcome Pioneers! Please be reminded that all accounts under you including your accounts are Free Slot(FS) which means 0 points. Your earnings starts on your first PAID affiliates. Thank yo', '2014-10-08 03:23:31', 0000000001, 1, 0),
(37, 0000000302, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque sapien vitae eros accumsan, ut tempor augue fermentum. Duis pharetra eu risus sit amet blandit. Sed eu aliquam nisl, quis congue purus. Nulla finibus erat augue, ac scelerisque dolor dictum quis. Duis id purus vestibulum, rutrum ligula sit amet, ultrices augue. Integer bibendum elit vitae lorem rhoncus, id interdum libero luctus. Cras a nisl viverra, bibendum quam vel, elementum orci. Quisque congue dui eu nibh sagittis ultricies. Praesent vel est at erat porta mattis. Ut sit amet condimentum orci.\r\n\r\nProin massa lacus, malesuada id ultrices sit amet, malesuada et ante. Praesent bibendum lacus vitae velit accumsan euismod. Etiam non lectus convallis, tempor eros vitae, rutrum dolor. Donec tellus arcu, euismod nec sagittis non, mollis sit amet nisi. Suspendisse vel aliquet justo. Nulla finibus commodo justo, at viverra sem pulvinar ut. Aliquam finibus commodo odio, eu lacinia orci interdum quis. Maecenas eros risus, mattis faucibus nunc id, bibendum vehicula augue. Sed eros dolor, elementum et consectetur at, interdum in mauris. Maecenas gravida cursus convallis. Donec lobortis turpis aliquet urna condimentum, id iaculis ipsum egestas. In hac habitasse platea dictumst.\r\n\r\nVivamus in turpis ut enim efficitur consequat. Etiam eget dui odio. Praesent leo metus, dictum id felis eu, consequat venenatis dui. Maecenas eu lorem sed dolor vehicula ullamcorper. Suspendisse aliquam diam vitae facilisis dictum. Nam convallis feugiat tortor, eget faucibus erat venenatis pellentesque. Phasellus sit amet magna vel sem mattis gravida. Nam elementum eu nibh at luctus.\r\n\r\nAliquam sit amet rhoncus velit. Vestibulum dignissim erat purus, sit amet viverra diam porta vel. Proin eu orci id purus blandit iaculis. Quisque faucibus, purus ut suscipit venenatis, risus magna pretium ante, sit amet congue augue sem nec massa. Nunc interdum venenatis consectetur. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce convallis leo eget tellus lacinia lacinia. Curabitur auctor, nisl in egestas rutrum, lectus enim placerat lorem, sit amet pharetra enim urna a urna. Donec eu venenatis turpis. Aliquam luctus tellus at sagittis lacinia. Quisque commodo accumsan magna sit amet tempor. Curabitur congue sapien sit amet velit finibus sagittis. Praesent mauris mi, tempor at erat in, bibendum porta dui. Suspendisse egestas arcu tortor, mattis varius turpis imperdiet ut. Etiam tincidunt dapibus volutpat.\r\n\r\nNullam facilisis auctor eros quis commodo. Etiam sagittis justo in pharetra interdum. Mauris ullamcorper molestie orci sed blandit. Maecenas tempus leo ut faucibus molestie. Cras eu quam efficitur, vehicula sapien et, pellentesque ligula. Curabitur nisi sapien, elementum ut felis vel, viverra commodo sapien. Nunc pellentesque aliquet vehicula. Vestibulum congue justo eu odio semper, eget pulvinar erat malesuada. Suspendisse potenti.', '2014-10-08 06:05:05', 0000000301, 1, 0),
(38, 0000000334, 'boss..', '2014-10-09 03:31:15', 0000000331, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `note`
--

DROP TABLE IF EXISTS `note`;
CREATE TABLE IF NOT EXISTS `note` (
  `ID` int(1) NOT NULL,
  `note` text NOT NULL,
  UNIQUE KEY `ID_2` (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `note`
--

INSERT INTO `note` (`ID`, `note`) VALUES
(1, '<p>Test&nbsp;</p>');

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

DROP TABLE IF EXISTS `options`;
CREATE TABLE IF NOT EXISTS `options` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `meta_name` varchar(100) NOT NULL,
  `meta_value` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='options holder' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `support_ticket`
--

DROP TABLE IF EXISTS `support_ticket`;
CREATE TABLE IF NOT EXISTS `support_ticket` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `user_ID` int(10) NOT NULL,
  `message` varchar(500) NOT NULL,
  `date` datetime NOT NULL,
  `status` int(1) NOT NULL COMMENT '0:pending | 1:active | 2:solved',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
CREATE TABLE IF NOT EXISTS `transaction` (
  `ID` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `type` int(5) NOT NULL COMMENT '1:membership | 2:upgrade | 3:widthraw | 4:purchase code',
  `reg_how_many` int(11) DEFAULT NULL COMMENT 'only applicable for basic and premium. options are 1, 3 and 7 only',
  `date_transaction` datetime NOT NULL,
  `remarks` text NOT NULL,
  `reference` text NOT NULL,
  `gross` float NOT NULL,
  `date_sent` datetime NOT NULL COMMENT 'date from the form',
  `status` int(1) NOT NULL COMMENT '0 = pending | 1 = completed | 2 = declined',
  `payment_option` varchar(100) NOT NULL,
  `bank_branch` varchar(200) NOT NULL,
  `acc_no` int(20) NOT NULL,
  `acc_name` int(40) NOT NULL,
  `wire_name` varchar(200) NOT NULL COMMENT 'name of the person when using wire transfers',
  `wire_contact` varchar(200) NOT NULL,
  `wire_address` varchar(300) NOT NULL,
  `user_ID` int(10) NOT NULL,
  `receipt_picture` varchar(200) NOT NULL,
  `sponsor_ID` int(10) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`ID`, `type`, `reg_how_many`, `date_transaction`, `remarks`, `reference`, `gross`, `date_sent`, `status`, `payment_option`, `bank_branch`, `acc_no`, `acc_name`, `wire_name`, `wire_contact`, `wire_address`, `user_ID`, `receipt_picture`, `sponsor_ID`) VALUES
(00020, 1, 3, '2014-10-02 15:08:27', '', '123123123123', 6000, '2014-10-03 02:17:00', 1, 'BPI Bank', 'admin', 0, 0, '', '', ', ', 287, 'reg-000000028720141002150827.png', 2),
(00021, 1, 3, '2014-10-02 16:13:32', '', '', 0, '0000-00-00 00:00:00', 1, 'BPI Bank', '', 0, 0, '', '', ', ', 290, 'reg-29020141002161332.jpg', 288),
(00022, 1, 3, '2014-10-02 16:23:30', '', '', 0, '0000-00-00 00:00:00', 1, 'BPI Bank', '', 0, 0, '', '', ', ', 293, 'reg-29320141002162330.jpg', 287),
(00023, 1, 3, '2014-10-02 16:28:41', '', '', 0, '0000-00-00 00:00:00', 1, 'BPI Bank', '', 0, 0, '', '', ', ', 297, 'reg-29720141002162841.jpg', 295),
(00024, 1, 1, '2014-10-02 16:29:51', '', '', 0, '0000-00-00 00:00:00', 1, 'BPI Bank', '', 0, 0, '', '', ', ', 294, 'reg-29420141002162951.jpg', 290),
(00025, 1, 3, '2014-10-02 16:32:21', '', '', 0, '0000-00-00 00:00:00', 1, 'BPI Bank', '', 0, 0, '', '', ', ', 301, 'reg-30120141002163221.jpg', 298),
(00026, 1, 3, '2014-10-02 16:34:16', '', '', 6000, '0000-00-00 00:00:00', 1, 'BPI Bank', '', 0, 0, '', '', ', ', 300, 'reg-30020141002163417.jpg', 289),
(00027, 1, 3, '2014-10-02 16:42:04', '', '', 0, '0000-00-00 00:00:00', 1, 'BPI Bank', '', 0, 0, '', '', ', ', 306, 'reg-30620141002164204.png', 302),
(00028, 1, 3, '2014-10-02 17:20:32', '', '123123789123', 6000, '2014-10-03 16:07:00', 1, 'BPI Bank', '', 0, 0, '', '', ', ', 309, 'reg-30920141002172032.jpg', 307),
(00029, 1, 1, '2014-10-02 17:23:23', '', '', 0, '0000-00-00 00:00:00', 1, 'BPI Bank', '', 0, 0, '', '', ', ', 313, 'reg-31320141002172323.jpg', 310),
(00030, 1, 3, '2014-10-02 17:25:18', '', '', 6000, '0000-00-00 00:00:00', 1, 'BPI Bank', '', 0, 0, '', '', ', ', 312, 'reg-31220141002172518.jpg', 300),
(00031, 1, 1, '2014-10-02 17:27:32', '', '', 0, '0000-00-00 00:00:00', 1, 'BPI Bank', '', 0, 0, '', '', ', ', 314, 'reg-31420141002172732.jpg', 303),
(00032, 1, 3, '2014-10-02 17:31:54', '', '', 0, '0000-00-00 00:00:00', 1, 'BPI Bank', '', 0, 0, '', '', ', ', 316, 'reg-31620141002173154.jpg', 314),
(00033, 1, 1, '2014-10-02 17:33:23', '', '', 0, '0000-00-00 00:00:00', 1, 'BPI Bank', '', 0, 0, '', '', ', ', 315, 'reg-31520141002173323.jpg', 300),
(00034, 1, 3, '2014-10-02 17:35:33', '', '', 0, '0000-00-00 00:00:00', 1, 'BPI Bank', '', 0, 0, '', '', ', ', 321, 'reg-32120141002173533.jpg', 289),
(00035, 1, 3, '2014-10-02 17:42:21', '', '', 0, '0000-00-00 00:00:00', 1, 'BPI Bank', '', 0, 0, '', '', ', ', 324, 'reg-32420141002174221.jpg', 319),
(00037, 1, 3, '2014-10-02 17:51:05', '', '', 0, '0000-00-00 00:00:00', 1, 'BPI Bank', '', 0, 0, '', '', ', ', 328, 'reg-32820141002175105.jpg', 325),
(00038, 1, 3, '2014-10-02 18:00:02', '', '', 0, '0000-00-00 00:00:00', 1, 'BPI Bank', '', 0, 0, '', '', ', ', 331, 'reg-33120141002180002.jpg', 329),
(00039, 1, 3, '2014-10-02 18:02:11', '', '', 0, '0000-00-00 00:00:00', 1, 'BPI Bank', '', 0, 0, '', '', ', ', 334, 'reg-33420141002180211.jpg', 332),
(00041, 1, 1, '2014-10-02 18:19:23', '', '', 0, '0000-00-00 00:00:00', 1, 'BPI Bank', '', 0, 0, '', '', ', ', 338, 'reg-33820141002181923.jpg', 304),
(00042, 1, 1, '2014-10-06 04:03:29', '', '3322', 222, '2014-10-07 13:03:00', 0, 'LBC', '', 0, 0, '', '', ', ', 339, 'reg-000000033920141006040329.png', 0),
(00043, 1, 1, '2014-10-06 06:29:53', '', '123456789', 2000, '2014-10-06 13:01:00', 0, 'Cebuana', 'Davao', 0, 0, 'Jimmy Giger', '', 'B9 L2 Davao City, Philippines', 340, 'reg-34020141006062953.png', 0),
(00044, 1, 1, '2014-10-06 09:06:53', '', '1232323232', 2000, '2014-10-06 02:08:00', 0, 'Palawan Exp', 'Davao', 0, 0, 'Your Name', '', 'Davao, Philippines', 342, 'reg-34220141006090653.png', 287),
(00045, 1, 3, '2014-10-06 10:00:40', '', '123123', 1, '2014-10-06 15:18:00', 1, 'BPI Bank', 'Bajada', 0, 0, '', '', ', ', 343, 'reg-34320141006100040.jpg', 308),
(00046, 1, 1, '2014-10-06 10:08:26', '', '123', 1, '2014-10-08 13:02:00', 1, 'LBC', '', 0, 0, 'Francis', '', 'Davao, Philippines', 344, 'reg-34420141006100826.jpg', 303),
(00047, 1, 1, '2014-10-06 10:24:50', '', '123123', 123, '2014-10-07 02:02:00', 1, 'LBC', '', 0, 0, 'test', '', 'test, test', 350, 'reg-35020141006102450.jpg', 301),
(00048, 1, 1, '2014-10-07 12:06:00', '', '1232323232', 2000, '2014-10-08 01:01:00', 1, 'BPI Bank', 'Davao', 0, 0, '', '', ', ', 352, 'reg-35220141007120600.png', 322),
(00049, 1, 1, '2014-10-07 12:20:24', '', '1232323232', 2000, '2014-10-08 01:02:00', 1, 'BPI Bank', 'Davao', 0, 0, '', '', ', ', 353, 'reg-35320141007122024.png', 322),
(00050, 1, 1, '2014-10-07 12:34:16', '', '1232323232', 2000, '2014-10-08 01:01:00', 1, 'BPI Bank', 'Davao', 0, 0, '', '', ', ', 354, 'reg-35420141007123416.png', 323),
(00051, 1, 3, '2014-10-07 14:13:41', '', '1232323232', 2000, '2014-10-08 01:01:00', 1, 'BPI Bank', 'Davao', 0, 0, '', '', ', ', 355, 'reg-35520141007141341.png', 323),
(00052, 1, 3, '2014-10-07 14:26:44', '', '1232323232', 2000, '2014-10-08 01:01:00', 1, 'BPI Bank', 'Davao', 0, 0, '', '', ', ', 358, 'reg-35820141007142644.png', 356),
(00053, 1, 3, '2014-10-07 14:37:57', '', '1232323232', 2000, '2014-10-08 01:06:00', 1, 'BPI Bank', 'Davao', 0, 0, '', '', ', ', 361, 'reg-36120141007143757.png', 357),
(00054, 1, 3, '2014-10-07 14:43:51', '', '1232323232', 2000, '2014-10-08 01:01:00', 1, 'BPI Bank', 'Davao', 0, 0, '', '', ', ', 364, 'reg-36420141007144351.png', 360),
(00055, 1, 3, '2014-10-07 14:50:27', '', '1232323232', 2000, '2014-10-08 01:14:00', 1, 'BPI Bank', 'Davao', 0, 0, '', '', ', ', 367, 'reg-36720141007145027.png', 366),
(00056, 1, 1, '2014-10-07 14:56:25', '', '1232323232', 2000, '2014-10-08 01:01:00', 1, 'BPI Bank', 'Davao', 0, 0, '', '', ', ', 370, 'reg-37020141007145625.png', 362),
(00057, 1, 3, '2014-10-07 15:20:17', '', '123', 2000, '2014-10-08 01:01:00', 1, 'BPI Bank', 'Davao', 0, 0, '', '', ', ', 371, 'reg-37120141007152017.png', 335),
(00058, 1, 3, '2014-10-07 16:48:49', '', '1232323232', 2000, '2014-10-08 01:01:00', 1, 'BPI Bank', 'Davao', 0, 0, '', '', ', ', 376, 'reg-37620141007164849.jpg', 372),
(00059, 1, 1, '2014-10-07 17:05:42', '', '1232323232', 2000, '2014-10-08 01:01:00', 1, 'BPI Bank', 'Davao', 0, 0, '', '', ', ', 379, 'reg-37920141007170542.jpg', 377),
(00060, 1, 1, '2014-10-08 03:20:58', '', '1232323232', 2000, '2014-10-08 01:01:00', 1, 'BPI Bank', 'Davao', 0, 0, '', '', ', ', 381, 'reg-38120141008032058.jpg', 369),
(00061, 1, 1, '2014-10-08 03:32:40', '', '1232323232', 2000, '2014-10-08 01:01:00', 1, 'BPI Bank', 'Davao', 0, 0, '', '', ', ', 382, 'reg-38220141008033240.jpg', 379),
(00062, 1, 1, '2014-10-08 03:36:13', '', '1232323232', 2000, '2014-10-08 01:07:00', 1, 'BPI Bank', 'Davao', 0, 0, '', '', ', ', 383, 'reg-38320141008033613.jpg', 382);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `ID` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `lname` varchar(20) NOT NULL,
  `mname` varchar(20) NOT NULL,
  `fname` varchar(20) NOT NULL,
  `status` int(1) NOT NULL COMMENT '0:pending | 1:active',
  `upline_id` int(10) unsigned zerofill NOT NULL COMMENT 'ID of up-line ("user" table)',
  `uposition` int(1) NOT NULL COMMENT 'Upline''s Position. options : 1:left | 2:right',
  `account_type` int(5) NOT NULL COMMENT '1 : Basic | 2:Premier | 3:Silver | 4:Gold | 5:Diamond || company Side || 6: 01 | 7:admin',
  `sposition` int(1) NOT NULL COMMENT 'Sponsore''s Position . options. 1 : left | 2: right',
  `sponsor_id` int(10) unsigned zerofill NOT NULL COMMENT 'ID of direct referrer/sponsor ("user" table)',
  `username` varchar(50) NOT NULL,
  `avatar_src` varchar(200) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `pin` int(6) NOT NULL,
  `registered_date` datetime NOT NULL,
  `upgrade_date` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `bdate` varchar(10) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `address` varchar(200) NOT NULL,
  `city` varchar(50) NOT NULL,
  `province` varchar(50) NOT NULL,
  `island_group` varchar(50) NOT NULL,
  `atm_number` varchar(20) NOT NULL,
  `weekly_balance` varchar(10) NOT NULL DEFAULT '0',
  `monthly_balance` varchar(10) NOT NULL DEFAULT '0',
  `current_balance` varchar(10) NOT NULL DEFAULT '0',
  `total_balance` varchar(10) NOT NULL DEFAULT '0',
  `profit_share` varchar(10) NOT NULL DEFAULT '0',
  `personal_status` varchar(50) NOT NULL,
  `isNotFirstTimer` int(1) NOT NULL DEFAULT '0' COMMENT 'Will be set to one on their first login as active account.. 1:true | 0:false',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=393 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `lname`, `mname`, `fname`, `status`, `upline_id`, `uposition`, `account_type`, `sposition`, `sponsor_id`, `username`, `avatar_src`, `email`, `password`, `pin`, `registered_date`, `upgrade_date`, `last_login`, `bdate`, `gender`, `phone`, `mobile`, `address`, `city`, `province`, `island_group`, `atm_number`, `weekly_balance`, `monthly_balance`, `current_balance`, `total_balance`, `profit_share`, `personal_status`, `isNotFirstTimer`) VALUES
(0000000001, 'Giger', 'Ruizo', 'Kurt Roy', 1, 0000000000, 1, 5, 1, 0000000000, 'krgiger', 'primary-0000000001.jpg', '', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-09-27 23:42:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '04/28/1988', 'Male', '', '', 'Davao City', 'Davao city', '', '', '', '0', '100', '100', '200', '3', 'Single', 1),
(0000000002, 'Giger', '(President 2)', 'Kurt Roy', 1, 0000000001, 1, 4, 1, 0000000001, 'krgiger2', 'primary-0000000002.jpg', 'mrthemetribe@gmail.com', '11ed57e580f9403534ed003c2d8cca6b', 5677, '2014-09-27 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '13/10/2011', 'Male', '123123123123', '', 'Maa Davao', 'Digos', 'Digos Prov', '', '', '0', '100', '100', '100', '3', 'Single', 1),
(0000000003, 'Giger', '(President 3)', 'Kur Roy', 1, 0000000001, 2, 4, 2, 0000000001, 'krgiger3', '', '', '11ed57e580f9403534ed003c2d8cca6b', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(0000000287, 'Giger Jr.', '', 'Jimmy', 1, 0000000002, 1, 2, 1, 0000000002, 'jrgiger', '', '', 'c47b3804e723fc7b9f7c50fdcd453b4d', 0, '2014-10-02 14:17:14', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'male', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 1),
(0000000288, 'Giger Jr.', '', 'Jimmy', 1, 0000000287, 1, 2, 1, 0000000287, 'jrgiger2', '', '', 'c47b3804e723fc7b9f7c50fdcd453b4d', 0, '2014-10-02 15:11:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'male', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000289, 'Giger Jr.', '', 'Jimmy', 1, 0000000287, 2, 2, 1, 0000000287, 'jrgiger3', '', '', 'c47b3804e723fc7b9f7c50fdcd453b4d', 0, '2014-10-02 15:11:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'male', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000290, 'Ruizo', '', 'Kerr', 1, 0000000288, 2, 2, 2, 0000000288, 'kerrruizo', '', '', '45c1b885ee4ce5cb20acd56cccd36b8f', 0, '2014-10-02 16:12:28', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'male', '', '', '', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000291, 'Ruizo', '', 'Kerr', 1, 0000000290, 1, 2, 1, 0000000290, 'kerrruizo2', '', '', '45c1b885ee4ce5cb20acd56cccd36b8f', 0, '2014-10-02 16:14:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'male', '', '', '', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000292, 'Ruizo', '', 'Kerr', 1, 0000000290, 2, 2, 2, 0000000290, 'kerrruizo3', '', '', '45c1b885ee4ce5cb20acd56cccd36b8f', 0, '2014-10-02 16:14:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'male', '', '', '', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000293, 'Giger', '', 'Kurl Keith', 1, 0000000288, 1, 2, 1, 0000000288, 'kurlgiger', '', '', 'f14219576710df5e8f9e3ffb3c2c548c', 0, '2014-10-02 16:22:56', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'male', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000294, 'Ruizo', '', 'Evelyn', 1, 0000000291, 1, 2, 1, 0000000290, 'EvelynRuizo', '', '', '607013e8bb6144c19ce3b6937a9786b5', 0, '2014-10-02 16:24:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'male', '', '', '', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000295, 'Giger', '', 'Kurl Keith', 1, 0000000293, 1, 2, 1, 0000000293, 'kurlgiger2', '', '', 'f14219576710df5e8f9e3ffb3c2c548c', 0, '2014-10-02 16:24:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'male', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000296, 'Giger', '', 'Kurl Keith', 1, 0000000293, 2, 2, 2, 0000000293, 'kurlgiger3', '', '', 'f14219576710df5e8f9e3ffb3c2c548c', 0, '2014-10-02 16:24:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'male', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000297, 'Carado', '', 'Adrian', 1, 0000000295, 1, 2, 1, 0000000295, 'acarado', '', 'acarado86@yahoo.com', 'ed19863a24ecd4115bf2f7f4c0e34e16', 0, '2014-10-02 16:28:10', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '7/20/1988', 'Male', '09094884302', '', 'poblacion, sta. maria, Davao Occidental', 'Davao', 'Davao Occidental', '', '', '0', '0', '0', '0', '0', 'Single', 1),
(0000000298, 'Carado', '', 'Adrian', 1, 0000000297, 1, 2, 1, 0000000297, 'acarado2', '', '', 'ed19863a24ecd4115bf2f7f4c0e34e16', 0, '2014-10-02 16:29:38', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'male', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000299, 'Carado', '', 'Adrian', 1, 0000000297, 2, 2, 2, 0000000297, 'acarado3', '', '', 'ed19863a24ecd4115bf2f7f4c0e34e16', 0, '2014-10-02 16:29:38', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'male', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000300, 'ruizo', 'canete', 'maico', 1, 0000000289, 2, 2, 2, 0000000289, 'maico4201', 'primary-0000000300.jpg', 'johnkurtz38@yahoo.com', '2b125e61ded904358bfd6b4736bd14cd', 0, '2014-10-02 16:30:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '01/10/1994', 'Male', '09331007637', '', '', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 1),
(0000000301, 'Albores', 'Mayola', 'Francis', 1, 0000000298, 1, 2, 1, 0000000298, 'falbores', 'primary-0000000301.jpg', '', '73660d89004ac08256ff81af5f947fdd', 0, '2014-10-02 16:31:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/03/2014', 'Male', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 1),
(0000000302, 'Albores', 'Mayola', 'Francis', 1, 0000000301, 1, 2, 1, 0000000301, 'falbores2', '', '', '73660d89004ac08256ff81af5f947fdd', 0, '2014-10-02 16:35:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/03/2014', 'male', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000303, 'Albores', 'Mayola', 'Francis', 1, 0000000301, 2, 2, 2, 0000000301, 'falbores3', '', '', '73660d89004ac08256ff81af5f947fdd', 0, '2014-10-02 16:35:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/03/2014', 'male', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000304, 'ruizo', 'canete', 'maico', 1, 0000000300, 1, 2, 1, 0000000300, 'maico42012', '', 'johnkurtz38@yahoo.com', '2b125e61ded904358bfd6b4736bd14cd', 0, '2014-10-02 16:35:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '01/10/1994', 'male', '09331007637', '', '', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000305, 'ruizo', 'canete', 'maico', 1, 0000000300, 2, 2, 2, 0000000300, 'maico42013', '', 'johnkurtz38@yahoo.com', '2b125e61ded904358bfd6b4736bd14cd', 0, '2014-10-02 16:35:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '01/10/1994', 'male', '09331007637', '', '', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000306, 'Albores', 'Layon', 'Febie', 1, 0000000302, 1, 2, 1, 0000000302, 'febalbores', '', '', 'e3c60d3983f3971f259fe9e0f4eccdf9', 0, '2014-10-02 16:41:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'female', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 1),
(0000000307, 'Albores', 'Layon', 'Febie', 1, 0000000306, 1, 2, 1, 0000000306, 'febalbores2', '', '', 'e3c60d3983f3971f259fe9e0f4eccdf9', 0, '2014-10-02 16:46:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'female', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 1),
(0000000308, 'Albores', 'Layon', 'Febie', 1, 0000000306, 2, 2, 2, 0000000306, 'febalbores3', '', '', 'e3c60d3983f3971f259fe9e0f4eccdf9', 0, '2014-10-02 16:46:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'female', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000309, 'Daramayo', '', 'Mariel', 1, 0000000307, 1, 2, 1, 0000000307, 'marieldaramayo', '', '', 'd1ddfefd5c22a112c72dd38bab77ed4f', 0, '2014-10-02 17:19:37', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'female', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000310, 'Daramayo', '', 'Mariel', 1, 0000000309, 1, 2, 1, 0000000309, 'marieldaramayo2', '', '', 'd1ddfefd5c22a112c72dd38bab77ed4f', 0, '2014-10-02 17:20:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'female', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000311, 'Daramayo', '', 'Mariel', 1, 0000000309, 2, 2, 2, 0000000309, 'marieldaramayo3', '', '', 'd1ddfefd5c22a112c72dd38bab77ed4f', 0, '2014-10-02 17:20:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'female', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000312, 'Ruizo', '', 'Donna', 1, 0000000304, 1, 2, 1, 0000000304, 'donna.pinklish', '', '', '5b9e2103e68bbb32c503f4c1b94e6047', 0, '2014-10-02 17:21:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '07/11/1991', 'female', '', '', '', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000313, 'Daramayo', 'Layon', 'Fe', 1, 0000000310, 1, 2, 1, 0000000310, 'fedaramayo', '', '', '2c74d121378488ed049123b925d9cf67', 0, '2014-10-02 17:22:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'female', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000314, 'Bermillo', 'Z', 'Ashley Clar', 1, 0000000303, 1, 2, 1, 0000000303, 'ashbermillo', '', '', '4b7684d1295eb87dc2cf9e313b9f2c8b', 0, '2014-10-02 17:27:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'female', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000315, 'Ruizo', '', 'Virgie', 1, 0000000305, 1, 2, 1, 0000000305, 'mama', '', '', '2b125e61ded904358bfd6b4736bd14cd', 0, '2014-10-02 17:30:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '07/23/1964', 'male', '', '', '', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000316, 'Seniedo', 'C', 'Arjane', 1, 0000000314, 1, 2, 1, 0000000314, 'aseniedo', '', '', 'ee062ca760e6021ce38b3884ea69e622', 0, '2014-10-02 17:30:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'female', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000317, 'Ruizo', '', 'Donna', 1, 0000000312, 1, 2, 1, 0000000312, 'donna.pinklish2', '', '', '5b9e2103e68bbb32c503f4c1b94e6047', 0, '2014-10-02 17:31:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '07/11/1991', 'female', '', '', '', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000318, 'Ruizo', '', 'Donna', 1, 0000000312, 2, 2, 2, 0000000312, 'donna.pinklish3', '', '', '5b9e2103e68bbb32c503f4c1b94e6047', 0, '2014-10-02 17:31:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '07/11/1991', 'female', '', '', '', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000319, 'Seniedo', 'C', 'Arjane', 1, 0000000316, 1, 2, 1, 0000000316, 'aseniedo2', '', '', 'ee062ca760e6021ce38b3884ea69e622', 0, '2014-10-02 17:32:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'female', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000320, 'Seniedo', 'C', 'Arjane', 1, 0000000316, 2, 2, 2, 0000000316, 'aseniedo3', '', '', 'ee062ca760e6021ce38b3884ea69e622', 0, '2014-10-02 17:32:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'female', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000321, 'Giger', 'Ruizo', 'Merlin', 1, 0000000289, 1, 2, 1, 0000000289, 'mrlgiger', '', '', 'eeba0f12cfd460ca4be3926be739067e', 0, '2014-10-02 17:34:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'female', '', '', '', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000322, 'Giger', 'Ruizo', 'Merlin', 1, 0000000321, 1, 2, 1, 0000000321, 'mrlgiger2', '', '', 'eeba0f12cfd460ca4be3926be739067e', 0, '2014-10-02 17:38:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'female', '', '', '', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000323, 'Giger', 'Ruizo', 'Merlin', 1, 0000000321, 2, 2, 2, 0000000321, 'mrlgiger3', '', '', 'eeba0f12cfd460ca4be3926be739067e', 0, '2014-10-02 17:38:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'female', '', '', '', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000324, 'Lopez', 'V', 'Lester John', 1, 0000000319, 1, 2, 1, 0000000319, 'ljlopez', '', '', 'f15ec2a2188471fac3d04ba689239510', 0, '2014-10-02 17:42:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'male', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 1),
(0000000325, 'Lopez', 'V', 'Lester John', 1, 0000000324, 1, 2, 1, 0000000324, 'ljlopez2', '', '', 'f15ec2a2188471fac3d04ba689239510', 0, '2014-10-02 17:42:38', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'male', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 1),
(0000000326, 'Lopez', 'V', 'Lester John', 1, 0000000324, 2, 2, 2, 0000000324, 'ljlopez3', '', '', 'f15ec2a2188471fac3d04ba689239510', 0, '2014-10-02 17:42:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'male', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 1),
(0000000328, 'Dadang', '', 'Risa Mae', 1, 0000000325, 1, 2, 1, 0000000325, 'risadadang', '', '', 'e7d15f326e5b1d82edf205baa946d968', 0, '2014-10-02 17:50:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'female', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 1),
(0000000329, 'Dadang', '', 'Risa Mae', 1, 0000000328, 1, 2, 1, 0000000328, 'risadadang2', '', '', 'e7d15f326e5b1d82edf205baa946d968', 0, '2014-10-02 17:54:21', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'female', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000330, 'Dadang', '', 'Risa Mae', 1, 0000000328, 2, 2, 2, 0000000328, 'risadadang3', '', '', 'e7d15f326e5b1d82edf205baa946d968', 0, '2014-10-02 17:54:21', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'female', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000331, 'Galdo', '', 'Arjon', 1, 0000000329, 1, 2, 1, 0000000329, 'arjongaldo', 'primary-0000000331.jpg', 'arjon_143@yahoo.com', 'ae6b1256fc7b4cebff9b437098266d42', 0, '2014-10-02 17:59:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '21/02/1990', 'Male', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', 'Single', 1),
(0000000332, 'Galdo', '', 'Arjon', 1, 0000000331, 1, 2, 1, 0000000331, 'arjongaldo2', '', '', 'ae6b1256fc7b4cebff9b437098266d42', 0, '2014-10-02 18:00:28', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'male', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000333, 'Galdo', '', 'Arjon', 1, 0000000331, 2, 2, 2, 0000000331, 'arjongaldo3', '', '', 'ae6b1256fc7b4cebff9b437098266d42', 0, '2014-10-02 18:00:28', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'male', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000334, 'Millendez', 'Patol', 'Mark Marlon', 1, 0000000332, 1, 2, 1, 0000000332, 'partakers18', 'primary-0000000334.jpg', 'analoumanliguez@gmail.com', '0849795fccf6b305b372099371ed842d', 0, '2014-10-02 18:01:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '12/08/1988', 'Male', '09098416474', '', 'Phase 9B Blk 33 Lot 36 DECA Homes, Mintal', 'Davao City', 'Davao del Sur', '', '', '0', '0', '0', '0', '0', 'In a relationship', 1),
(0000000335, 'Millendez', '', 'Mark Marlon', 1, 0000000334, 1, 2, 1, 0000000334, 'marlonmillendez2', '', '', 'e8d08bef74a18542a6211efc69adda79', 0, '2014-10-02 18:02:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'male', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000336, 'Millendez', '', 'Mark Marlon', 1, 0000000334, 2, 2, 2, 0000000334, 'marlonmillendez3', '', '', 'e8d08bef74a18542a6211efc69adda79', 0, '2014-10-02 18:02:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'male', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000338, 'Ruizo', '', 'Fernando', 1, 0000000304, 2, 2, 2, 0000000304, 'fruizo', '', '', '5b9e2103e68bbb32c503f4c1b94e6047', 0, '2014-10-02 18:18:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'male', '', '', '', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000339, 'Ming', '', 'Evee', 0, 0000000000, 1, 2, 1, 0000000000, 'Evee', '', '', '982e587d19d2f7302ca70f9110359715', 0, '2014-10-06 01:30:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'male', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', 0),
(0000000340, 'evee', 'a', 'Evee', 0, 0000000000, 2, 2, 2, 0000000000, 'evee1', '', 'a@y.com', 'da9fbf785c73d8f31c98e4ad8dd27e4a', 0, '2014-10-06 06:17:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/06/2014', 'male', 'awew', '', 'Aw', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000342, 'Evee', 'E', 'Evee', 0, 0000000287, 1, 2, 1, 0000000287, 'evee2', '', 'a@y.com', '982e587d19d2f7302ca70f9110359715', 0, '2014-10-06 08:49:52', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'male', '22222222222', '', 'awdw', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000343, 'Albores', 'M', 'Rodel', 1, 0000000308, 2, 2, 2, 0000000308, 'rodalbores', 'primary-0000000343.jpg', 'rocketph@live.com', '5cfe1269df606507f1bc811ff1f344ac', 0, '2014-10-06 09:59:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/15/2014', 'Male', '09372469719', '', 'Deca Homes Resort and Residences', 'Davao City', 'Davao', 'mindanao', '', '0', '0', '0', '0', '0', 'In a relationship', 1),
(0000000344, 'Besinan', 'M', 'Jonathan', 1, 0000000303, 2, 2, 2, 0000000303, 'jonbesinan', '', 'jonathan.besinan@gmail.com', '2923d92f7ef029a365bbeb33e1dc2def', 0, '2014-10-06 10:07:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/07/2014', 'male', '123123', '', 'Tibungco', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 1),
(0000000348, 'Albores', 'Mayola', 'Rodel', 1, 0000000343, 1, 2, 1, 0000000343, 'rodalbores2', 'primary-0000000348.jpg', 'rocketph@live.com', '5cfe1269df606507f1bc811ff1f344ac', 0, '2014-10-06 10:10:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/15/2014', 'Male', '09372469719', '', 'Davao', 'DAVAO CITY', '', 'mindanao', '', '0', '0', '0', '0', '0', 'Single', 1),
(0000000349, 'Albores', 'Mayola', 'Rodel', 1, 0000000343, 2, 2, 2, 0000000343, 'rodalbores3', '', 'rod@temp.com', '2c841a9a3e310e91b7536035adc9382e', 0, '2014-10-06 10:10:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/15/2014', 'male', '123123123123', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000350, 'Test', 'test', 'Test', 1, 0000000344, 2, 2, 2, 0000000301, 'test', '', 'test@test.com', 'cc265f2ba82a811896481476c0ff9fee', 0, '2014-10-06 10:23:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/06/2014', 'male', '123', '', 'Test', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000352, 'Ruizo', 'B', 'Constancio', 1, 0000000322, 1, 2, 1, 0000000322, 'ConstancioRuizo', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 12:04:28', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '09/19/2014', 'male', '0123456789', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000353, 'Ruizo', 'B', 'Uldarica', 1, 0000000322, 2, 2, 2, 0000000322, 'UldaricaRuizo', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 12:11:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'female', '0123456789', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000354, 'Fernandez', 'R', 'Susan', 1, 0000000323, 1, 2, 1, 0000000323, 'susanruizo', '', 'ajdkajwdkjawd@ymail.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 12:32:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'female', '123', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000355, 'Solano', 'A', 'Darwin', 1, 0000000323, 2, 2, 2, 0000000323, 'darwinsolano', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 14:12:14', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'male', '0123456', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000356, 'Solano', 'A', 'Darwin', 1, 0000000355, 1, 2, 1, 0000000355, 'darwinsolano2', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 14:14:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'male', '0123456', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000357, 'Solano', 'A', 'Darwin', 1, 0000000355, 2, 2, 2, 0000000355, 'darwinsolano3', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 14:14:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'male', '0123456', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000358, 'Kuan Tiu', 'Ang', 'John Gabriel', 1, 0000000356, 2, 2, 2, 0000000356, 'jgkuantiu', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 14:25:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'male', '123', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 1),
(0000000359, 'Kuan Tiu', 'Ang', 'John Gabriel', 1, 0000000358, 1, 2, 1, 0000000358, 'jgkuantiu2', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 14:27:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'male', '123', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000360, 'Kuan Tiu', 'Ang', 'John Gabriel', 1, 0000000358, 2, 2, 2, 0000000358, 'jgkuantiu3', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 14:27:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'male', '123', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000361, 'Ganson', 'Marcojos ', 'Genevieve ', 1, 0000000357, 1, 2, 1, 0000000357, 'genganson', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 14:37:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'female', '1234', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 1),
(0000000362, 'Ganson', 'Marcojos ', 'Genevieve ', 1, 0000000361, 1, 2, 1, 0000000361, 'genganson2', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 14:38:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'female', '1234', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000363, 'Ganson', 'Marcojos ', 'Genevieve ', 1, 0000000361, 2, 2, 2, 0000000361, 'genganson3', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 14:38:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'female', '1234', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000364, 'Reyes', 'Gonzales', 'Jeric', 1, 0000000360, 2, 2, 2, 0000000360, 'jericreyes', 'primary-0000000364.jpg', 'kjr_21@yahoo.com', '3f676d7c990592c113e0ee6390dddf5b', 0, '2014-10-07 14:43:10', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'Male', '09124781099', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', 'Single', 1),
(0000000365, 'Reyes', 'Gonzales', 'Jeric', 1, 0000000364, 1, 2, 1, 0000000364, 'jericreyes2', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 14:44:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'male', '123', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000366, 'Reyes', 'Gonzales', 'Jeric', 1, 0000000364, 2, 2, 2, 0000000364, 'jericreyes3', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 14:44:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'male', '123', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000367, 'Reyes', 'J', 'Mark Ronnes', 1, 0000000366, 2, 2, 2, 0000000366, 'markreyes', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 14:49:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'male', '123', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000368, 'Reyes', 'J', 'Mark Ronnes', 1, 0000000367, 1, 2, 1, 0000000367, 'markreyes2', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 14:51:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'male', '123', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000369, 'Reyes', 'J', 'Mark Ronnes', 1, 0000000367, 2, 2, 2, 0000000367, 'markreyes3', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 14:51:07', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'male', '123', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 1),
(0000000370, ' Eque', 'Parilla', 'Richel ', 1, 0000000362, 1, 2, 1, 0000000362, 'richeque', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 14:55:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'female', '123', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000371, 'Maghari', 'G', 'Antonio', 1, 0000000372, 1, 2, 1, 0000000335, 'antoniomaghari', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 15:19:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'male', '123', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000372, 'Maghari', 'G', 'Antonio', 1, 0000000371, 1, 2, 1, 0000000371, 'antoniomaghari2', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 15:20:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'male', '123', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000373, 'Maghari', 'G', 'Antonio', 1, 0000000371, 2, 2, 2, 0000000371, 'antoniomaghari3', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 15:20:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'male', '123', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000376, 'Bautista', 'A', 'James ', 1, 0000000372, 1, 2, 1, 0000000372, 'jbautista', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 16:39:56', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'male', '123', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000377, 'Bautista', 'A', 'James ', 1, 0000000376, 1, 2, 1, 0000000376, 'jbautista2', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 16:50:14', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'male', '123', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000378, 'Bautista', 'A', 'James ', 1, 0000000376, 2, 2, 2, 0000000376, 'jbautista3', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 16:50:14', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'male', '123', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000379, 'Galeon', 'F', 'Audrey ', 1, 0000000377, 1, 2, 1, 0000000377, 'audgaleon', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-07 17:04:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'female', '123', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 1),
(0000000381, 'Cabuga', 'A', 'Irene', 1, 0000000369, 2, 2, 2, 0000000369, 'irenecabuga', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-08 03:19:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'female', '123', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000382, 'Surban', 'A', 'Zyra Mae', 1, 0000000379, 1, 2, 1, 0000000379, 'zyrasuraban', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-08 03:32:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'female', '123', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000383, 'Dello', 'Tan', 'Christopher', 1, 0000000382, 1, 2, 1, 0000000382, 'ctdello', '', 'a@y.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-08 03:35:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/08/2014', 'male', '123', '', 'Davao', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 1),
(0000000384, 'Manliguez', 'C', 'Analou', 0, 0000000334, 2, 2, 2, 0000000334, 'commodum12', '', 'analoumanliguez@yahoo.com', 'fdbb2044d2622021a6c69f206183d2e0', 0, '2014-10-08 05:12:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11/21/1989', 'female', '09098416474', '', 'Phase 9B Deca Homes, Mintal', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000385, 'dsada', 'dasd', 'dasda', 0, 0000000324, 2, 2, 2, 0000000324, 'dasd', '', 'asdasd@yahoo', '9ab68ddefb42028765e21d42f956aee7', 0, '2014-10-08 20:20:07', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10/07/2014', 'male', '1241231541', '', 'asdasd', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000387, 'Lobiano', 'Abadies', 'Joyce Ann', 0, 0000000334, 1, 2, 1, 0000000334, 'jaalobiano', '', 'joyceannlobiano@yahoo.com', 'ac71fdad0effeddd1ee53332efb7205a', 0, '2014-10-08 23:23:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11/13/1996', 'female', '09307697690', '', 'Toril, Davao City', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000388, 'Peralta', 'B.', 'Mica Princess', 0, 0000000287, 1, 2, 1, 0000000287, 'pmicaprincess@yahoo.com', '', 'pmicaprincess@yahoo.com', '40d9f79d8a1b411fdc9fbb3d468e971d', 0, '2014-10-09 00:29:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '09/18/1996', 'female', '+63912979968', '', 'B.8 , L.26  , SGR Village , Cat. Grande , Davao City', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0),
(0000000389, 'Linog', 'Heje', 'Elijohn Anthony', 0, 0000000287, 1, 2, 1, 0000000287, 'elijohn.anthony', '', 'elijohn.anthony@gmail.com', '55b46f0b47d371f9ff7508b220d37e1b', 0, '2014-10-09 05:19:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '06-04-1988', 'Male', '09153908273', '', 'Gusa, Cagayan de Oro City', 'Cagayan de Oro City', 'Misamis Oriental', 'mindanao', '', '0', '0', '0', '0', '0', 'In a relationship', 0),
(0000000392, 'Eque', 'Parilla', 'Richel', 0, 0000000287, 1, 2, 1, 0000000287, 'richeleque', '', 'richel_eque@yahoo.com', '11ed57e580f9403534ed003c2d8cca6b', 0, '2014-10-09 07:56:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'September ', 'female', '09484518476', '', 'Davao city', '', '', 'mindanao', '', '0', '0', '0', '0', '0', '', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
